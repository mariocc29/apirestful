<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\Access\AuthorizationException;

class LocalCallOnly
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->server('SERVER_ADDR') != $request->server('REMOTE_ADDR')){
          throw new AuthorizationException;
        }

        return $next($request);
    }
}
