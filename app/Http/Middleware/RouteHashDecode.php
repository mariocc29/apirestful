<?php

namespace App\Http\Middleware;

use App\Traits\ApiHash;
use Closure;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class RouteHashDecode
{
    use ApiHash;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $keys = array_keys($request->route()->parameters());

        foreach ($keys as $key) {
          $model_name = 'App\Models\\' . studly_case(str_singular($key));
          $decoded    = $this->hash_decode( $request->route()->parameter($key), $model_name );
          if ( !is_null($decoded) ) {
            $request->route()->setParameter($key, $decoded);
          }
        }

        return $next($request);
    }
}
