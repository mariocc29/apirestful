<?php

namespace App\Http\Middleware;

use App\Http\Middleware\ApiRequest;
use App\Models\App as Application;
use App\Traits\ApiResponser;
use Closure;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\App;
use Symfony\Component\HttpFoundation\Response;

class ApiAuth
{
    use ApiResponser;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Auth ServerToServer
        if ( !is_null($request->user()) && $request->user()->token()->name === Application::class ) {
          $application = $request->user();
        } else {
          // Auth ConsoleToServer, AppToServer
          if( !$request->hasHeader('AppKey') ){
            return $this->errorResponse(__('auth.appkey'), Response::HTTP_UNAUTHORIZED);
          }

          $application = Application::where('appkey', $request->header('AppKey'))->get()->first();
          if ( is_null($application) ) {
            throw new AuthorizationException;
          }
        }

        //Setea el lenguage de la aplicación
        $language = $application->languages->where('pivot.default', true)->pluck('iso')->first();
        if ( !is_null($language) ) {
            App::setLocale( $language );
        }

        $request->attributes->add(['application' => $application]);

        return $next($request);
    }
}
