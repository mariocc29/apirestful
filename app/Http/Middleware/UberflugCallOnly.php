<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Config;

class UberflugCallOnly
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( !$request->hasHeader('StaffKey') || Config::get('constants.uberflug.token') !== $request->header('StaffKey') ){
          throw new AuthorizationException;
        }

        $request->attributes->add(['source' => 'console']);
        
        return $next($request);
    }
}
