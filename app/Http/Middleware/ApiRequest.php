<?php

namespace App\Http\Middleware;

use App\Traits\ApiResponser;
use Closure;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;

class ApiRequest
{
    use ApiResponser;

    const CONSOLE = 'console';
    
    const APP     = 'app';

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( $request->hasHeader('ConsoleKey') ){
          if ( Config::get('constants.console.token') !== $request->header('ConsoleKey') ) {
            throw new AuthorizationException;
          }
          
          $request->attributes->add(['source' => ApiRequest::CONSOLE]);
          Config::set('auth.providers.users.model', \App\Models\Account::class);
        } elseif( $request->hasHeader('AppKey') ) {
          $request->attributes->add(['source' => ApiRequest::APP]);
          Config::set('auth.providers.users.model', \App\Models\User::class);
        } else {
          Config::set('auth.providers.users.model', \App\Models\App::class);
          $application = Auth::guard('api')->user();
          $request->headers->set('AppKey', $application->appkey);
        }

        return $next($request);
    }
}
