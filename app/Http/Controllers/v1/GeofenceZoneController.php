<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Models\Geofence;
use App\Models\Zone;
use Illuminate\Http\Request;

class GeofenceZoneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Geofence $geofence)
    {
        $zones = $geofence->zones;
        return $this->showAll($zones);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Geofence $geofence)
    {
        $this->validate($request, [
          'label'        => 'required|string|max:50', 
          'description'  => 'required|string'
        ]);

        $zone = new Zone;
        $zone->geofence_id  = $geofence->id;
        $zone->label        = $request->label;
        $zone->description  = $request->description;
        $zone->save();

        return  $this->showOne($zone);
    }
}
