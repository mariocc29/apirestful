<?php

namespace App\Http\Controllers\v1;

use App\Models\Language;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LanguageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $languages = Language::all();
        return $this->showAll($languages);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'label'   => 'required|string',
            'iso'     => 'required|string|max:2',
            'picture' => 'nullable|image',
          ]);

        $language = new Language;
        $language->label   = $request->label;
        $language->iso     = $request->iso;
        $language->picture = $request->picture;
        $language->save();

        return $this->showOne($language);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Language  $language
     * @return \Illuminate\Http\Response
     */
    public function show(Language $language)
    {
        return $this->showOne($language);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Language  $language
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Language $language)
    {
        $this->validate($request, [
            'label'   => 'string',
            'iso'     => 'string|max:2',
            'picture' => 'image',
          ]);

        $language->fill($request->only(['label', 'iso', 'picture']));
        if ( $language->isDirty() ) {
          $language->update();
        }
        
        return $this->showOne($language);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Language  $language
     * @return \Illuminate\Http\Response
     */
    public function destroy(Language $language)
    {
        if ( $input->apps->count() > 0 ) {
          throw new \Exception( __('exception.children') );
        }

        $language->delete();
        return $this->showOne($language);
    }

    /**
     * Display a listing of the resource with conditions parameters.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $this->validate($request, [
            'keyword' => 'required|string',
            'value'   => 'required|string|min:3'
          ]);

        $languages = Language::where($request->keyword, 'iLike', "%{$request->value}%")->get();
        return $this->showAll($languages);
    }
}
