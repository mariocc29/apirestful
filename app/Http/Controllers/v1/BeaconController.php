<?php

namespace App\Http\Controllers\v1;

use App\Models\Beacon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BeaconController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $beacons = Beacon::all();
        return $this->showAll($beacons);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'label'       => 'required|string|max:50', 
          'description' => 'required|string', 
          'uuid'        => 'required|string|unique:triggers.beacons,uuid', 
          'major'       => 'required|string', 
          'minor'       => 'required|string'
        ]);

        $beacon = new Beacon;
        $beacon->app_id      = $request->get('application')->id;
        $beacon->label       = $request->label;
        $beacon->description = $request->description;
        $beacon->uuid        = $request->uuid;
        $beacon->major       = $request->major;
        $beacon->minor       = $request->minor;
        $beacon->save();

        return $this->showOne($beacon);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Beacon  $beacon
     * @return \Illuminate\Http\Response
     */
    public function show(Beacon $beacon)
    {
        return $this->showOne($beacon);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Beacon  $beacon
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Beacon $beacon)
    {
        $this->validate($request, [
          'label'       => 'string|max:50', 
          'description' => 'string', 
          'uuid'        => "string|unique:triggers.beacons,uuid,{$beacon->id}",
          'major'       => 'string', 
          'minor'       => 'string'
        ]);

        $beacon->fill($request->only(['label', 'description', 'uuid', 'major', 'minor']));
        if ( $beacon->isDirty() ) {
          $beacon->update();
        }

        return $this->showOne($beacon);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Beacon  $beacon
     * @return \Illuminate\Http\Response
     */
    public function destroy(Beacon $beacon)
    {
        $beacon->delete();
        return $this->showOne($beacon);
    }

    /**
     * Display a listing of the resource with conditions parameters.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $this->validate($request, [
            'keyword' => 'required|string',
            'value'   => 'required|string|min:3'
          ]);

        $beacon = Beacon::where($request->keyword, 'iLike', "%{$request->value}%")->get();
        return $this->showAll($beacon);
    }
}
