<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Geofence;

class GeofenceController extends Controller
{
  public function show(Geofence $geofence)
  {
    return $this->showOne($geofence);
  }

  public function update(Geofence $geofence)
  {
    $this->validate($request, [
      'label'        => 'string|max:50',
      'description'  => 'string', 
      'geo_position' => 'geo', 
      'radius'       => 'integer'
    ]);

    $geofence->fill($request->only(['label', 'description', 'geo_position', 'radius']));

    if ( $geofence->isDirty() ) {
      $geofence->update();
    }
    
    return $this->showOne($geofence);
  }

  public function destroy(Geofence $geofence)
  {
    $geofence->delete();
    return $this->showOne($geofence);
  }
}

