<?php

namespace App\Http\Controllers\v1;

use App\Models\Repeat; 
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RepeatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $repeats = Repeat::all();
      return $this->showAll($repeats);
    }   
}
