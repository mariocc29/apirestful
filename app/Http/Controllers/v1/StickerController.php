<?php

namespace App\Http\Controllers\v1;

use App\Models\Sticker; 
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StickerController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
          'experience_key' => 'required|string|exists_key:marketing,experiences,id,deleted_at,NULL',
          'picture'        => 'required|image'
        ]);

      $sticker = new Sticker;
      $sticker->experience_id  = $request->experience_key;
      $sticker->picture        = $request->picture;
      $sticker->save();

      return  $this->showOne($sticker);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sticker $sticker)
    {
      $this->validate($request, [
          'experience_key'  => 'string|exists_key:marketing,experiences,id,deleted_at,NULL',
          'picture'         => 'image'
        ]);

      $sticker->experience_id = $request->experience_key;
      $sticker->picture       = $request->picture; 

      if ( $sticker->isDirty() ) {
        $sticker->update();
      }
      
      return $this->showOne($sticker);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sticker $sticker)
    {
      $sticker->delete();
      return $this->showOne($sticker);
    }
}
