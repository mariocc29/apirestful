<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;

class ProductController extends Controller
{
  public function index()
  {
    return $this->showAll(Product::all());
  }

  public function show(Product $product)
  {
    return $this->showOne($product);
  }

  public function store(Request $request)
  {
    $this->validate($request, [
        'label'            => 'required|language',
        'picture'          => 'required|image',
        'additional_data'  => 'array'
    ]);

    $product = new Product;
    $product->app_id          = $request->get('application')->id;
    $product->label           = $request->label;
    $product->picture         = $request->picture;
    $product->additional_data = $request->additional_data;
    $product->tagname         = null;
    $product->save();

    return $this->showOne($product);
  }

  public function update(Request $request, Product $product)
  {
    $this->validate($request, [ 
        'label'            => 'language',
        'picture'          => 'image',
        'additional_data'  => 'array'
    ]);

    $product->fill($request->only(['label', 'picture', 'condition', 'additional_data']));

    if ( $product->isDirty() ) {
      $product->update();
    }
    
    return $this->showOne($product);
  }

  public function destroy(Product $product)
  {
    $product->delete();
    return $this->showOne($product);
  }

  public function search(Request $request)
  {
      $this->validate($request, [
          'keyword' => 'required|string',
          'value'   => 'required|string',
          'key'     => 'required|string'
        ]);

      $products = Product::where($request->keyword.'->'.$request->key, 'iLike', "%{$request->value}%")->get();
      return $this->showAll($products);
  }
}



