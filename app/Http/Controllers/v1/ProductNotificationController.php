<?php

namespace App\Http\Controllers\v1;

use App\Models\Product;
use App\Models\Notification;  
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductNotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Product $product)
    {
      $notifications = $product->notifications;
      return $this->showAll($notifications);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Product $product)
    {
      $this->validate($request, [
          'segment_key' => 'required|string|exists_key:marketing,segments,id,deleted_at,NULL',
          'title'       => 'required|language', 
          'message'     => 'required|language', 
          'send_at'     => 'required|date'
        ]);

        $notification = new Notification;
        $notification->product_id = $product->id; 
        $notification->segment_id = $request->segment_key; 
        $notification->title      = $request->title; 
        $notification->message    = $request->message;
        $notification->send_at    = $request->send_at;
        $notification->save();

        return $this->showOne($notification);
    } 

    public function search(Request $request, Product $product)
    {
      $this->validate($request, [
          'keyword' => 'required|string',
          'value'   => 'required|string|min:3', 
          'key'     => 'required|string'
        ]);

      $notifications = $product->notifications()->where($request->keyword.'->'.$request->key, 'iLike', "%{$request->value}%")->get();
      return $this->showAll($notifications);
    }     
}
