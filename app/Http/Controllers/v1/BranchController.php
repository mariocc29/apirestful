<?php

namespace App\Http\Controllers\v1;

use App\Models\Branch;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $branches = Branch::all();
        return $this->showAll($branches);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'label'        => 'required|string', 
            'description'  => 'required|language', 
            'geo_position' => 'required|geo', 
            'telephones'   => 'required|array', 
            'address'      => 'required|string', 
            'schedule'     => 'required|schedule'
        ]);

        $branch = new Branch;
        $branch->app_id       = $request->get('application')->id;
        $branch->label        = $request->label;
        $branch->description  = $request->description;
        $branch->geo_position = $request->geo_position;
        $branch->telephones   = $request->telephones;
        $branch->address      = $request->address;
        $branch->schedule     = $request->schedule;
        $branch->tagname      = null;
        $branch->save();

        return $this->showOne($branch);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function show(Branch $branch)
    {
        return $this->showOne($branch);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Branch $branch)
    {
        $this->validate($request, [
            'label'        => 'string', 
            'description'  => 'language', 
            'geo_position' => 'geo', 
            'telephones'   => 'array', 
            'address'      => 'string', 
            'schedule'     => 'schedule'
        ]);

        $branch->fill($request->only(['label', 'description', 'geo_position', 'telephones', 'address', 'schedule']));
        if ( $branch->isDirty() ) {
          $branch->update();
        }
        
        return $this->showOne($branch);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function destroy(Branch $branch)
    {
        $branch->delete();
        return $this->showOne($branch);
    }

    /**
     * Display a listing of the resource with conditions parameters.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $this->validate($request, [
            'keyword' => 'required|string',
            'value'   => 'required|string|min:3'
          ]);

        $branches = Branch::where($request->keyword, 'iLike', "%{$request->value}%")->get();
        return $this->showAll($branches);
    }
}
