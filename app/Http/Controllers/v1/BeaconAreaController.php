<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Models\Area;
use App\Models\Beacon;
use Illuminate\Http\Request;

class BeaconAreaController extends Controller
{
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Beacon  $beacon
     * @return \Illuminate\Http\Response
     */
    public function destroy(Beacon $beacon, Area $area)
    {
        $beacon->area_id      = null;
        $beacon->geo_position = null;
        $beacon->position     = null;
        $beacon->update();
        return $this->showOne($beacon);
    }
}
