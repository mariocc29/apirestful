<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Jobs\Inputs\BeaconZoneJob;
use App\Jobs\Inputs\InputJob;
use App\Models\Beacon;
use App\Models\Zone;
use Illuminate\Http\Request;

class BeaconZoneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Beacon $beacon)
    {
        $zones = $beacon->zones;
        return $this->showAll($zones);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Beacon  $beacon
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Beacon $beacon, Zone $zone)
    {
        $result = $beacon->zones()->syncWithoutDetaching($zone->id);
        if ( count($result['attached']) > 0 ) {
          BeaconZoneJob::dispatch($beacon, $zone, InputJob::TRIGGER_UPDATED);
        }
        return $this->showAll($beacon->zones()->get());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Beacon  $beacon
     * @return \Illuminate\Http\Response
     */
    public function destroy(Beacon $beacon, Zone $zone)
    {
        $result = (boolean)$beacon->zones()->detach([$zone->id]);
        if ( $result === true ) {
          BeaconZoneJob::dispatch($beacon, $zone, InputJob::TRIGGER_DELETED);
        }
        return $this->showAll($beacon->zones()->get());
    }
}
