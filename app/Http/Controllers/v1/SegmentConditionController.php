<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Models\Condition;
use App\Models\Segment;
use App\Rules\ConditionInRule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class SegmentConditionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Segment $segment)
    {
        $conditions = $segment->conditions;
        return $this->showAll($conditions);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Segment $segment)
    {
        $this->validate($request, [
            'type'      => 'required|string|in:'.Condition::TYPE_TAG.','.Condition::TYPE_CUSTOM_PROPERTY,
            'provider'  => 'required_if:type,'.Condition::TYPE_TAG.'|string|in:'.Condition::PROVIDER_BRANCH.','.Condition::PROVIDER_CATEGORY.','.Condition::PROVIDER_PRODUCT,
            'property'  => 'required|string',
            'condition' => ['required', new ConditionInRule($request->type)],
            'value'     => 'required|string',
          ]);

        $condition = new Condition;
        $condition->segment_id = $segment->id;
        $condition->type       = $request->type;
        $condition->provider   = $request->provider;
        $condition->property   = $request->property;
        $condition->condition  = $request->condition;
        $condition->value      = $request->value;
        $condition->save();

        return $this->showOne($condition);
    }
}
