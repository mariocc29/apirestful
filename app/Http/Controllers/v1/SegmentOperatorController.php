<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class SegmentOperatorController extends Controller
{
    public function index(Request $request)
    {
        $conditions = collect( Config::get('constants.segments.operators') );
        return $this->showAll($conditions);
    }
}
