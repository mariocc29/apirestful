<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Models\Campaign;
use App\Models\Experience;
use App\Rules\ExperienceUniqueRule;
use Illuminate\Http\Request;

class CampaignExperienceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Campaign $campaign)
    {
      $experiences = $campaign->experiences;
      return $this->showAll($experiences);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Campaign $campaign)
    { 
      $this->validate($request, [
        'name'                   => 'required|language', 
        'description'            => 'required|language', 
        'message_notification'   => 'required|language',
        'enabled'                => 'required|boolean',
        'points'                 => 'required|integer',
        'attachement_type'       => 'required|string|max:5|in:'.Experience::ATTACHEMENT_TYPE_URL.','.Experience::ATTACHEMENT_TYPE_VIDEO.','.Experience::ATTACHEMENT_TYPE_IMAGE.','.Experience::ATTACHEMENT_TYPE_AUDIO, 
        'geofence_key'           => 'required_without_all:zone_key,beacon_key|exists_key:triggers,geofences,id,deleted_at,NULL',
        'zone_key'               => 'required_without_all:geofence_key,beacon_key|exists_key:triggers,zones,id,deleted_at,NULL',
        'beacon_key'             => 'required_without_all:geofence_key,zone_key|exists_key:triggers,beacons,id,deleted_at,NULL',
        'repeat_key'             => 'required|exists_key:marketing,repeats,id,deleted_at,NULL',
        'trigger'                => 'required|string|in:'.Experience::TRIGGER_ENTER.','.Experience::TRIGGER_EXIT.','.Experience::TRIGGER_INMEDIATE,
        'url'                    => 'url',
        'custom_property_label'  => 'string',
        'custom_property_key'    => ['required_with:custom_property_label', 'string', new ExperienceUniqueRule],
        'custom_property_values' => 'required_with:custom_property_label|array',
        'points'                 => 'integer',
        'probability'            => 'integer',
        'steps'                  => 'integer',
        'goal'                   => 'integer',
        'reminder_hours'         => 'integer',
      ]);

      $experience = new Experience;
      $experience->campaign_id              = $campaign->id;
      $experience->name                     = $request->name;
      $experience->description              = $request->description;
      $experience->message_notification     = $request->message_notification;
      $experience->enabled                  = $request->enabled;
      $experience->attachement_type         = $request->attachement_type;
      $experience->geofence_id              = $request->geofence_key;
      $experience->zone_id                  = $request->zone_key;
      $experience->beacon_id                = $request->beacon_key;
      $experience->repeat_id                = $request->repeat_key;
      $experience->trigger                  = $request->trigger;
      $experience->url                      = $request->url;
      $experience->custom_property_label    = $request->custom_property_label;
      $experience->custom_property_key      = $request->custom_property_key;
      $experience->custom_property_values   = $request->custom_property_values;
      $experience->points                   = $request->points;
      $experience->probability              = $request->probability;
      $experience->steps                    = $request->steps;
      $experience->goal                     = $request->goal;
      $experience->reminder_hours           = $request->reminder_hours;
      $experience->save();

      return $this->showOne($experience);

    }
}
