<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Condition;

class ConditionController extends Controller
{
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Segment  $segment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Condition $condition)
    {
      $conditions = collect( Config::get('constants.segments.operators') )->collapse()->unique()->toArray();

      $this->validate($request, [
          'type'      => 'string|in:'.Condition::TYPE_TAG.','.Condition::TYPE_CUSTOM_PROPERTY,
          'provider'  => 'required_if:type,'.Condition::TYPE_TAG.'|string|in:'.Condition::PROVIDER_BRANCH.','.Condition::PROVIDER_CATEGORY.','.Condition::PROVIDER_PRODUCT,
          'property'  => 'string',
          'condition' => 'string|in:'.implode(',', $conditions),
          'value'     => 'string'
      ]);

      $condition->fill($request->only(['type', 'provider', 'property', 'condition', 'value']));

      if ( $condition->isDirty() ) {
        $condition->update();
      }
      
      return $this->showOne($condition);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Segment  $segment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Condition $condition)
    {
      $condition->delete();
      return $this->showOne($condition);
    }
}

