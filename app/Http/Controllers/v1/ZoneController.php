<?php

namespace App\Http\Controllers\v1;

use App\Models\Zone; 
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ZoneController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Zone $zone)
    {
      return $this->showOne($zone);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Zone $zone)
    {
      $this->validate($request, [
          'label'           => 'string|max:50', 
          'description'     => 'string'
        ]);

      $zone->fill($request->only(['label', 'description']));
      
      if ( $zone->isDirty() ) {
        $zone->update();
      }

      return  $this->showOne($zone);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Zone $zone)
    {
      $zone->delete();
      return $this->showOne($zone);
    }
}
