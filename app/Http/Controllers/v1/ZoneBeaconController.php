<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Jobs\Inputs\BeaconZoneJob;
use App\Jobs\Inputs\InputJob;
use App\Models\Beacon;
use App\Models\Zone;
use Illuminate\Http\Request;

class ZoneBeaconController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Zone $zone)
    {
      $beacons = $zone->beacons;
      return $this->showAll($beacons);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Zone $zone, Beacon $beacon)
    {
        $result = $zone->beacons()->syncWithoutDetaching($zone->id);
        if ( count($result['attached']) > 0 ) {
          BeaconZoneJob::dispatch($beacon, $zone, InputJob::TRIGGER_UPDATED);
        }
        return $this->showAll($zone->beacons()->get());      
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Zone $zone, Beacon $beacon)
    {
        $result = (boolean)$zone->beacons()->detach([$beacon->id]);
        if ( $result === true ) {
          BeaconZoneJob::dispatch($beacon, $zone, InputJob::TRIGGER_DELETED);
        }
        return $this->showAll($zone->beacons()->get());
    }
}
