<?php

namespace App\Http\Controllers\v1;

use App\Models\App;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AppCampaignTypeController extends Controller
{
    public function index(App $app)
    {
      $campaigns_types = $app->campaigns_types;
      return $this->showAll($campaigns_types);
    }

    public function update(App $app, CampaignType $campaign_type)
    {
      $app->campaigns_types()->sync($campaign_type->id);
      return $this->showAll($app->campaigns_types()->get());
    }

    public function destroy(App $app, CampaignType $campaign_type)
    {
      $app->campaigns_types()->detach([$campaign_type]);
      return $this->showAll($app->campaigns_types()->get());
    }
}
