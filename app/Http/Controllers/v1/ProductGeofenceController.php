<?php

namespace App\Http\Controllers\v1;

use App\Models\Product; 
use App\Models\Geofence;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductGeofenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Product $product)
    {
      $geofences = $product->geofences;
      return $this->showAll($geofences);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Product $product)
    {
      $this->validate($request, [
          'label'        => 'required|string|max:50',
          'description'  => 'required|string', 
          'geo_position' => 'required|geo', 
          'radius'       => 'required|integer'
        ]);

        $geofence = new Geofence;
        $geofence->product_id   = $product->id;
        $geofence->label        = $request->label; 
        $geofence->description  = $request->description; 
        $geofence->geo_position = $request->geo_position; 
        $geofence->radius       = $request->radius; 
        $geofence->save();

        return $this->showOne($geofence);
    }

    public function search(Request $request, Product $product)
    {
      $this->validate($request, [
          'keyword' => 'required|string',
          'value'   => 'required|string|min:3'
        ]);

      $geofences = $product->geofences()->where($request->keyword, 'iLike', "%{$request->value}%")->get();
      return $this->showAll($geofences);
    }   
}
