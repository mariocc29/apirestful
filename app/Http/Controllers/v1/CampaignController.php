<?php

namespace App\Http\Controllers\v1;

use App\Models\Campaign;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CampaignController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Beacon  $beacon
     * @return \Illuminate\Http\Response
     */
    public function show(Campaign $campaign)
    {
        return $this->showOne($campaign);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Campaign  $campaign
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Campaign $campaign)
    {
        $this->validate($request, [
            'segment_key'       => 'string', 
            'name'              => 'language', 
            'description'       => 'language', 
            'schedule'          => 'schedule', 
            'status'            => 'boolean', 
            'start_at'          => 'date', 
            'end_at'            => 'date'
        ]);

        $campaign->fill($request->only(['segment_key', 'name', 'description', 'schedule', 'status', 'start_at', 'end_at']));
        if ( $campaign->isDirty() ) {
          $campaign->update();
        }
        
        return $this->showOne($campaign);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Campaign  $campaign
     * @return \Illuminate\Http\Response
     */
    public function destroy(Campaign $campaign)
    {
        $campaign->delete();
        return $this->showOne($campaign);
    }
}
