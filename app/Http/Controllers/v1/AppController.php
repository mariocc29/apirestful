<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Models\App;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Symfony\Component\HttpFoundation\Response;

class AppController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $applications = App::all();
        return $this->showAll($applications);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'name'             => 'required|string',
          'email'            => ['required', 'email', Rule::unique('general.apps')->whereNull('deleted_at')],
          'deep_link_scheme' => 'required|string',
          'domain_url'       => 'required|domain',
          'method_auth'      => 'required|auth',
        ]);

        $application = new App;
        $application->name             = $request->name;
        $application->email            = $request->email;
        $application->deep_link_scheme = $request->deep_link_scheme;
        $application->domain_url       = $request->domain_url;
        $application->appkey           = str_random(255);
        $application->additional_data  = ['auth' => $request->method_auth];

        $application->save();

        return $this->showOne($application);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\App  $app
     * @return \Illuminate\Http\Response
     */
    public function show(App $app)
    {
        return $this->showOne($app);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\App  $app
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, App $app)
    {
        $this->validate($request, [
          'name'             => 'string',
          'email'            => 'email',
          'deep_link_scheme' => 'string',
          'domain_url'       => 'url',
          'status'           => 'boolean',
        ]);

        $app->fill($request->only(['name', 'email', 'deep_link_scheme', 'domain_url', 'status']));
        if ( $app->isDirty() ) {
          $app->update();
        }
        
        return $this->showOne($app);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\App  $app
     * @return \Illuminate\Http\Response
     */
    public function destroy(App $app)
    {
        $app->delete();
        return $this->showOne($app);
    }

    /**
     * Display a listing of the resource with conditions parameters.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $this->validate($request, [
            'keyword' => 'required|string',
            'value'   => 'required|string|min:3'
          ]);

        $languages = App::where($request->keyword, 'iLike', "%{$request->value}%")->get();
        return $this->showAll($languages);
    }

}
