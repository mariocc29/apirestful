<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;

class ProductBeaconController extends Controller
{
  public function index(Product $product)
  {
    $beacons = $product->beacons;
    return $this->showAll($beacons);
  }
}
