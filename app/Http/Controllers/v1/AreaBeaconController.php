<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Models\Area;
use App\Models\Beacon;
use Illuminate\Http\Request;

class AreaBeaconController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Area $area)
    {
        $beacons = $area->beacons;
        return $this->showAll($beacons);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Area  $area
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Area $area, Beacon $beacon)
    {
        $this->validate($request, [
          'position' => 'required|position'
        ]);

        $beacon->area_id      = $area->id;
        $beacon->geo_position = $area->branch->geo_position;
        $beacon->position     = $request->position;
        $beacon->update();
        return $this->showAll($area->beacons()->get());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Area  $area
     * @return \Illuminate\Http\Response
     */
    public function destroy(Area $area, Beacon $beacon)
    {
        $beacon->area_id      = null;
        $beacon->geo_position = null;
        $beacon->position     = null;
        $beacon->update();
        return $this->showAll($area->beacons()->get());
    }
}
