<?php

namespace App\Http\Controllers\v1;

use App\Models\Survey; 
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SurveyController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
          'user_key'       => 'required|string|exists_key:general,users,id,deleted_at,NULL',
          'experience_key' => 'required|string|exists_key:marketing,experiences,id,deleted_at,NULL',          
          'tag'            => 'required|array', 
          'star'           => 'nullable|integer'
        ]);

        $survey = new Survey;
        $survey->user_id       = $request->user_key;
        $survey->experience_id = $request->experience_key;
        $survey->tag           = $request->tag;
        $survey->star          = $request->star;
        $survey->save();

        return  $this->showOne($survey);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Survey $survey)
    {
      $this->validate($request, [
          'user_key'       => 'string|exists_key:general,users,id,deleted_at,NULL',
          'experience_key' => 'string|exists_key:marketing,experiences,id,deleted_at,NULL',
          'tag'            => 'array',           
          'star'           => 'integer'
        ]);

        $survey->user_id       = $request->user_key; 
        $survey->experience_id = $request->experience_key; 
        $survey->tag           = $request->tag; 
        $survey->star          = $request->star; 

        if ( $survey->isDirty() ) {
          $survey->update();
        }
        
        return $this->showOne($survey);
    }
}
