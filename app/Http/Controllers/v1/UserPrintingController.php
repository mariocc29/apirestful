<?php

namespace App\Http\Controllers\v1;

use App\Models\User; 
use App\Models\Printing; 
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserPrintingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
      $printings = $user->printings;
      return $this->showAll($printings);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Printing $printing)
    {
      return $this->showOne($printing);
    }
}
