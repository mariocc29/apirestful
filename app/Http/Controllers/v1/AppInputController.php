<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Jobs\CreateMocaAccountJob;
use App\Models\App;
use App\Models\Input;
use Illuminate\Http\Request;

class AppInputController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(App $app)
    {
        $inputs = $app->inputs;
        return $this->showAll($inputs);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\App  $app
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, App $app, Input $input)
    {
        $this->validate($request, [
            'email'      => 'required|email',
            'password'   => 'required|string',
            'key'        => 'required|string',
            'account_id' => 'required|string',
            'app_id'     => 'required|string',
        ]);

        $app->inputs()->syncWithoutDetaching($input->id);
        if ($input->code === Input::MOCA) {
            CreateMocaAccountJob::dispatch($app, $input, (object)$request->all());
        }
        return $this->showAll($app->inputs()->get());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\App  $app
     * @return \Illuminate\Http\Response
     */
    public function destroy(App $app, Input $input)
    {
        $app->inputs()->detach([$input->id]);
        return $this->showAll($app->inputs()->get());
    }
}
