<?php

namespace App\Http\Controllers\v1;

use Hash;

use App\Models\App;
use App\Models\Account;

use App\Http\Controllers\Controller;
use App\Http\Middleware\ApiRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends Controller
{
    public function signIn(Request $request)
    {
      if ( $request->get('source') === ApiRequest::CONSOLE ) {
        return $this->signInConsole($request);
      } else {
        return $this->signInApp($request);
      }
    }

    public function signInConsole($request)
    {
      $validator = $this->validate($request, [
          'email'    => 'required|email',
          'password' => 'required|string|min:6'
        ]);

      if ( Auth::attempt(['email'=>$request->email, 'password' => $request->password]) ) {
        $user = Auth::user();
        $app  = $user->app()->first();
        $user->setAttribute('access_token', $user->createToken( \App\Models\Account::class, ['console_read', 'console_write'])->accessToken );
        $user->setAttribute('role', $user->role()->first());
        $user->setAttribute('languages', $app->languages );
        $user->setAttribute('inputs', $app->inputs );
        $user->setAttribute('campaigns_types', $app->campaigns_types );
        $user->setAttribute('appkey', $app->appkey );
        $user->setAttribute('datamapping', (isset($app->additional_data->datamapping)) ? $app->additional_data->datamapping : [] );

        return $this->showOne($user);
      }

      return $this->showMessage( __('auth.failed'), Response::HTTP_UNAUTHORIZED );
    }

    public function signInApp($request)
    {
      if( !$request->hasHeader('AppKey') ){
        return $this->errorResponse(__('auth.appkey'), Response::HTTP_UNAUTHORIZED);
      }

      $application = App::where('appkey', $request->header('AppKey'))->get()->first();
      if ( is_null($application) ) {
        throw new AuthorizationException;
      }

      $field    = $application->additional_data->auth->field;
      $rules    = collect([]);
      $attempts = collect([]);

      if ($field === 'email') {
        $rules->put($field, "required|email");
      } else {
        $rules->put($field, "required|string");
      }

      $attempts->put($field, $request->{$field});
      if ($application->additional_data->auth->has_password) {
        $rules->put('password', 'required|string|min:6');
        $attempts->put('password', $request->password);
      }

      $validator = $this->validate($request, $rules->toArray());

      if ( Auth::attempt($attempts->toArray()) ) {
        $user = Auth::user();
        $user->setAttribute('access_token', $user->createToken( \App\Models\User::class, ['app_read', 'app_write'])->accessToken );
        return $this->showOne($user);
      }
      
    }

    public function updatePassword(Request $request, Account $account)
    {
      $validator    = $this->validate($request, [
          'password'  => 'required|string',
          'new'       => 'required|string',
          'confirm'   => 'required|string'
        ]);
      
      if($account){
        if(Hash::check($request->password, $account->password) && ($request->new === $request->confirm)){ 
          $account->password = bcrypt($request->input('new'));
          $account->update();

          return $this->showOne($account);
        }

        return $this->errorResponse(__('exception.query'), Response::HTTP_CONFLICT);
      }

      return $this->errorResponse(__('exception.model'), Response::HTTP_BAD_REQUEST);
    }

    public function rememberPassword(Request $request)
    {
      $validator = $this->validate($request, [
          'email'    => 'required|email'
        ]);

      $user = Account::where('email', $request->email)->first();

      if($user){
        $newPassword      = str_random(10);
        $user->password   = bcrypt($newPassword);
        $user->update();

        $user->setAttribute('clave_test', $newPassword ); ##test##
        return $this->showOne($user);
      }

      return $this->errorResponse(__('exception.model'), Response::HTTP_BAD_REQUEST);
    }
}
