<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Notification;
use App\Models\Gallery;

class NotificationGalleryController extends Controller
{
  public function store(Request $request, Notification $notification)
  {
    $this->validate($request, [
      'language_key'         => 'required|exists_key:general.languages,id,deleted_at,NULL',
      'filename'             => 'required|string', 
      'additional_data'      => 'nullable|array'
    ]);

    $gallery = new Gallery;
    $gallery->language_id       = $request->language_key;
    $gallery->filename          = $request->filename;
    $gallery->source_id         = $notification->id;
    $gallery->source_type       = '\App\Models\Notification::class';
    $gallery->additional_data   = $request->additional_data;
    $gallery->save();

    return $this->showOne($gallery);
  }
}
