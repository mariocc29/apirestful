<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Models\Beacon;
use App\Models\Product;
use Illuminate\Http\Request;

class BeaconProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Beacon $beacon)
    {
        $products = $beacon->products;
        return $this->showAll($products);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Beacon  $beacon
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Beacon $beacon, Product $product)
    {
        $beacon->products()->syncWithoutDetaching($product->id);
        return $this->showAll($beacon->products()->get());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Beacon  $beacon
     * @return \Illuminate\Http\Response
     */
    public function destroy(Beacon $beacon, Product $product)
    {
        $beacon->products()->detach([$product->id]);
        return $this->showAll($beacon->products()->get());
    }
}
