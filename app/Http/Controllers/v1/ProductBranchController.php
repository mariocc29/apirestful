<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Branch;
use App\Models\Product;

class ProductBranchController extends Controller
{
  public function index(Product $product)
  {
    $branches = $product->branches;
    return $this->showAll($branches);
  }

  public function update(Request $request, Product $product, Branch $branch)
  {
    $product->branches()->syncWithoutDetaching($branch->id);
    return $this->showAll($product->branches()->get());
  }

  public function destroy(Product $product, Branch $branch)
  {
    $product->branches()->detach([$branch->id]);
    return $this->showAll($product->branches()->get());
  }
}
