<?php

namespace App\Http\Controllers\v1;

use App\Models\CampaignType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CampaignTypeController extends Controller
{
  /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
  public function index()
  {

    return $this->showAll(CampaignType::all());

  }
}
