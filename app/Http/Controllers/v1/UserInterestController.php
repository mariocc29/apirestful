<?php

namespace App\Http\Controllers\v1;

use App\Models\User;
use App\Models\Interest;  
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserInterestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
      $interests = $user->interests;
      return $this->showAll($interests);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user, Interest $interest)
    {
      $user->interests()->syncWithoutDetaching($interest->id);
      return $this->showAll($user->interests()->get());      
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user, Interest $interest)
    {
      $user->interests()->detach([$interest->id]);
      return $this->showAll($user->interests()->get());
    }
}
