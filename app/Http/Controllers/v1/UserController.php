<?php

namespace App\Http\Controllers\v1;

use App\Models\User; 
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $users = User::all();
      return $this->showAll($users);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
          'language_key'    => 'required|string|exists_key:general,languages,id,deleted_at,NULL',
          'network'         => 'required|string|max:25|in:none,facebook,google,twitter',
          'fullname'        => 'required|string|max:255', 
          'nickname'        => 'nullable|string|max:255',
          'email'           => ['nullable', 'email', Rule::unique('general.users')->whereNull('deleted_at')],
          'dni'             => 'nullable|string|max:255',
          'password'        => 'nullable|min:6', 
          'gender'          => 'required|string|max:1|min:1|in:m,f', 
          'birthdate'       => 'required|date', 
          'picture'         => 'nullable|string|max:255', 
          'additional_data' => 'nullable|array'
        ]);

      $user = new User;
      $user->app_id          = $request->get('application')->id;
      $user->language_id     = $request->language_key; 
      $user->network         = $request->network;
      $user->fullname        = $request->fullname; 
      $user->nickname        = $request->nickname;
      $user->email           = $request->email;
      $user->dni             = $request->dni;
      $user->password        = ( !is_null($request->password) ) ? bcrypt($request->password) : null; 
      $user->gender          = $request->gender;
      $user->birthdate       = $request->birthdate;
      $user->picture         = $request->picture;
      $user->additional_data = $request->additional_data; 
      $user->save();

      return  $this->showOne($user);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
      return $this->showOne($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
      $this->validate($request, [
          'language_key'    => 'string|exists_key:general.languages,id,deleted_at,NULL',
          'fullname'        => 'string|max:255', 
          'nickname'        => 'string|max:255',
          'dni'             => 'string|max:255',
          'password'        => 'nullable|min:6', 
          'gender'          => 'string|max:1|min:1', 
          'birthdate'       => 'date', 
          'picture'         => 'string|max:255', 
          'additional_data' => 'array'
        ]);

      $user->language_id     = $request->language_key; 
      $user->fullname        = $request->fullname; 
      $user->nickname        = $request->nickname;
      $user->dni             = $request->dni;
      $user->password        = ( !is_null($request->password) ) ? bcrypt($request->password) : null; 
      $user->gender          = $request->gender;
      $user->birthdate       = $request->birthdate;
      $user->picture         = $request->picture;
      $user->additional_data = $request->additional_data; 
      
      if ( $user->isDirty() ) {
        $user->update();
      }

      return  $this->showOne($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
      $user->delete();
      return $this->showOne($user);
    }

     /**
     * Display a listing of the resource with conditions parameters.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
      $this->validate($request, [
          'keyword' => 'required|string',
          'value'   => 'required|string|min:3'
        ]);

      $users = User::where($request->keyword, 'iLike', "%{$request->value}%")->get();
      return $this->showAll($users);
    }
}
