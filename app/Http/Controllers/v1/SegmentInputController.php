<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Inputs\ApiInput;
use App\Inputs\MocaInput;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class SegmentInputController extends Controller
{
    use ApiInput;

    public function index()
    {
        $segments = collect([]);

        $inputs = request()->get('application')->inputs->pluck('pivot', 'code');
        foreach ($inputs as $code => $info) {
          if ( is_callable(SegmentInputController::class, $code) ) {
            $segments->put($code, call_user_func_array ([SegmentInputController::class, $code], [$info->additional_data]));
          }
        }

        return $this->showAll( $segments );
    }

    public function moca($additional_data) : Collection
    {
        $this->init( new MocaInput );
        return $this->get('fields', ['appId' => $additional_data->app_id]);
    }
}
