<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Experience;

class ExperienceController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Experience  $experience
     * @return \Illuminate\Http\Response
     */
    public function show(Experience $experience)
    {
      return $this->showOne($experience);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Experience  $experience
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Experience $experience)
    {
      $this->validate($request, [
        'name'                   => 'language', 
        'description'            => 'language', 
        'message_notification'   => 'language',
        'enabled'                => 'boolean',
        'points'                 => 'integer',
        'attachement_type'       => 'string|max:5|in:'.Experience::ATTACHEMENT_TYPE_URL.','.Experience::ATTACHEMENT_TYPE_VIDEO.','.Experience::ATTACHEMENT_TYPE_IMAGE.','.Experience::ATTACHEMENT_TYPE_AUDIO,
        'geofence_key'           => 'exists_key:triggers,geofences,id,deleted_at,NULL',
        'zone_key'               => 'exists_key:triggers,zones,id,deleted_at,NULL',
        'beacon_key'             => 'exists_key:triggers,beacons,id,deleted_at,NULL',
        'repeat_key'             => 'exists_key:marketing,repeats,id,deleted_at,NULL',
        'trigger'                => 'string|in:'.Experience::TRIGGER_ENTER.','.Experience::TRIGGER_EXIT.','.Experience::TRIGGER_INMEDIATE,
        'url'                    => 'url',
        'custom_property_label'  => 'string',
        'custom_property_values' => 'array',
        'points'                 => 'integer',
        'probability'            => 'integer',
        'steps'                  => 'integer',
        'goal'                   => 'integer',
        'reminder_hours'         => 'integer',
      ]);

      $experience->fill($request->only(['name', 'description', 'message_notification', 'enabled', 'attachement_type', 'geofence_key', 'zone_key', 'beacon_key', 'repeat_key', 'trigger', 'url', 'custom_property_label', 'custom_property_values', 'points', 'probability', 'steps', 'goal', 'reminder_hours']));

      if ( $experience->isDirty() ) {
        $experience->update();
      }
      
      return $this->showOne($experience);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Experience  $experience
     * @return \Illuminate\Http\Response
     */
    public function destroy(Experience $experience)
    {
      $experience->delete();
      return $this->showOne($experience);
    }

}
