<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CustomProperty;

class CustomPropertyController extends Controller
{
  public function update(Request $request, CustomProperty $custom_property)
  {
    $this->validate($request, [
        'property'        => 'string',
        'value'           => 'string'
    ]);

    $custom_property->fill($request->only(['property', 'value']));

    if ( $custom_property->isDirty() ) {
      $custom_property->update();
    }
    
    return $this->showOne($custom_property);
  }

  public function destroy(CustomProperty $custom_property)
  {
    $custom_property->delete();
    return $this->showOne($custom_property);
  }
}

