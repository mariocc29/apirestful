<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Mail\AccountCreated;
use App\Models\Account;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Rule;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $accounts = Account::all();
        return $this->showAll($accounts);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
          'role_key' => 'required|string|exists_key:general,roles,id,deleted_at,NULL',
          'email'    => ['required', 'email', Rule::unique('general.accounts')->whereNull('deleted_at')],
          'fullname' => 'required|string',
          'picture'  => 'nullable|image'
        ]);

        $password = ( env('APP_ENV') === 'local' ) ? 'secret' : str_random( rand(5, 15) );

        $account = new Account;
        $account->app_id   = $request->get('application')->id;
        $account->role_id  = $request->role_key;
        $account->email    = $request->email;
        $account->fullname = $request->fullname;
        $account->picture  = $request->picture;
        $account->password = bcrypt( $password );
        $account->save();

        Mail::to($account)->queue( new AccountCreated($account, $password, App::getLocale()) );

        return  $this->showOne($account);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function show(Account $account)
    {
        return $this->showOne($account);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Account $account)
    {
        $this->validate($request, [
          'role_key' => 'required|string|exists_key:general,roles,id,deleted_at,NULL',
          'fullname' => 'string',
          'picture'  => 'image',
          'password' => 'string|min:6|confirmed'
        ]);

        $account->role_id  = $request->role_key;
        $account->fullname = $request->fullname;
        $account->picture  = $request->picture;
        $account->password = bcrypt( $request->password );

        if ( $account->isDirty() ) {
          $account->update();
        }
        
        return $this->showOne($account);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Account  $account
     * @return \Illuminate\Http\Response
     */
    public function destroy(Account $account)
    {
        $account->delete();
        return $this->showOne($account);
    }

    /**
     * Display a listing of the resource with conditions parameters.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $this->validate($request, [
            'keyword' => 'required|string',
            'value'   => 'required|string|min:3'
          ]);

        $accounts = Account::where($request->keyword, 'iLike', "%{$request->value}%")->get();
        return $this->showAll($accounts);
    }
}
