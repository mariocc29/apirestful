<?php

namespace App\Http\Controllers\v1;

use App\Models\Branch; 
use App\Models\User; 
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserBranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
      $branches = $user->branches;
      return $this->showAll($branches);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user, Branch $branch)
    {
      $user->branches()->syncWithoutDetaching($branch->id);
      return $this->showAll($user->branches()->get());      
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user, Branch $branch)
    {
      $user->branches()->detach([$branch->id]);
      return $this->showAll($user->branches()->get());
    }
}
