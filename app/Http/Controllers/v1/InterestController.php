<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Interest;

class InterestController extends Controller
{
  public function index()
  {
    return $this->showAll(Interest::all());
  }
}
