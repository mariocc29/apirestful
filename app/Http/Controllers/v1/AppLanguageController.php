<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Models\App;
use App\Models\Language;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class AppLanguageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(App $app)
    {
        $languages = $app->languages;
        return $this->showAll($languages);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\App  $app
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, App $app, Language $language)
    {
        $this->validate($request, [
            'default' => ['required','boolean', Rule::unique('general.app_language')->where('app_id', $app->id)->where('default', true)]
          ]);

        $app->languages()->syncWithoutDetaching([$language->id => ['default' => $request->default]]);
        return $this->showAll($app->languages()->get());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\App  $app
     * @return \Illuminate\Http\Response
     */
    public function destroy(App $app, Language $language)
    {
        $app->languages()->detach([$language->id]);
        return $this->showAll($app->languages()->get());
    }
}
