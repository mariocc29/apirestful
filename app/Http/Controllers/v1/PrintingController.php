<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Printing;

class PrintingController extends Controller
{
  public function store(Request $request)
  {
    $this->validate($request, [
      'experience_key'       => 'required|exists_key:marketing,experiences,id,deleted_at,NULL',
      'user_key'             => 'required|exists_key:general,users,id,deleted_at,NULL',
      'favorite'             => 'required|boolean',
      'saved'                => 'required|boolean'
    ]);

    dd('xxx');

    $printing = new Printing;
    $printing->experience_id = $request->experience_key;
    $printing->user_id       = $request->user_key;
    $printing->opens         = 1;
    $printing->amount        = 0;
    $printing->favorite      = $request->favorite;
    $printing->saved         = $request->saved;
    $printing->save();

    return $this->showOne($printing);
  }

  public function show(Printing $printing)
  {
    return $this->showOne($printing);
  }

  public function update(Request $request, Printing $printing)
  {
    $this->validate($request, [
      'favorite'             => 'boolean',
      'saved'                => 'boolean'
    ]);

    $printing->opens         = $printing->opens + 1;
    $printing->amount        = $printing->amount + 1;
    $printing->favorite      = $request->favorite;
    $printing->saved         = $request->saved;
    $printing->update();

    return $this->showOne($printing);
  }
}

