<?php

namespace App\Http\Controllers\v1;

use App\Models\User; 
use App\Models\CustomProperty; 
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserCustomPropertyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
      $customs_properties = $user->customs_properties;
      return $this->showAll($customs_properties);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $user)
    {
      $this->validate($request, [
          'experience_key' => 'required|string|exists_key:marketing.experiences,id,deleted_at,NULL',
          'property'       => 'required|string|max:255',
          'value'          => 'required|string|max:255'
        ]);

      $custom_property = new CustomProperty;
      $custom_property->experience_id  = $request->experience_key; 
      $custom_property->user->id       = $user->id; 
      $custom_property->property       = $request->property; 
      $custom_property->value          = $request->value; 
      $custom_property->save();

      return $this->showOne($custom_property);
    }
}
