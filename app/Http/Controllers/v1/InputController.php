<?php

namespace App\Http\Controllers\v1;

use App\Models\Input;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InputController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $inputs = Input::all();
        return $this->showAll($inputs);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'label' => 'required|string|max:50',
            'code'  => 'required|string|max:10',
          ]);

        $input = new Input;
        $input->label = $request->label;
        $input->code  = $request->code;
        $input->save();

        return $this->showOne($input);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Input  $input
     * @return \Illuminate\Http\Response
     */
    public function show(Input $input)
    {
        return $this->showOne($input);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Input  $input
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Input $input)
    {
        $this->validate($request, [
            'label'  => 'string|max:50',
            'code'   => 'string|max:10',
            'status' => 'boolean',
          ]);

        $input->fill($request->only(['label', 'code', 'status']));
        if ( $input->isDirty() ) {
          $input->update();
        }
        
        return $this->showOne($input);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Input  $input
     * @return \Illuminate\Http\Response
     */
    public function destroy(Input $input)
    {
        if ( $input->apps->count() > 0 ) {
          throw new \Exception( __('exception.children') );
        }

        $input->delete();
        return $this->showOne($input);
    }

    /**
     * Display a listing of the resource with conditions parameters.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $this->validate($request, [
            'keyword' => 'required|string',
            'value'   => 'required|string|min:3'
          ]);

        $inputs = Input::where($request->keyword, 'iLike', "%{$request->value}%")->get();
        return $this->showAll($inputs);
    }
}
