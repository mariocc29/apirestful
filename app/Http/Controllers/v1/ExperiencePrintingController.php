<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Experience;
use App\Models\Printing;

class ExperiencePrintingController extends Controller
{
  public function index(Experience $experience)
  {
    $printings = $experience->printings;
    return $this->showAll($printings);
  }

  public function show(Printing $printing)
  {
    return $this->showOne($printing);
  }

}
