<?php

namespace App\Http\Controllers\v1;

use App\Models\Category; 
use App\Models\Subcategory; 
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategorySubcategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Category $category)
    {
      $subcategories = $category->subcategories;
      return $this->showAll($subcategories);
    }

    public function search(Request $request, Category $category)
    {
      $this->validate($request, [
          'keyword' => 'required|string',
          'value'   => 'required|string|min:3', 
          'key'     => 'required|string'
        ]);

      $subcategories = $category->subcategories()->where($request->keyword.'->'.$request->key, 'iLike', "%{$request->value}%")->get();
      return $this->showAll($subcategories);
    }     
}
