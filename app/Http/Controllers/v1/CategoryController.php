<?php

namespace App\Http\Controllers\v1;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
  public function index()
  {
    return $this->showAll(Category::all());
  }

  public function store(Request $request)
  {
    $this->validate($request, [
        'label'        => 'required|language', 
        'description'  => 'nullable|language',
        'picture'      => 'image',
        'fontcolor'    => 'colorhex',
        'backcolor'    => 'colorhex',
        'category_key' => 'string|exists_key:interests,categories,id,deleted_at,NULL'
    ]);

    $cateory = new Category;
    $cateory->app_id      = $request->get('application')->id;
    $cateory->label       = $request->label;
    $cateory->description = $request->description;
    $cateory->picture     = $request->picture;
    $cateory->fontcolor   = $request->fontcolor;
    $cateory->backcolor   = $request->backcolor;
    $cateory->category_id = $request->category_key;
    $cateory->tagname     = null;
    $cateory->save();

    return $this->showOne($cateory);
  }

  public function show(Category $category)
  {
    return $this->showOne($category);
  }

  public function update(Request $request, Category $category)
  { 
    $this->validate($request, [
        'label'        => 'language', 
        'description'  => 'language',
        'picture'      => 'image',
        'fontcolor'    => 'string',
        'backcolor'    => 'string',
        'category_key' => 'string|exists_key:interests,categories,id,deleted_at,NULL'
    ]); 

    $category->label        = $request->label;
    $category->description  = $request->description;
    $category->picture      = $request->picture;
    $category->fontcolor    = $request->fontcolor;
    $category->backcolor    = $request->backcolor;
    $category->category_id  = $request->category_key;

    if ( $category->isDirty() ) {
      $category->update();
    }
    
    return $this->showOne($category);
  }

  public function destroy(Category $category)
  {
    $category->delete();
    return $this->showOne($category);
  }

  public function search(Request $request)
  {
    $this->validate($request, [
          'keyword' => 'required|string',
          'value'   => 'required|string',
          'key'     => 'required|string'
        ]);

    $categories = Category::where($request->keyword.'->'.$request->key, 'iLike', "%{$request->value}%")->get();
    return $this->showAll($categories);
  }

}
