<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Interest;
use App\Models\Product;

class CategoryProductController extends Controller
{
  public function index(Category $category)
  { 
    return $this->showAll($category->products);
  }

  public function update(Request $request, Category $category, Product $product)
  {

    $category->products()->syncWithoutDetaching([$product->id]);

    return $this->showAll($category->products()->get());
  }

  public function destroy(Category $category, Product $product)
  {
    $category->products()->detach([$product->id]);
    return $this->showAll($category->products()->get());
  }

}
