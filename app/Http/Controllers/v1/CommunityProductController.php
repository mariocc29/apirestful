<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Community;
use App\Models\Product;

class CommunityProductController extends Controller
{
  public function index(Community $community)
  {
    $products = $community->products;
    return $this->showAll($products);
  }

  public function update(Request $request, Community $community, Product $product)
  {
    $community->products()->syncWithoutDetaching($product->id);
    return $this->showAll($community->products()->get());
  }

  public function destroy(Community $community, Product $product)
  {
    $community->products()->detach([$product->id]);
    return $this->showAll($community->products()->get());
  }
}
