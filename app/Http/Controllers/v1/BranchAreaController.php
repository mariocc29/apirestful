<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Models\Area;
use App\Models\Branch;
use Illuminate\Http\Request;

class BranchAreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Branch $branch)
    {
        $areas = $branch->areas;
        return $this->showAll($areas);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Branch $branch)
    {
        $this->validate($request, [
          'picture' => 'required|image',
          'order'   => 'required|integer',
        ]);

        $area = new Area;
        $area->branch_id = $branch->id;
        $area->picture   = $request->picture;
        $area->order     = $request->order;
        $area->save();

        return $this->showOne($area);
    }
}
