<?php

namespace App\Http\Controllers\v1;

use App\Models\Segment; 
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SegmentController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Segment $segment)
    {
      return $this->showOne($segment);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Segment $segment)
    {
      $this->validate($request, [
          'label'       => 'string',
          'description' => 'string'
        ]);

        $segment->label       = $request->label;
        $segment->description = $request->description; 
      
        if ( $segment->isDirty() ) {
          $segment->update();
        }

        return $this->showOne($segment);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Segment $segment)
    {
      $segment->delete();
      return $this->showOne($segment);
    }
}
