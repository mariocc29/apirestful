<?php

namespace App\Http\Controllers\v1;
 
use App\Models\Product;
use App\Models\Campaign;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductCampaignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Product $product)
    {
      $campaigns = $product->campaigns;
      return $this->showAll($campaigns);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Product $product)
    {
      $this->validate($request, [
          'campaign_type_key' => 'required|string|exists_key:general,campaigns_types,id,deleted_at,NULL',
          'segment_key'      => 'nullable|string|exists_key:marketing,segments,id,deleted_at,NULL',
          'name'             => 'required|language', 
          'description'      => 'required|language', 
          'start_at'         => 'required|date', 
          'end_at'           => 'required|date', 
          'schedule'         => 'required|schedule',
          'status'           => 'required|boolean'
        ]);

        $campaign = new Campaign;
        $campaign->campaign_type_id = $request->campaign_type_key;
        $campaign->product_id       = $product->id; 
        $campaign->segment_id       = $request->segment_key; 
        $campaign->name             = $request->name; 
        $campaign->description      = $request->description; 
        $campaign->start_at         = $request->start_at; 
        $campaign->end_at           = $request->end_at; 
        $campaign->schedule         = $request->schedule; 
        $campaign->status           = $request->status; 
        $campaign->save();

        return $this->showOne($campaign);
    }

    public function search(Request $request, Product $product)
    {
      $this->validate($request, [
          'keyword' => 'required|string',
          'value'   => 'required|string|min:3', 
          'key'     => 'required|string'
        ]);

      $campaigns = $product->campaigns()->where($request->keyword.'->'.$request->key, 'iLike', "%{$request->value}%")->get();
      return $this->showAll($campaigns);
    }
}
