<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Gallery;

class GalleryController extends Controller
{
  public function update(Request $request, Gallery $gallery)
  {
    $this->validate($request, [
      'filename'             => 'string', 
      'additional_data'      => 'array'
    ]);

    $gallery->fill($request->only(['filename', 'additional_data']));

    if ( $gallery->isDirty() ) {
      $gallery->update();
    }
    
    return $this->showOne($gallery);
  }

  public function destroy(Gallery $gallery)
  {
    $gallery->delete();
    return $this->showOne($gallery);
  }
}

