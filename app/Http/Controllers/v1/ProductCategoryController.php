<?php

namespace App\Http\Controllers\v1;

use App\Models\Product; 
use App\Models\Category; 
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Product $product)
    {
      $categories = $product->categories;
      return $this->showAll($categories);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product, Category $category)
    {
      $product->categories()->syncWithoutDetaching($category->id);
      return $this->showAll($product->categories()->get());            
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product, Category $category)
    {
      $product->categories()->detach([$category->id]);
      return $this->showAll($product->categories()->get());
    }
}
