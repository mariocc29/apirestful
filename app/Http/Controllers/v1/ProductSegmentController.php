<?php

namespace App\Http\Controllers\v1;

use App\Models\Segment; 
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductSegmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Product $product)
    {
      $segments = $product->segments;
      return $this->showAll($segments);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Product $product)
    {
      $this->validate($request, [
          'label'       => 'required|string|max:255',
          'description' => 'required|string'
        ]);

        $segment = new Segment;
        $segment->product_id  = $product->id; 
        $segment->label       = $request->label; 
        $segment->description = $request->description;  
        $segment->save();

        return $this->showOne($segment);
    }

    public function search(Request $request, Product $product)
    {
      $this->validate($request, [
          'keyword' => 'required|string',
          'value'   => 'required|string|min:3'
        ]);

      $segments = $product->segments()->where($request->keyword, 'iLike', "%{$request->value}%")->get();
      return $this->showAll($segments);
    }      
}
