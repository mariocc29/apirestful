<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Notification;

class NotificationController extends Controller
{
  public function show(Notification $notification)
  {
    return $this->showOne($product);
  } 

  public function destroy(Notification $notification)
  {
    $notification->delete();
    return $this->showOne($notification);
  }
}
