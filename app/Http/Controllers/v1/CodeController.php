<?php

namespace App\Http\Controllers\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Code;

class CodeController extends Controller
{
  public function store(Request $request)
  {
    $this->validate($request, [
        'experience_key'  => 'required|exists_key:marketing,experiences,id,deleted_at,NULL', 
        'code'            => 'required|string',
        'status'          => 'required|string',
        'user_key'        => 'required|exists_key:general,users,id,deleted_at,NULL'
    ]);

    $code = new Code;
    $code->experience_id = $request->experience_key;
    $code->code          = $request->code;
    $code->status        = $request->status;
    $code->user_id       = $request->user_key; 
    $code->save();

    return $this->showOne($code);
  }

  public function update(Request $request, Code $code)
  {
    $this->validate($request, [ 
        'code'            => 'string',
        'status'          => 'string'
    ]);

    $code->fill($request->only(['code', 'status']));

    if ( $code->isDirty() ) {
      $code->update();
    }
    
    return $this->showOne($code);
  }

  public function destroy(Code $code)
  {
    $code->delete();
    return $this->showOne($code);
  }
  
}


    