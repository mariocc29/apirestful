<?php

namespace App\Mail;

use App\Models\Account;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\App;

class AccountCreated extends Mailable
{
    use Queueable, SerializesModels;

    public $account;

    public $password;

    public $locale;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Account $account, string $password, string $locale = null)
    {
        $this->account  = $account;

        $this->password = $password;
        
        $this->locale   = ( !is_null($locale) ) ? $locale : App::getLocale();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->text("emails.{$this->locale}.accountCreated")->subject( __('emails.accounts.new') );
    }
}
