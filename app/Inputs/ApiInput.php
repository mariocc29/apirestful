<?php

namespace App\Inputs;

use Illuminate\Support\Collection;

/**
 * 
 */
trait ApiInput
{
  
  private $input;

  protected function init(iInput $input)
  {
    $this->input = $input;
  }

  protected function get(string $path, array $querystring = [])
  {
    return $this->input->index($path, $querystring);
  }

  protected function push(string $path, Collection $payload)
  {
    return $this->input->store($path, $payload);
  }

  protected function getOne(string $path, $id)
  {
    return $this->input->show($path, $id);
  }

  protected function put(string $path, Collection $payload, $id)
  {
    return $this->input->update($path, $payload, $id);
  }

  protected function delete(string $path, $id)
  {
    return $this->input->destroy($path, $id);
  }
}