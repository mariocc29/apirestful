<?php

namespace App\Inputs;

use Illuminate\Support\Collection;

interface iInput
{
    /**
     * Obtiene el listado de un recurso
     * @return collection Una colección de los recursos
     */
    public function index(string $path, array $querystring) : Collection;
    
    /**
     * Almacena un recurso recién creado en el servicio
     * @return collection Una colección de los recursos
     */
    public function store(string $path, Collection $payload) : Collection;

    /**
     * Obtiene un recurso específico
     * @return collection Una colección de los recursos
     */
    public function show(string $path, $id) : Collection;

    /**
     * Actualiza un recurso recién creado en el servicio
     * @return collection Una colección de los recursos
     */
    public function update(string $path, Collection $payload, $id) : Collection;

    /**
     * Elimina un recurso recién creado en el servicio
     * @return collection Una colección de los recursos
     */
    public function destroy(string $path, $id) : bool;
}
