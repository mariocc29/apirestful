<?php

namespace App\Inputs;

use App\Helpers\ApiLog;
use App\Inputs\iInput;
use App\Models\Input;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use Illuminate\Support\Collection;

class MocaInput implements iInput {

  const BASE_PATH  = 'http://api.mocaplatform.com/v2/';

  protected $account_settings;

  public function __construct(){
    $this->account_settings = request()->get('application')->inputs->where('code', Input::MOCA)->pluck('pivot')->first()->additional_data;
  }

  private static function base_path (){
    $client = new Client([ 'base_uri' => MocaInput::BASE_PATH ]);
    return $client;
  }

  public function index(string $path, array $query = null) : Collection
  {
    $data = [];
    try {
      $client      = MocaInput::base_path();
      $querystring = ( count($query) > 0 ) ? '?'.http_build_query($query) : null;
      $response    = $client->get("{$path}/{$querystring}",[
            'headers' => ['Authorization' => "Bearer {$this->account_settings->token}"]
          ]);
      $data     = json_decode($response->getBody()->getContents(), true);
    } catch (ClientException $e) {
      $formatter = ApiLog::errorFormatter($e);
      ApiLog::error('EndpointMocaInput', $formatter->getLogMessage());
    } catch (ConnectException $e){
      $formatter = ApiLog::errorFormatter($e);
      ApiLog::error('EndpointMocaInput', $formatter->getLogMessage());
    }
    
    return collect( $data );
  }

  public function store(string $path, Collection $payload) : Collection
  {
    $data = [];

    try {
      $client   = MocaInput::base_path();
      $response = $client->post($path,[
            'headers' => ['Authorization' => "Bearer {$this->account_settings->token}"],
            'json'    => $payload->toArray()
          ]);
      $data     = json_decode($response->getBody()->getContents(), true);
    } catch (ClientException $e) {
      $formatter = ApiLog::errorFormatter($e);
      $formatter->setPayload( $payload->toJson() );
      ApiLog::error('EndpointMocaInput', $formatter->getLogMessage());
    } catch (ConnectException $e){
      $formatter = ApiLog::errorFormatter($e);
      $formatter->setPayload( $payload->toJson() );
      ApiLog::error('EndpointMocaInput', $formatter->getLogMessage());
    }

    return collect( $data );
  }

  public function show(string $path, $id) : Collection
  {
    $data = [];

    try {
      $client   = MocaInput::base_path();
      $response = $client->get("{$path}/{$id}",[
            'headers' => ['Authorization' => "Bearer {$this->account_settings->token}"]
          ]);
      $data     = json_decode($response->getBody()->getContents(), true);
    } catch (ClientException $e) {
      $formatter = ApiLog::errorFormatter($e);
      ApiLog::error('EndpointMocaInput', $formatter->getLogMessage());
    } catch (ConnectException $e){
      $formatter = ApiLog::errorFormatter($e);
      ApiLog::error('EndpointMocaInput', $formatter->getLogMessage());
    }

    return collect( $data );
  }

  public function update(string $path, Collection $payload, $id) : Collection
  {
    $data = [];

    try {
      $client   = MocaInput::base_path();
      $response = $client->put("{$path}/{$id}",[
            'headers' => ['Authorization' => "Bearer {$this->account_settings->token}"],
            'json'    => $payload->toArray()
          ]);
      $data     = json_decode($response->getBody()->getContents(), true);
    } catch (ClientException $e) {
      $formatter = ApiLog::errorFormatter($e);
      $formatter->setPayload( $payload->toJson() );
      ApiLog::error('EndpointMocaInput', $formatter->getLogMessage());
    } catch (ConnectException $e){
      $formatter = ApiLog::errorFormatter($e);
      $formatter->setPayload( $payload->toJson() );
      ApiLog::error('EndpointMocaInput', $formatter->getLogMessage());
    }

    return collect( $data );
  }

  public function destroy(string $path, $id) : bool
  {
    $result = false;

    try {
      $client   = MocaInput::base_path();
      $response = $client->delete("{$path}/{$id}",[
            'headers' => ['Authorization' => "Bearer {$this->account_settings->token}"]
          ]);
      $result   = true;
    } catch (ClientException $e) {
      $formatter = ApiLog::errorFormatter($e);
      ApiLog::error('EndpointMocaInput', $formatter->getLogMessage());
    } catch (ConnectException $e){
      $formatter = ApiLog::errorFormatter($e);
      ApiLog::error('EndpointMocaInput', $formatter->getLogMessage());
    }

    return $result;
  }

}