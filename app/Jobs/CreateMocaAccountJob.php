<?php

namespace App\Jobs;

use App\Models\App;
use App\Models\Input;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;
use Symfony\Component\HttpFoundation\Response;

class CreateMocaAccountJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    const AUTH_PATH  = 'https://www.getpostman.com/collections/';

    const TOKEN_PATH = 'https://api.mocaplatform.com/';

    protected $app;

    protected $input;
    
    protected $request;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(App $app, Input $input, $request)
    {
        $this->app     = $app;
        $this->input   = $input;
        $this->request = $request;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $authorization = $this->generateAuthorization();
        if ( !is_null($authorization) ){
          $access_token = $this->getAccessToken($authorization);
          $this->app->inputs()->syncWithoutDetaching([$this->input->id => [
              'additional_data' => json_encode([
                  'account'    => $this->request->email,
                  'password'   => $this->request->password,
                  'token'      => $access_token,
                  'account_id' => $this->request->password,
                  'app_id'     => $this->request->password,
                ])
            ]]);
        }
    }

    private function generateAuthorization()
    {
        try {

          $client   = new Client([ 'base_uri' => CreateMocaAccountJob::AUTH_PATH ]);
          $response = retry(2, function() use ($client){
              return $client->get($this->request->key);
            }, 100);

          $data  = json_decode($response->getBody()->getContents());
          $oauth = collect( array_filter( explode(PHP_EOL, $data->requests[0]->headers), function($header){
              return ( strpos($header, 'Authorization') > -1 ) ? true : false;
            }));

          return trim(explode(':', $oauth->first())[1]);

        } catch (ClientException $e) {
          $formatter = ApiLog::errorFormatter($e);
          ApiLog::error('CreateMocaAccountJob', $formatter->getLogMessage());
        } catch (ConnectException $e){
          $formatter = ApiLog::errorFormatter($e);
          ApiLog::error('CreateMocaAccountJob', $formatter->getLogMessage());
        }        
    }

    private function getAccessToken($authorization)
    {
      try {
        $client = new Client([ 'base_uri' => CreateMocaAccountJob::TOKEN_PATH ]);
        $path   = "oauth/token?grant_type=password&username={$this->request->email}&password={$this->request->password}";
        $response = retry(2, function() use($client, $path, $authorization){
            return $client->post($path,[ 'headers' => ['Authorization' => $authorization] ]);
          }, 100);

        if ( $response->getStatusCode() !== Response::HTTP_OK ) {
          throw new \Exception("Error {$response->getStatusCode()}");
        }
        
        $oauth = json_decode($response->getBody()->getContents());
        return $oauth->access_token;

      } catch (ClientException $e) {
        $formatter = ApiLog::errorFormatter($e);
        ApiLog::error('CreateMocaAccount', $formatter->getLogMessage());
      } catch (ConnectException $e){
        $formatter = ApiLog::errorFormatter($e);
        ApiLog::error('CreateMocaAccount', $formatter->getLogMessage());
      }
    }
}
