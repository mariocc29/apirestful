<?php

namespace App\Jobs;

use App\Mail\AppCreated;
use App\Models\App;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class CreateServerAuthorization implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $app;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(App $app)
    {
        $this->app = $app;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $s2s = new \stdClass;
        $s2s->token_type   = 'Bearer';
        $s2s->access_token = $this->app->createToken(App::class, ['server_read', 'server_write'])->accessToken;

        $additional_data = collect( (array)$this->app->additional_data );
        $additional_data->put('s2s', $s2s);
        
        $this->app->additional_data = $additional_data->toArray();
        $this->app->update();

        Mail::to($this->app)->send( new AppCreated($this->app) );
    }
}
