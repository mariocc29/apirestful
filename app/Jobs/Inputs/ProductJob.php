<?php

namespace App\Jobs\Inputs;

use App\Events\InputMocaEvent;
use App\Jobs\Inputs\InputJob;
use App\Models\Product;
use Faker\Factory as Faker;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ProductJob extends InputJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $product;

    protected $trigger; //created, updated, deleted

    protected $faker;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Product $product, string $trigger)
    {
        $this->product = $product;
        
        $this->trigger = $trigger;
        
        $this->faker   = Faker::create();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Obtiene todos los input de una aplicación
        $inputs = $this->product->app->inputs->pluck('pivot', 'code');

        foreach ($inputs as $code => $info) {
          // Si la función existe, ejecuta el post correspondiente
          if( is_callable([ProductJob::class, $code]) ){
            // Según cada input, realiza la tarea
            call_user_func_array ([ProductJob::class, $code], [$info->additional_data]);
          }
        }
    }

    /**
     * Conexión con el proveedor MOCA para ucontext para cargar los tags de los productos en el app
     * @param  json $additional_data Información de la conexión con el proveedor
     * @return void                  Crea el código
     */
    private function moca($additional_data)
    {
        $payload = collect([]);

        if ($this->trigger !== InputJob::TRIGGER_DELETED) {
          $payload = collect([
            'name'        => $this->product->tagname,
            'displayName' => $this->product->tagname,
            'color'       => $this->faker->hexcolor,
            'hidden'      => false,
          ]);
        }
        
        switch ($this->trigger) {
          case InputJob::TRIGGER_CREATED:
            
              $path   = "tags/?appId={$additional_data->app_id}";
              $result = collect( event( new InputMocaEvent(InputJob::TRIGGER_CREATED, $path, $payload) ) )->first();

              if ( !is_null( $result->get('tagId') ) ) {
                
                Product::flushEventListeners();

                $input = collect( ( !is_null($this->product->input) ) ? $this->product->input : [] );
                $input->put('moca', $result->get('tagId'));
                $this->product->input = $input->toArray();
                $this->product->update();
              }
            
            break;
          
          case InputJob::TRIGGER_DELETED:
              if (is_null($this->product->input) || !isset($this->product->input->moca)) {
                throw new \Exception( $this->getExceptionMessage('MOCA_ID not exists', Product::class, $this->product->id, __CLASS__) );
              }
              
              event( new InputMocaEvent(InputJob::TRIGGER_DELETED, 'tags', $payload, $this->product->input->moca) );
            break;
        }
    }
}
