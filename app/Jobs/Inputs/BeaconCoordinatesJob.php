<?php

namespace App\Jobs\Inputs;

use App\Events\InputMocaEvent;
use App\Jobs\Inputs\InputJob;
use App\Models\Beacon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class BeaconCoordinatesJob extends InputJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $beacon;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Beacon $beacon)
    {
        $this->beacon = $beacon;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Obtiene todos los input de una aplicación
        $inputs = $this->beacon->app->inputs->pluck('pivot', 'code');

        foreach ($inputs as $code => $info) {
          // Si la función existe, ejecuta el post correspondiente
          if( is_callable([BeaconCoordinatesJob::class, $code]) ){
            // Según cada input, realiza la tarea
            call_user_func_array ([BeaconCoordinatesJob::class, $code], [$info->additional_data]);
          }
        }
    }

    /**
     * Conexión con el proveedor MOCA para ucontext para cargar beacons en el app
     * @param  json $additional_data Información de la conexión con el proveedor
     * @return void                  Crea el código
     */
    private function moca($additional_data)
    {
        if (is_null($this->beacon->input) || !isset($this->beacon->input->moca)) {
          throw new \Exception( $this->getExceptionMessage('MOCA_ID not exists', Beacon::class, $this->beacon->id, __CLASS__) );
        }

        if ( !is_null($this->beacon->area_id) ) {
          $payload = collect([
              'latitude'        => $this->beacon->geo_position->latitude,
              'longitude'       => $this->beacon->geo_position->longitude,
              'floor'           => $this->beacon->area->order,
              'heightOverFloor' => 3
            ]);
          
          event( new InputMocaEvent(InputJob::TRIGGER_UPDATED, "beacons", $payload, "{$this->beacon->input->moca}/coordinates") );
        } else {
          event( new InputMocaEvent(InputJob::TRIGGER_DELETED, 'beacons', collect([]), "{$this->beacon->input->moca}/coordinates") );
        }
    }
}
