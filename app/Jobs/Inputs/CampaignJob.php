<?php

namespace App\Jobs\Inputs;

use App\Events\InputMocaEvent;
use App\Jobs\Inputs\InputJob;
use App\Models\Campaign;
use App\Transformers\ScheduleTransformer;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CampaignJob extends InputJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $campaign;

    protected $trigger; //created, updated, deleted

    protected $default_language;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Campaign $campaign, string $trigger)
    {
        $this->campaign = $campaign;
        
        $this->trigger  = $trigger;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Obtiene el lenguaje por defecto de la campaña
        $this->default_language = $this->campaign->product->app->languages->where('pivot.default', true)->first()->iso;
        
        // Obtiene todos los input de una aplicación
        $inputs = $this->campaign->product->app->inputs->pluck('pivot', 'code');

        foreach ($inputs as $code => $info) {
          // Si la función existe, ejecuta el post correspondiente
          if( is_callable([CampaignJob::class, $code]) ){
            // Según cada input, realiza la tarea
            call_user_func_array ([CampaignJob::class, $code], [$info->additional_data]);
          }
        }
    }

    /**
     * Conexión con el proveedor MOCA para ucontext para cargar beacons en el app
     * @param  json $additional_data Información de la conexión con el proveedor
     * @return void                  Crea el código
     */
    private function moca($additional_data)
    {
        $payload = collect([]);

        if ($this->trigger !== InputJob::TRIGGER_DELETED) {
          
          $segment_id = null;
          if ( !is_null($this->campaign->segment) ) {
            $segment_id = $this->campaign->segment->input->moca;
          }

          // init Obtiene los días de la semana que se ejecuta la campaña
          $week       = collect(['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat']);
          $schedule   = collect( (array)$this->campaign->schedule );
          $daysOfWeek = collect([]);
          foreach ($schedule->keys() as $day) {
            $daysOfWeek->push( $week->search($day) );
          }
          // end Obtiene los días de la semana que se ejecuta la campaña

          // init Obtiene las horas en las que se ejecuta la campaña
          $start_at   = date("G",strtotime($schedule->first()->start_at));
          $end_at     = (int) date("G",strtotime($schedule->first()->end_at));
          $hoursOfDay = range((int) $start_at, (int) $end_at);
          // end Obtiene las horas en las que se ejecuta la campaña

          $payload = collect([
            'name'          => $this->campaign->name->{$this->default_language},
            'description'   => $this->campaign->description->{$this->default_language},
            'type'          => 'Proximity',
            'startAt'       => $this->campaign->start_at->timestamp * 1000,
            'endAt'         => $this->campaign->end_at->timestamp * 1000,
            'segmentId'     => $segment_id,
            'recurrence'    => [
              'hoursOfDay' => $hoursOfDay,
              'daysOfWeek' => $daysOfWeek->toArray(),
            ],
          ]);
        }
        
        switch ($this->trigger) {
          case InputJob::TRIGGER_CREATED:
            
              $path   = "campaigns/?appId={$additional_data->app_id}";
              $result = collect( event( new InputMocaEvent(InputJob::TRIGGER_CREATED, $path, $payload) ) )->first();

              if ( !is_null( $result->get('campaignId') ) ) {
                
                Campaign::flushEventListeners();

                $input = collect( ( !is_null($this->campaign->input) ) ? $this->campaign->input : [] );
                $input->put('moca', $result->get('campaignId'));
                $this->campaign->input = $input->toArray();
                $this->campaign->update();
              }
            
            break;
          
          case InputJob::TRIGGER_UPDATED:
              if (is_null($this->campaign->input) || !isset($this->campaign->input->moca)) {
                throw new \Exception( $this->getExceptionMessage('MOCA_ID not exists', Campaign::class, $this->campaign->id, __CLASS__) );
              }

              event( new InputMocaEvent(InputJob::TRIGGER_UPDATED, 'campaigns', $payload, $this->campaign->input->moca) );
            break;
          
          case InputJob::TRIGGER_DELETED:
              if (is_null($this->campaign->input) || !isset($this->campaign->input->moca)) {
                throw new \Exception( $this->getExceptionMessage('MOCA_ID not exists', Campaign::class, $this->campaign->id, __CLASS__) );
              }
              
              event( new InputMocaEvent(InputJob::TRIGGER_DELETED, 'campaigns', $payload, $this->campaign->input->moca) );
            break;
        }

    }
}
