<?php

namespace App\Jobs\Inputs;

abstract class InputJob
{

  const TRIGGER_CREATED = 'created';
  const TRIGGER_UPDATED = 'updated';
  const TRIGGER_DELETED = 'deleted';

  protected function getExceptionMessage( $exception, $model, $resource_id, $job )
  {
      $message = "EXCEPTION[{$exception}]|MODEL[{$model}]|ID[{$resource_id}]|JOB[{$job}]";
      return $message;
  }

}