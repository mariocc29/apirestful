<?php

namespace App\Jobs\Inputs;

use App\Events\InputMocaEvent;
use App\Jobs\Inputs\InputJob;
use App\Models\Notification;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class NotificationJob extends InputJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $notification;

    protected $trigger; //created, updated, deleted

    protected $default_language;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Notification $notification, string $trigger)
    {
        $this->notification = $notification;
        
        $this->trigger      = $trigger;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Obtiene el lenguaje por defecto de la campaña
        $this->default_language = $this->notification->product->app->languages->where('pivot.default', true)->first()->iso;

        // Obtiene todos los input de una aplicación
        $inputs = $this->notification->product->app->inputs->pluck('pivot', 'code');

        foreach ($inputs as $code => $info) {
          // Si la función existe, ejecuta el post correspondiente
          if( is_callable([NotificationJob::class, $code]) ){
            // Según cada input, realiza la tarea
            call_user_func_array ([NotificationJob::class, $code], [$info->additional_data]);
          }
        }
    }

    /**
     * Conexión con el proveedor MOCA para ucontext para cargar beacons en el app
     * @param  json $additional_data Información de la conexión con el proveedor
     * @return void                  Crea el código
     */
    private function moca($additional_data)
    {
        $payload = collect([]);

        if ($this->trigger !== InputJob::TRIGGER_DELETED) {

          $audience = ['type' => 'All'];
          if ( !is_null($this->notification->segment_id) ) {
             $audience = [
                  'type' => 'Query',
                  'query' => json_encode(['$all' => $this->notification->segment->audience->toArray()]),
                ];
          }
           
          $payload = collect([
            'config'   => [
                'name' => $this->notification->title->{$this->default_language},
              ],
            'trigger'  => [
                '@type' => 'TimeTrigger',
                'time'  => $this->notification->send_at->timestamp * 1000,
              ],
            'audience' => $audience,
            'action'   => [
                '@type'       => 'OpenUrlAction',
                "contentType" => "text/x-url",
                'content'     => [
                    'default' => "{$this->notification->product->app->deep_link}/?id={$this->notification->key}&type=notification",
                  ],
                'bgAlert'     => [
                    'default' => $this->notification->message->{$this->default_language},
                  ]
              ],
            'draft'    => false,
          ]);
        }
        
        switch ($this->trigger) {
          case InputJob::TRIGGER_CREATED:
            
              $path   = "notifications/?appId={$additional_data->app_id}";
              $result = collect( event( new InputMocaEvent(InputJob::TRIGGER_CREATED, $path, $payload) ) )->first();

              if ( !is_null( $result->get('notificationId') ) ) {
                
                Notification::flushEventListeners();

                $input = collect( ( !is_null($this->notification->input) ) ? $this->notification->input : [] );
                $input->put('moca', $result->get('notificationId'));
                $this->notification->input = $input->toArray();
                $this->notification->update();
              }
            
            break;
          
          case InputJob::TRIGGER_UPDATED:
              if (is_null($this->notification->input) || !isset($this->notification->input->moca)) {
                throw new \Exception( $this->getExceptionMessage('MOCA_ID not exists', Notification::class, $this->notification->id, __CLASS__) );
              }

              event( new InputMocaEvent(InputJob::TRIGGER_UPDATED, 'notifications', $payload, $this->notification->input->moca) );
            break;
          
          case InputJob::TRIGGER_DELETED:
              if (is_null($this->notification->input) || !isset($this->notification->input->moca)) {
                throw new \Exception( $this->getExceptionMessage('MOCA_ID not exists', Notification::class, $this->notification->id, __CLASS__) );
              }
              
              event( new InputMocaEvent(InputJob::TRIGGER_DELETED, 'notifications', $payload, $this->notification->input->moca) );
            break;
        }

    }
}
