<?php

namespace App\Jobs\Inputs;

use App\Events\InputMocaEvent;
use App\Jobs\Inputs\InputJob;
use App\Models\Geofence;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class GeofenceJob extends InputJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $geofence;

    protected $trigger; //created, updated, deleted

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Geofence $geofence, string $trigger)
    {
        $this->geofence = $geofence;
        
        $this->trigger  = $trigger;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Obtiene todos los input de una aplicación
        $inputs = $this->geofence->product->app->inputs->pluck('pivot', 'code');

        foreach ($inputs as $code => $info) {
          // Si la función existe, ejecuta el post correspondiente
          if( is_callable([GeofenceJob::class, $code]) ){
            // Según cada input, realiza la tarea
            call_user_func_array ([GeofenceJob::class, $code], [$info->additional_data]);
          }
        }
    }

    /**
     * Conexión con el proveedor MOCA para ucontext para cargar places en el app
     * @param  json $additional_data Información de la conexión con el proveedor
     * @return void                  Crea el código
     */
    private function moca($additional_data)
    {
        $payload = collect([]);

        if ($this->trigger !== InputJob::TRIGGER_DELETED) {
          $payload = collect([
            'name'        => $this->geofence->label,
            'description' => $this->geofence->description,
            'geoFence'    => [
              'latitude'  => $this->geofence->geo_position->latitude,
              'longitude' => $this->geofence->geo_position->longitude,
              'radius'    => $this->geofence->radius
            ],
          ]);
        }
        
        switch ($this->trigger) {
          case InputJob::TRIGGER_CREATED:
            
              $path   = "places/?accountId={$additional_data->account_id}";
              $result = collect( event( new InputMocaEvent(InputJob::TRIGGER_CREATED, $path, $payload) ) )->first();

              if ( !is_null( $result->get('placeId') ) ) {
                
                Geofence::flushEventListeners();

                $input = collect( ( !is_null($this->geofence->input) ) ? $this->geofence->input : [] );
                $input->put('moca', $result->get('placeId'));
                $this->geofence->input = $input->toArray();
                $this->geofence->update();
              }
            
            break;
          
          case InputJob::TRIGGER_UPDATED:
              if (is_null($this->geofence->input) || !isset($this->geofence->input->moca)) {
                throw new \Exception( $this->getExceptionMessage('MOCA_ID not exists', Geofence::class, $this->geofence->id, __CLASS__) );
              }

              event( new InputMocaEvent(InputJob::TRIGGER_UPDATED, 'places', $payload, $this->geofence->input->moca) );
            break;
          
          case InputJob::TRIGGER_DELETED:
              if (is_null($this->geofence->input) || !isset($this->geofence->input->moca)) {
                throw new \Exception( $this->getExceptionMessage('MOCA_ID not exists', Geofence::class, $this->geofence->id, __CLASS__) );
              }
              
              event( new InputMocaEvent(InputJob::TRIGGER_DELETED, 'places', $payload, $this->geofence->input->moca) );
            break;
        }

    }
}
