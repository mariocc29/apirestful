<?php

namespace App\Jobs\Inputs;

use App\Events\InputMocaEvent;
use App\Jobs\Inputs\InputJob;
use App\Models\Category;
use Faker\Factory as Faker;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CategoryJob extends InputJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $category;

    protected $trigger; //created, updated, deleted

    protected $faker;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Category $category, string $trigger)
    {
        $this->category = $category;
        
        $this->trigger  = $trigger;

        $this->faker    = Faker::create();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Obtiene todos los input de una aplicación
        $inputs = $this->category->app->inputs->pluck('pivot', 'code');

        foreach ($inputs as $code => $info) {
          // Si la función existe, ejecuta el post correspondiente
          if( is_callable([CategoryJob::class, $code]) ){
            // Según cada input, realiza la tarea
            call_user_func_array ([CategoryJob::class, $code], [$info->additional_data]);
          }
        }
    }

    /**
     * Conexión con el proveedor MOCA para ucontext para cargar los tags de las categorías en el app
     * @param  json $additional_data Información de la conexión con el proveedor
     * @return void                  Crea el código
     */
    private function moca($additional_data)
    {
        $payload = collect([]);

        if ($this->trigger !== InputJob::TRIGGER_DELETED) {
          $payload = collect([
            'name'        => $this->category->tagname,
            'displayName' => $this->category->tagname,
            'color'       => $this->faker->hexcolor,
            'hidden'      => false,
          ]);
        }
        
        switch ($this->trigger) {
          case InputJob::TRIGGER_CREATED:
            
              $path   = "tags/?appId={$additional_data->app_id}";
              $result = collect( event( new InputMocaEvent(InputJob::TRIGGER_CREATED, $path, $payload) ) )->first();

              if ( !is_null( $result->get('tagId') ) ) {
                
                Category::flushEventListeners();

                $input = collect( ( !is_null($this->category->input) ) ? $this->category->input : [] );
                $input->put('moca', $result->get('tagId'));
                $this->category->input = $input->toArray();
                $this->category->update();
              }
            
            break;
          
          case InputJob::TRIGGER_DELETED:

              if (is_null($this->category->input) || !isset($this->category->input->moca)) {
                throw new \Exception( $this->getExceptionMessage('MOCA_ID not exists', Category::class, $this->category->id, __CLASS__) );
              }
              
              event( new InputMocaEvent(InputJob::TRIGGER_DELETED, 'tags', $payload, $this->category->input->moca) );
            break;
        }
    }
}
