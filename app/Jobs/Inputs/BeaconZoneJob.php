<?php

namespace App\Jobs\Inputs;

use App\Events\InputMocaEvent;
use App\Jobs\Inputs\InputJob;
use App\Models\Beacon;
use App\Models\Zone;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class BeaconZoneJob extends InputJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $beacon;

    protected $zone;

    protected $trigger;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Beacon $beacon, Zone $zone, string $trigger)
    {
        $this->beacon  = $beacon;
        
        $this->zone    = $zone;
        
        $this->trigger = $trigger;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Obtiene todos los input de una aplicación
        $inputs = $this->beacon->app->inputs->pluck('pivot', 'code');

        foreach ($inputs as $code => $info) {
          // Si la función existe, ejecuta el post correspondiente
          if( is_callable([BeaconZoneJob::class, $code]) ){
            // Según cada input, realiza la tarea
            call_user_func_array ([BeaconZoneJob::class, $code], [$info->additional_data]);
          }
        }
    }

    /**
     * Conexión con el proveedor MOCA para ucontext para cargar beacons en el app
     * @param  json $additional_data Información de la conexión con el proveedor
     * @return void                  Crea el código
     */
    private function moca($additional_data)
    {
        if (is_null($this->beacon->input) || !isset($this->beacon->input->moca)) {
          throw new \Exception( $this->getExceptionMessage('MOCA_ID not exists', Beacon::class, $this->beacon->id, __CLASS__) );
        }

        if (is_null($this->zone->input) || !isset($this->zone->input->moca)) {
          throw new \Exception( $this->getExceptionMessage('MOCA_ID not exists', Zone::class, $this->zone->id, __CLASS__) );
        }

        if ( $this->trigger === InputJob::TRIGGER_UPDATED ) {         
          event( new InputMocaEvent(InputJob::TRIGGER_UPDATED, "beacons/{$this->beacon->input->moca}", collect([]), "zone?zoneId={$this->zone->input->moca}") );
        } else {
          event( new InputMocaEvent(InputJob::TRIGGER_DELETED, "beacons/{$this->beacon->input->moca}", collect([]), "zone") );
        }
    }
}
