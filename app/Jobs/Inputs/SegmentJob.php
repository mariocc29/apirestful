<?php

namespace App\Jobs\Inputs;

use App\Events\InputMocaEvent;
use App\Jobs\Inputs\InputJob;
use App\Models\Segment;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SegmentJob extends InputJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $segment;

    protected $trigger; //created, updated, deleted

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Segment $segment, string $trigger)
    {
        $this->segment = $segment;
        
        $this->trigger = $trigger;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Obtiene todos los input de una aplicación
        $inputs = $this->segment->product->app->inputs->pluck('pivot', 'code');

        foreach ($inputs as $code => $info) {
          // Si la función existe, ejecuta el post correspondiente
          if( is_callable([SegmentJob::class, $code]) ){
            // Según cada input, realiza la tarea
            call_user_func_array ([SegmentJob::class, $code], [$info->additional_data]);
          }
        }
    }

    /**
     * Conexión con el proveedor MOCA para ucontext para cargar los segmentos en el app
     * @param  json $additional_data Información de la conexión con el proveedor
     * @return void                  Crea el código
     */
    private function moca($additional_data)
    {
        $payload = collect([]);

        if ($this->trigger !== InputJob::TRIGGER_DELETED) {
          $payload = collect([
            'name'        => "({$this->segment->product->tagname}) {$this->segment->label}",
            'description' => $this->segment->description,
            'query'       => json_encode(['$all' => $this->segment->audience->toArray()]),
          ]);
        }
        
        switch ($this->trigger) {
          case InputJob::TRIGGER_CREATED:
            
              $path   = "segments/?appId={$additional_data->app_id}";
              $result = collect( event( new InputMocaEvent(InputJob::TRIGGER_CREATED, $path, $payload) ) )->first();

              if ( !is_null( $result->get('segmentId') ) ) {
                
                Segment::flushEventListeners();

                $input = collect( ( !is_null($this->segment->input) ) ? $this->segment->input : [] );
                $input->put('moca', $result->get('segmentId'));
                $this->segment->input = $input->toArray();
                $this->segment->update();
              }
            
            break;

          case InputJob::TRIGGER_UPDATED:
              if (is_null($this->segment->input) || !isset($this->segment->input->moca)) {
                throw new \Exception( $this->getExceptionMessage('MOCA_ID not exists', Segment::class, $this->segment->id, __CLASS__) );
              }

              event( new InputMocaEvent(InputJob::TRIGGER_UPDATED, 'segments', $payload, $this->segment->input->moca) );
            break;
          
          case InputJob::TRIGGER_DELETED:
              if (is_null($this->segment->input) || !isset($this->segment->input->moca)) {
                throw new \Exception( $this->getExceptionMessage('MOCA_ID not exists', Segment::class, $this->segment->id, __CLASS__) );
              }
              
              event( new InputMocaEvent(InputJob::TRIGGER_DELETED, 'segments', $payload, $this->segment->input->moca) );
            break;
        }
    }
}
