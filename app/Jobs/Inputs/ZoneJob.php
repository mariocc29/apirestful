<?php

namespace App\Jobs\Inputs;

use App\Events\InputMocaEvent;
use App\Jobs\Inputs\InputJob;
use App\Models\Zone;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ZoneJob extends InputJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $zone;

    protected $trigger; //created, updated, deleted

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Zone $zone, string $trigger)
    {
        $this->zone    = $zone;
        
        $this->trigger = $trigger;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Obtiene todos los input de una aplicación
        $inputs = $this->zone->geofence->product->app->inputs->pluck('pivot', 'code');

        foreach ($inputs as $code => $info) {
          // Si la función existe, ejecuta el post correspondiente
          if( is_callable([ZoneJob::class, $code]) ){
            // Según cada input, realiza la tarea
            call_user_func_array ([ZoneJob::class, $code], [$info->additional_data]);
          }
        }
    }

    /**
     * Conexión con el proveedor MOCA para ucontext para cargar zones en el app
     * @param  json $additional_data Información de la conexión con el proveedor
     * @return void                  Crea el código
     */
    private function moca($additional_data)
    {
        $payload = collect([]);

        if ($this->trigger !== InputJob::TRIGGER_DELETED) {
          $payload = collect([
            'name'        => $this->zone->label,
            'description' => $this->zone->description
          ]);
        }
        
        switch ($this->trigger) {
          case InputJob::TRIGGER_CREATED:
            
              $path   = "zones/?placeId={$this->zone->geofence->input->moca}";
              $result = collect( event( new InputMocaEvent(InputJob::TRIGGER_CREATED, $path, $payload) ) )->first();

              if ( !is_null( $result->get('zoneId') ) ) {
                
                Zone::flushEventListeners();

                $input = collect( ( !is_null($this->zone->input) ) ? $this->zone->input : [] );
                $input->put('moca', $result->get('zoneId'));
                $this->zone->input = $input->toArray();
                $this->zone->update();
              }
            
            break;
          
          case InputJob::TRIGGER_UPDATED:
              if (is_null($this->zone->input) || !isset($this->zone->input->moca)) {
                throw new \Exception( $this->getExceptionMessage('MOCA_ID not exists', Zone::class, $this->zone->id, __CLASS__) );
              }
              
              event( new InputMocaEvent(InputJob::TRIGGER_UPDATED, 'zones', $payload, $this->zone->input->moca) );
            break;
          
          case InputJob::TRIGGER_DELETED:
              if (is_null($this->zone->input) || !isset($this->zone->input->moca)) {
                throw new \Exception( $this->getExceptionMessage('MOCA_ID not exists', Zone::class, $this->zone->id, __CLASS__) );
              }
              
              event( new InputMocaEvent(InputJob::TRIGGER_DELETED, 'zones', $payload, $this->zone->input->moca) );
            break;
        }
    }
}
