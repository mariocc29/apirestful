<?php

namespace App\Jobs\Inputs;

use App\Events\InputMocaEvent;
use App\Jobs\Inputs\InputJob;
use App\Models\Beacon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class BeaconJob extends InputJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $beacon;

    protected $trigger; //created, updated, deleted

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Beacon $beacon, string $trigger)
    {
        $this->beacon  = $beacon;
        
        $this->trigger = $trigger;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Obtiene todos los input de una aplicación
        $inputs = $this->beacon->app->inputs->pluck('pivot', 'code');

        foreach ($inputs as $code => $info) {
          // Si la función existe, ejecuta el post correspondiente
          if( is_callable([BeaconJob::class, $code]) ){
            // Según cada input, realiza la tarea
            call_user_func_array ([BeaconJob::class, $code], [$info->additional_data]);
          }
        }
    }

    /**
     * Conexión con el proveedor MOCA para ucontext para cargar beacons en el app
     * @param  json $additional_data Información de la conexión con el proveedor
     * @return void                  Crea el código
     */
    private function moca($additional_data)
    {
        $payload = collect([]);

        if ($this->trigger !== InputJob::TRIGGER_DELETED) {
          $payload = collect([
            'name'        => $this->beacon->label,
            'description' => $this->beacon->description,
            'provider'    => 'Estimote',
            'uuid'        => $this->beacon->uuid,
            'major'       => $this->beacon->major,
            'minor'       => $this->beacon->minor,
          ]);
        }
        
        switch ($this->trigger) {
          case InputJob::TRIGGER_CREATED:
            
              $path   = "beacons/?accountId={$additional_data->account_id}";
              $result = collect( event( new InputMocaEvent(InputJob::TRIGGER_CREATED, $path, $payload) ) )->first();

              if ( !is_null( $result->get('beaconId') ) ) {
                
                Beacon::flushEventListeners();

                $input = collect( ( !is_null($this->beacon->input) ) ? $this->beacon->input : [] );
                $input->put('moca', $result->get('beaconId'));
                $this->beacon->input = $input->toArray();
                $this->beacon->update();
              }
            
            break;
          
          case InputJob::TRIGGER_UPDATED:
              if (is_null($this->beacon->input) || !isset($this->beacon->input->moca)) {
                throw new \Exception( $this->getExceptionMessage('MOCA_ID not exists', Beacon::class, $this->beacon->id, __CLASS__) );
              }

              event( new InputMocaEvent(InputJob::TRIGGER_UPDATED, 'beacons', $payload, $this->beacon->input->moca) );
            break;
          
          case InputJob::TRIGGER_DELETED:
              if (is_null($this->beacon->input) || !isset($this->beacon->input->moca)) {
                throw new \Exception( $this->getExceptionMessage('MOCA_ID not exists', Beacon::class, $this->beacon->id, __CLASS__) );
              }
              
              event( new InputMocaEvent(InputJob::TRIGGER_DELETED, 'beacons', $payload, $this->beacon->input->moca) );
            break;
        }

    }
}
