<?php

namespace App\Jobs\Inputs;

use App\Events\InputMocaEvent;
use App\Jobs\Inputs\InputJob;
use App\Models\Branch;
use Faker\Factory as Faker;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class BranchJob extends InputJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $branch;

    protected $trigger; //created, updated, deleted

    protected $faker;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Branch $branch, string $trigger)
    {
        $this->branch  = $branch;
        
        $this->trigger = $trigger;
        
        $this->faker   = Faker::create();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Obtiene todos los input de una aplicación
        $inputs = $this->branch->app->inputs->pluck('pivot', 'code');

        foreach ($inputs as $code => $info) {
          // Si la función existe, ejecuta el post correspondiente
          if( is_callable([BranchJob::class, $code]) ){
            // Según cada input, realiza la tarea
            call_user_func_array ([BranchJob::class, $code], [$info->additional_data]);
          }
        }
    }

    /**
     * Conexión con el proveedor MOCA para ucontext para cargar los tags de las sucursales en el app
     * @param  json $additional_data Información de la conexión con el proveedor
     * @return void                  Crea el código
     */
    private function moca($additional_data)
    {
        $payload = collect([]);

        if ($this->trigger !== InputJob::TRIGGER_DELETED) {
          $payload = collect([
            'name'        => $this->branch->tagname,
            'displayName' => $this->branch->tagname,
            'color'       => $this->faker->hexcolor,
            'hidden'      => false,
          ]);
        }
        
        switch ($this->trigger) {
          case InputJob::TRIGGER_CREATED:
            
              $path   = "tags/?appId={$additional_data->app_id}";
              $result = collect( event( new InputMocaEvent(InputJob::TRIGGER_CREATED, $path, $payload) ) )->first();

              if ( !is_null( $result->get('tagId') ) ) {
                
                Branch::flushEventListeners();

                $input = collect( ( !is_null($this->branch->input) ) ? $this->branch->input : [] );
                $input->put('moca', $result->get('tagId'));
                $this->branch->input = $input->toArray();
                $this->branch->update();
              }
            
            break;
          
          case InputJob::TRIGGER_DELETED:

              if (is_null($this->branch->input) || !isset($this->branch->input->moca)) {
                throw new \Exception( $this->getExceptionMessage('MOCA_ID not exists', Branch::class, $this->branch->id, __CLASS__) );
              }
              
              event( new InputMocaEvent(InputJob::TRIGGER_DELETED, 'tags', $payload, $this->branch->input->moca) );
            break;
        }
    }
}
