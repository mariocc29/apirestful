<?php

namespace App\Jobs\Inputs;

use App\Events\InputMocaEvent;
use App\Jobs\Inputs\InputJob;
use App\Models\Experience;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ExperienceJob extends InputJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $experience;

    protected $trigger; //created, updated, deleted

    protected $default_language;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Experience $experience, string $trigger)
    {
        $this->experience = $experience;
        
        $this->trigger    = $trigger;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Obtiene el lenguaje por defecto de la campaña
        $this->default_language = $this->experience->campaign->product->app->languages->where('pivot.default', true)->first()->iso;

        // Obtiene todos los input de una aplicación
        $inputs = $this->experience->campaign->product->app->inputs->pluck('pivot', 'code');

        foreach ($inputs as $code => $info) {
          // Si la función existe, ejecuta el post correspondiente
          if( is_callable([ExperienceJob::class, $code]) ){
            // Según cada input, realiza la tarea
            call_user_func_array ([ExperienceJob::class, $code], [$info->additional_data]);
          }
        }
    }

    /**
     * Conexión con el proveedor MOCA para ucontext para cargar beacons en el app
     * @param  json $additional_data Información de la conexión con el proveedor
     * @return void                  Crea el código
     */
    private function moca($additional_data)
    {
        $payload = collect([]);

        // init Obtiene el objecto que va a disparar la experiencia
        if ( !is_null($this->experience->beacon_id) ) {
          $trigger_action = 'Beacon';
          $trigger_value  = [
              'key'   => 'beaconId',
              'value' => $this->experience->beacon->input->moca,
            ];
        } elseif ( !is_null($this->experience->zone_id) ){
          $trigger_action = 'Zone';
          $trigger_value  = [
              'key'   => 'zoneId',
              'value' => $this->experience->zone->input->moca,
            ];
        } elseif ( !is_null($this->experience->geofence_id) ){
          $trigger_action = 'Place';
          $trigger_value  = [
              'key'   => 'placeId',
              'value' => $this->experience->geofence->input->moca,
            ];
        }
        // end Obtiene el objecto que va a disparar la experiencia

        // init Junto al objeto de la experiencia, verifica el momento del disparador
        switch ($this->experience->trigger) {
          case Experience::TRIGGER_ENTER:
              $trigger_type = "Enter{$trigger_action}Trigger";
            break;
          case Experience::TRIGGER_EXIT:
              $trigger_type = "Exit{$trigger_action}Trigger";
            break;
          case Experience::TRIGGER_INMEDIATE:
              $trigger_type = "{$trigger_action}ProximityTrigger";
            break;
        }
        // end Junto al objeto de la experiencia, verifica el momento del disparador

        if ($this->trigger !== InputJob::TRIGGER_DELETED) {
          $payload = collect([
            'config' => [
                'name'        => $this->experience->name->{$this->default_language},
                'description' => $this->experience->description->{$this->default_language},
                'repeat'      => $this->experience->repeat->code,
                'enabled'     => $this->experience->enabled,
              ],
            'trigger' => [
                '@type'               => $trigger_type,
                $trigger_value['key'] => $trigger_value['value']
              ],
            'action' => [
                '@type'       => 'OpenUrlAction',
                "contentType" => "text/x-url",
                'content'     => [
                    'default' => "{$this->experience->campaign->product->app->deep_link}/?id={$this->experience->key}&type=experience",
                  ],
                'bgAlert'     => [
                    'default' => $this->experience->message_notification->{$this->default_language},
                  ]
              ]
          ]);
        }
        
        switch ($this->trigger) {
          case InputJob::TRIGGER_CREATED:
            
              $path   = "experiences/?campaignId={$this->experience->campaign->input->moca}";
              $result = collect( event( new InputMocaEvent(InputJob::TRIGGER_CREATED, $path, $payload) ) )->first();

              if ( !is_null( $result->get('experienceId') ) ) {
                
                Experience::flushEventListeners();

                $input = collect( ( !is_null($this->experience->input) ) ? $this->experience->input : [] );
                $input->put('moca', $result->get('experienceId'));
                $this->experience->input = $input->toArray();
                $this->experience->update();
              }
            
            break;
          
          case InputJob::TRIGGER_UPDATED:
              if (is_null($this->experience->input) || !isset($this->experience->input->moca)) {
                throw new \Exception( $this->getExceptionMessage('MOCA_ID not exists', Experience::class, $this->experience->id, __CLASS__) );
              }

              event( new InputMocaEvent(InputJob::TRIGGER_UPDATED, 'experiences', $payload, $this->experience->input->moca) );
            break;
          
          case InputJob::TRIGGER_DELETED:
              if (is_null($this->experience->input) || !isset($this->experience->input->moca)) {
                throw new \Exception( $this->getExceptionMessage('MOCA_ID not exists', Experience::class, $this->experience->id, __CLASS__) );
              }
              
              event( new InputMocaEvent(InputJob::TRIGGER_DELETED, 'experiences', $payload, $this->experience->input->moca) );
            break;
        }

    }
}
