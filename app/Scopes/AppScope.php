<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class AppScope implements Scope
{
  
  public function apply(Builder $builder, Model $model)
  {
      $application = request()->get('application');
      if ( !is_null($application) ) {
        $builder->where('app_id', $application->id);
      }
  }
}