<?php

namespace App\Listeners;

use App\Events\InputMocaEvent;
use App\Inputs\ApiInput;
use App\Inputs\MocaInput;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class InputMocaListener
{
    use ApiInput;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(InputMocaEvent $event)
    {
        // Si la función existe, ejecuta el post correspondiente
        if( is_callable([InputMocaListener::class, $event->trigger]) ){
          // Según cada input, realiza la tarea
          return call_user_func_array ([InputMocaListener::class, $event->trigger], [$event]);
        }
    }

    private function created($event)
    {
      $this->init( new MocaInput );
      return $this->push($event->path, $event->payload);
    }

    private function updated($event)
    {
      $this->init( new MocaInput );
      $this->put($event->path, $event->payload, $event->resource_id);
    }

    private function deleted($event)
    {
      $this->init( new MocaInput );
      $this->delete($event->path, $event->resource_id);
    }
}
