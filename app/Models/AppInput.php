<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class AppInput extends Pivot
{
    protected $table    = 'general.app_input'; 
    protected $guarded  = [];
    protected $fillable = ['app_id', 'input_id', 'additional_data'];
    protected $hidden   = [];
    protected $dates    = [];
    protected $appends  = [];

    public $timestamps  = false;

    public function getAdditionalDataAttribute($value){
      return json_decode($value);
    }
}
