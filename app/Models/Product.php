<?php

namespace App\Models;

use App\Jobs\Inputs\InputJob;
use App\Jobs\Inputs\ProductJob;
use App\Scopes\AppScope;
use App\Traits\ApiHash;
use App\Traits\ClearString;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use  SoftDeletes, ApiHash, ClearString;

    protected $table    = 'interests.products'; 
    protected $guarded  = ['id'];
    protected $fillable = ['app_id', 'label', 'tagname', 'picture', 'additional_data', 'input'];
    protected $hidden   = ['id', 'app_id', 'input'];
    protected $dates    = ['created_at', 'updated_at', 'deleted_at'];
    protected $appends  = ['key'];

    public $timestamps  = true;

    protected static function boot()
    {
      parent::boot();
      static::addGlobalScope(new AppScope);

      Product::created(function($product){
        ProductJob::dispatch($product, InputJob::TRIGGER_CREATED);
      });

      Product::deleted(function($product){
        ProductJob::dispatch($product, InputJob::TRIGGER_DELETED);
      });
    }

    public function app()
    {
      return $this->belongsTo( \App\Models\App::class);
    }

    public function beacons()
    {
      return $this->belongsToMany( \App\Models\Beacon::class, 'marketing.beacon_product');
    }

    public function branches()
    {
      return $this->belongsToMany( \App\Models\Branche::class, 'interests.branch_product');
    }

    public function categories()
    {
      return $this->belongsToMany( \App\Models\Category::class, 'interests.interests');
    }

    public function campaigns()
    {
      return $this->hasMany( \App\Models\Campaign::class);
    }

    public function communities()
    {
      return $this->belongsToMany( \App\Models\Community::class, 'general.account_product');
    }

    public function interests()
    {
      return $this->hasMany( \App\Models\Interest::class);
    }

    public function geofences()
    {
      return $this->hasMany( \App\Models\Geofence::class);
    }

    public function notifications()
    {
      return $this->hasMany( \App\Models\Notification::class);
    }

    public function segments()
    {
      return $this->hasMany( \App\Models\Segment::class);
    }

    public function setLabelAttribute($value)
    {
      return $this->attributes['label'] = json_encode( $value ); 
    }

    public function getLabelAttribute($value)
    {
      return json_decode( $value );  
    }

    public function getPictureAttribute($value)
    {
      return \App\Helpers\ApiStorage::get( $value, 'products' );
    }

    public function setPictureAttribute($value)
    {
      if( filter_var($value, FILTER_VALIDATE_URL) === false ){
        return $this->attributes['picture'] = \App\Helpers\ApiStorage::save( $value, 'products' );
      }
      
      return $this->attributes['picture'] = basename($value);
    }

    public function setAdditionalDataAttribute($value)
    {
      return $this->attributes['additional_data'] = ( !is_null($value) ) ? json_encode( $value ) : null;
    }

    public function getAdditionalDataAttribute($value)
    {
      return ( !is_null($value) ) ? json_decode( $value ) : null;
    }

    public function setInputAttribute($value)
    {
      return $this->attributes['input'] = ( !is_null( $value ) ) ? json_encode( $value ) : null;
    }

    public function getInputAttribute($value)
    {
      return ( !is_null($value) ) ? json_decode( $value ) : null;
    }

    public function getTagnameAttribute($value)
    {
      return "product:{$value}";
    }

    public function setTagnameAttribute($value)
    {
      $default_language = $this->app->languages->where('pivot.default', true)->first()->iso;
      $tagname          = strtolower( preg_replace('/product:|([^a-zA-Z0-9_])/', '', $this->toUTF8($this->label->{$default_language}) ) );

      return $this->attributes['tagname'] = $tagname;
    }

}
