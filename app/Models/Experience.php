<?php

namespace App\Models;

use App\Jobs\Inputs\ExperienceJob;
use App\Jobs\Inputs\InputJob;
use App\Traits\ApiHash;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Experience extends Model
{
  use  SoftDeletes, ApiHash;

  const ATTACHEMENT_TYPE_URL   = 'url';
  const ATTACHEMENT_TYPE_VIDEO = 'video';
  const ATTACHEMENT_TYPE_IMAGE = 'image';
  const ATTACHEMENT_TYPE_AUDIO = 'audio';
  
  const TRIGGER_ENTER          = 'onenter';
  const TRIGGER_EXIT           = 'onexit';
  const TRIGGER_INMEDIATE      = 'oninmediate';

  protected $table    = 'marketing.experiences';
  protected $guarded  = [ 'id'];
  protected $fillable = ['campaign_id', 'name', 'description', 'message_notification', 'enabled', 'attachement_type', 'geofence_id', 'zone_id', 'beacon_id', 'trigger', 'url', 'custom_property_label', 'custom_property_key', 'custom_property_values', 'points', 'probability', 'steps', 'goal', 'repeat_id', 'reminder_hours', 'input'];
  protected $hidden   = [ 'id', 'campaign_id', 'geofence_id', 'zone_id', 'beacon_id', 'repeat_id', 'input'];
  protected $dates    = ['created_at', 'updated_at', 'deleted_at'];
  protected $appends  = ['key', 'geofence_key', 'zone_key', 'beacon_key', 'repeat_key'];

  public $timestamps  = true; 

  protected static function boot()
    {
      parent::boot();

      Experience::created(function($experience){
        ExperienceJob::dispatch($experience, InputJob::TRIGGER_CREATED);
      });

      Experience::updated(function($experience){
        ExperienceJob::dispatch($experience, InputJob::TRIGGER_UPDATED);
      });

      Experience::deleted(function($experience){
        ExperienceJob::dispatch($experience, InputJob::TRIGGER_DELETED);
      });
    }

  public function campaign()
  {
    return $this->belongsTo( \App\Models\Campaign::class);
  }

  public function code()
  {
    return $this->belongsTo( \App\Models\Code::class);
  }

  public function geofence()
  {
    return $this->belongsTo( \App\Models\Geofence::class);
  }

  public function zone()
  {
    return $this->belongsTo( \App\Models\Zone::class);
  }

  public function beacon()
  {
    return $this->belongsTo( \App\Models\Beacon::class);
  }

  public function galleries()
  {
    return $this->morphMany(App\Models\Gallery::class, 'source');
  }

  public function repeat()
  {
    return $this->belongsTo( \App\Models\Repeat::class);
  }

  public function stickers()
  {
    return $this->hasMany( \App\Models\Sticker::class);
  }

  public function printings()
  {
    return $this->hasMany( \App\Models\Printing::class);
  }

  public function setNameAttribute($value)
  {    
    return $this->attributes['name'] = json_encode( $value );  
  }

  public function getNameAttribute($value)
  {    
    return json_decode( $value );  
  }

  public function setDescriptionAttribute($value)
  {
    return $this->attributes['description'] = json_encode( $value ); 
  }

  public function getDescriptionAttribute($value)
  {
    return json_decode( $value );  
  }

  public function setMessageNotificationAttribute($value)
  {
    return $this->attributes['message_notification'] = json_encode( $value ); 
  }

  public function getMessageNotificationAttribute($value)
  {
    return json_decode( $value ); 
  }

  public function setCustomPropertyValuesAttribute($value)
  {
    return $this->attributes['custom_property_values'] = ( !is_null($value) ) ? json_encode( $value ) : null;
  }

  public function getCustomPropertyValuesAttribute($value)
  {
    return ( !is_null($value) ) ? json_decode( $value ) : null;
  }

  public function setInputAttribute($value)
  {
    return $this->attributes['input'] = ( !is_null($value) ) ? json_encode( $value ) : null; 
  }

  public function getInputAttribute($value)
  {
    return ( !is_null($value) ) ? json_decode( $value ) : null;
  }

  public function getGeofenceKeyAttribute($value)
  {
    return $this->hash_encode($this->geofence_id, \App\Models\Geofence::class);
  }

  public function getZoneKeyAttribute($value)
  {
    return $this->hash_encode($this->zone_id, \App\Models\Zone::class);
  }

  public function getBeaconKeyAttribute($value)
  {
    return $this->hash_encode($this->beacon_id, \App\Models\Beacon::class);
  }

  public function getRepeatKeyAttribute($value)
  {
    return $this->hash_encode($this->repeat_id, \App\Models\Repeat::class);
  }

  public function setGeofenceIdAttribute($value)
  {
    return $this->attributes['geofence_id'] = $this->hash_decode($value, \App\Models\Geofence::class); 
  }

  public function setZoneIdAttribute($value)
  {
    return $this->attributes['zone_id'] = $this->hash_decode($value, \App\Models\Zone::class); 
  }

  public function setBeaconIdAttribute($value)
  {
    return $this->attributes['beacon_id'] = $this->hash_decode($value, \App\Models\Beacon::class); 
  }

  public function setRepeatIdAttribute($value)
  {
    return $this->attributes['repeat_id'] = $this->hash_decode($value, \App\Models\Repeat::class); 
  }

}
