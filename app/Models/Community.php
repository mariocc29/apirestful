<?php

namespace App\Models;

use App\Models\Account;
use App\Scopes\CommunityScope;

class Community extends Account
{
    protected static function boot()
    {
      parent::boot();
      static::addGlobalScope(new CommunityScope);
    }

    public function products()
    {
      return $this->belongsToMany(\App\Models\Product::class, 'general.account_product');
    }

}
