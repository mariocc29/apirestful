<?php

namespace App\Models;

use App\Traits\ApiHash;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Repeat extends Model
{
  use  SoftDeletes, ApiHash;

  protected $table    = 'marketing.repeats'; 
  protected $guarded  = ['id'];
  protected $fillable = ['label', 'code'];
  protected $hidden   = ['id'];
  protected $dates    = ['created_at', 'updated_at', 'deleted_at'];
  protected $appends  = ['key'];

  public $timestamps  = true;

  public function experiences()
  {
    return $this->hasMany( \App\Models\Experience::class);
  }
}
