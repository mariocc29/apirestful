<?php

namespace App\Models;

use App\Traits\ApiHash;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Interest extends Model
{
  use  SoftDeletes, ApiHash;

  protected $table    = 'interests.interests';
  protected $guarded  = [ 'id'];
  protected $fillable = ['category_id', 'product_id'];
  protected $hidden   = [ 'id', 'category_id', 'product_id'];
  protected $dates    = ['created_at', 'updated_at', 'deleted_at'];
  protected $appends  = ['key', 'category_key', 'product_key'];

  public $timestamps  = true; 

  public function category()
  {
    return $this->belongsTo( \App\Models\Category::class);
  }

  public function product()
  {
    return $this->belongsTo( \App\Models\Product::class);
  }

  public function getCategoryKeyAttribute($value)
  {
    return $this->hash_encode($this->category_id, \App\Models\Category::class);
  }

  public function getProductKeyAttribute($value)
  {
    return $this->hash_encode($this->product_id, \App\Models\Product::class);
  }
}