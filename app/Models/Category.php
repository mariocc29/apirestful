<?php

namespace App\Models;

use App\Jobs\Inputs\CategoryJob;
use App\Jobs\Inputs\InputJob;
use App\Scopes\AppScope;
use App\Traits\ApiHash;
use App\Traits\ClearString;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use  SoftDeletes, ApiHash, ClearString;

    protected $table    = 'interests.categories'; 
    protected $guarded  = ['id'];
    protected $fillable = ['app_id', 'label', 'tagname', 'description', 'picture', 'fontcolor', 'backcolor', 'category_id', 'input'];
    protected $hidden   = ['id', 'app_id', 'category_id', 'input'];
    protected $dates    = ['created_at', 'updated_at', 'deleted_at'];
    protected $appends  = ['key', 'category_key'];

    public $timestamps  = true;

    protected static function boot()
    {
      parent::boot();
      static::addGlobalScope(new AppScope);

      Category::created(function($category){
        CategoryJob::dispatch($category, InputJob::TRIGGER_CREATED);
      });

      Category::deleted(function($category){
        CategoryJob::dispatch($category, InputJob::TRIGGER_DELETED);
      });
    }

    public function app()
    {
      return $this->belongsTo(\App\Models\App::class);
    }

    public function interests()
    {
      return $this->hasMany(\App\Models\Interest::class);
    }

    public function products()
    {
      return $this->belongsToMany(\App\Models\Product::class, 'interests.interests');
    }

    public function subcategories()
    {
      return $this->hasMany(\App\Models\Subcategory::class);
    }

    public function getLabelAttribute($value)
    {
      return json_decode($value);   
    }

    public function setLabelAttribute($value)
    {
      return $this->attributes['label'] = json_encode($value);   
    }

    public function getPictureAttribute($value)
    {
      return ( !is_null($value) ) ? \App\Helpers\ApiStorage::get( $value, 'categories' ) :  null;  
    }

    public function setPictureAttribute($value)
    {
      $filename = null;

      if ( !is_null($value) ) {
        if( filter_var($value, FILTER_VALIDATE_URL) === false ){
          $filename = \App\Helpers\ApiStorage::save( $value, 'categories' );
        }

        $filename = basename($value);
      }

      return $this->attributes['picture'] = $filename;
    }

    public function getDescriptionAttribute($value)
    {
      return ( !is_null($value) ) ? json_decode($value) : null;
    }

    public function setDescriptionAttribute($value)
    {
      return $this->attributes['description'] = ( !is_null($value) ) ? json_encode($value) : null;
    }

    public function getInputAttribute($value)
    {
      return ( !is_null($value) ) ? json_decode($value) : null;
    }

    public function setInputAttribute($value)
    {
      return $this->attributes['input'] = ( !is_null($value) ) ? json_encode($value) : null;
    }

    public function getCategoryKeyAttribute($value)
    {
      return $this->hash_encode($this->category_id, \App\Models\Category::class);
    }

    public function setCategoryIdAttribute($value)
    {
      return $this->attributes['category_id'] = $this->hash_decode($value, \App\Models\Category::class); 
    }

    public function getTagnameAttribute($value)
    {
      if ( !is_null($this->category_id) ) {
        $default_language = $this->app->languages->where('pivot.default', true)->first()->iso;
        $tagname          = strtolower( preg_replace('/category:([a-zA-Z0-9\s]{1,}:?)|([^a-zA-Z0-9_]?)/', '', $this->toUTF8($this->category->label->{$default_language}) ) );
        return "category:{$tagname}:{$value}";
      }

      return "category:{$value}";
    }

    public function setTagnameAttribute($value)
    {
      $default_language = $this->app->languages->where('pivot.default', true)->first()->iso;
      $tagname          = strtolower( preg_replace('/category:([a-zA-Z0-9\s]{1,}:?)|([^a-zA-Z0-9_])/', '', $this->toUTF8($this->label->{$default_language}) ) );

      return $this->attributes['tagname'] = $tagname;
    }
}
