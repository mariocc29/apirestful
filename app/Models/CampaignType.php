<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\ApiHash;

class CampaignType extends Model
{
  use  SoftDeletes, ApiHash;

  const ACTION_PROXIMITY = 'proximity';
  const ACTION_PUSH      = 'push';

  protected $table    = 'general.campaigns_types'; 
  protected $guarded  = ['id'];
  protected $fillable = ['label','action'];
  protected $hidden   = ['id'];
  protected $dates    = ['created_at', 'updated_at', 'deleted_at'];
  protected $appends  = ['key'];

  public $timestamps  = true;

  public function apps()
  {
    return $this->belongsToMany(\App\Models\App::class, 'general.app_campaigns_types');
  }

  public function campaigns()
  {
    return $this->hasMany(\App\Models\Campaign::class);
  }
}
