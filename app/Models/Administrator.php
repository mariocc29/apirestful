<?php

namespace App\Models;

use App\Models\Account;
use App\Scopes\AdministratorScope;

class Administrator extends Account
{
    protected static function boot()
    {
      parent::boot();
      static::addGlobalScope(new AdministratorScope);
    }
}
