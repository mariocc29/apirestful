<?php

namespace App\Models;

use App\Jobs\Inputs\BeaconCoordinatesJob;
use App\Jobs\Inputs\BeaconJob;
use App\Jobs\Inputs\InputJob;
use App\Scopes\AppScope;
use App\Traits\ApiHash;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; 

class Beacon extends Model
{
    use  SoftDeletes, ApiHash;

    protected $table    = 'triggers.beacons'; 
    protected $guarded  = ['id'];
    protected $fillable = ['app_id', 'label', 'description', 'uuid', 'major', 'minor', 'geo_position', 'position', 'area_id', 'input'];
    protected $hidden   = ['id', 'app_id', 'area_id', 'input'];
    protected $dates    = ['created_at', 'updated_at', 'deleted_at'];
    protected $appends  = ['key', 'area_key'];

    public $timestamps  = true;

    protected static function boot()
    {
      parent::boot();
      static::addGlobalScope(new AppScope);

      Beacon::created(function($beacon){
        BeaconJob::dispatch($beacon, InputJob::TRIGGER_CREATED);
      });

      Beacon::updated(function($beacon){
        BeaconJob::dispatch($beacon, InputJob::TRIGGER_UPDATED);

        if ( $beacon->isDirty('area_id') || $beacon->isDirty('geo_position') ) {
          BeaconCoordinatesJob::dispatch($beacon);
        }
      });

      Beacon::deleted(function($beacon){
        BeaconJob::dispatch($beacon, InputJob::TRIGGER_DELETED);
      });
    }

    public function app()
    {
      return $this->belongsTo(\App\Models\App::class);
    }

    public function experiences()
    {
      return $this->hasMany(\App\Models\Experience::class);
    }

    public function area()
    {
      return $this->belongsTo(\App\Models\Area::class);
    }

    public function products()
    {
      return $this->belongsToMany(\App\Models\Product::class, 'marketing.beacon_product');
    }

    public function zones()
    {
      return $this->belongsToMany(\App\Models\Zone::class, 'triggers.beacon_zone');
    }

    public function getGeoPositionAttribute($value)
    { 
      return ( !is_null($value) ) ? json_decode($value) : null;   
    }

    public function setGeoPositionAttribute($value)
    {
      return $this->attributes['geo_position'] =  ( !is_null($value) ) ? json_encode($value) : null;   
    }

    public function getPositionAttribute($value)
    {
      return ( !is_null($value) ) ? json_decode($value) : null; 
    }

    public function setPositionAttribute($value)
    {
      return $this->attributes['position'] = ( !is_null($value) ) ? json_encode($value) : null;    
    }

    public function getInputAttribute($value)
    {
      return ( !is_null($value) ) ? json_decode($value) : null;
    }

    public function setInputAttribute($value)
    {
      return $this->attributes['input'] = ( !is_null($value) ) ? json_encode($value) : null;   
    }

    public function getAreaKeyAttribute($value)
    {
      return $this->hash_encode($this->area_id, \App\Models\Area::class);
    }
}
