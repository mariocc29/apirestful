<?php

namespace App\Models;

use App\Jobs\Inputs\InputJob;
use App\Jobs\Inputs\SegmentJob;
use App\Traits\ApiHash;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Config;

class Segment extends Model
{
    use  SoftDeletes, ApiHash;

    protected $table    = 'marketing.segments'; 
    protected $guarded  = ['id'];
    protected $fillable = ['product_id', 'label', 'description', 'input'];
    protected $hidden   = ['id', 'product_id', 'input'];
    protected $dates    = ['created_at', 'updated_at', 'deleted_at'];
    protected $appends  = ['key', 'audience'];

    public $timestamps  = true;

    protected static function boot()
    {
      parent::boot();
      
      Segment::updated(function($segment){
        if ( $segment->audience->count() > 0 ) {
          SegmentJob::dispatch($segment, InputJob::TRIGGER_UPDATED);
        } else {
          SegmentJob::dispatch($segment, InputJob::TRIGGER_DELETED);
        }
      });

      Segment::deleted(function($segment){
        SegmentJob::dispatch($segment, InputJob::TRIGGER_DELETED);
      });
    }

    public function campaings()
    {
      return $this->hasMany( \App\Models\Campaing::class);
    }

    public function product()
    {
      return $this->belongsTo( \App\Models\Product::class); 
    }

    public function conditions()
    {
      return $this->hasMany( \App\Models\Condition::class );
    }

    public function setInputAttribute($value)
    {
      return $this->attributes['input'] = ( !is_null( $value ) ) ? json_encode( $value ) : null;
    }

    public function getInputAttribute($value)
    {
      return ( !is_null( $value ) ) ? json_decode( $value ) : null;
    }

    // Audiencia es solo para moca, hasta el momento
    public function getAudienceAttribute($value)
    {
      $query = collect([]);

      foreach ($this->conditions as $item) {
        $operator = Config::get("constants.moca.segments.operators.{$item->condition}");
        $query->push([$item->property => [$operator => $item->value]]);
      }

      return $query;
    }
}
