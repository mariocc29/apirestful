<?php

namespace App\Models;

use App\Traits\ApiHash;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Gallery extends Model
{
  use  SoftDeletes, ApiHash;

  protected $table    = 'marketing.galleries';
  protected $guarded  = [ 'id'];
  protected $fillable = ['language_id', 'filename', 'source_id', 'source_type', 'additional_data'];
  protected $hidden   = [ 'id', 'language_id', 'source_id'];
  protected $dates    = ['created_at', 'updated_at', 'deleted_at'];
  protected $appends  = ['key', 'language_key', 'source_key'];

  public $timestamps  = true; 

  public function source()
  {
    return $this->morphTo();
  }

  public function setFilenameAttribute($value)
  {
    return $this->attributes['filename'] = \App\Helpers\ApiStorage::save( $value, 'galleries' );   
  }

  public function setAdditionalDataAttribute($value)
  {
    return $this->attributes['additional_data'] = ( !is_null($value) ) ? json_encode( $value ) : null;
  }

  public function getAdditionalDataAttribute($value)
  {
    return ( !is_null($value) ) ? json_decode( $value ) : null;
  }

  public function getLanguageKeyAttribute($value)
  {
    return $this->hash_encode($this->language_id, \App\Models\Language::class);
  }

  public function getSourceKeyAttribute($value)
  {
    return $this->hash_encode($this->source_id, $this->source_type);
  }

  public function setLanguageIdAttribute($value)
  {
    return $this->attributes['language_id'] = $this->hash_decode($value, \App\Models\Language::class); 
  }

}