<?php

namespace App\Models;

use App\Models\Category;

class Subcategory extends Category
{
  public function category()
  {
    return $this->belongsTo( Category::class, 'category_id'); 
  }
}
