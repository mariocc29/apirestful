<?php

namespace App\Models;

use App\Traits\ApiHash;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sticker extends Model
{
  use  SoftDeletes, ApiHash;

  protected $table    = 'marketing.stickers';
  protected $guarded  = [ 'id'];
  protected $fillable = ['experience_id', 'picture'];
  protected $hidden   = [ 'id', 'experience_id'];
  protected $dates    = ['created_at', 'updated_at', 'deleted_at'];
  protected $appends  = ['key', 'experience_key'];

  public $timestamps  = true; 

  public function experience()
  {
    return $this->belongsTo( \App\Models\Experience::class);
  }

  public function getPictureAttribute($value)
  {
    return ( !is_null($value) ) ? \App\Helpers\ApiStorage::get( $value, 'stickers' ) :  null;  
  }

  public function setPictureAttribute($value)
  {
    return $this->attributes['picture'] = ( !is_null($value) ) ? \App\Helpers\ApiStorage::save( $value, 'stickers' ) :  null;   
  }

  public function getExperienceKeyAttribute($value)
  {
    return $this->hash_encode($this->experience_id, \App\Models\Experience::class);
  }

  public function setExperienceIdAttribute($value)
  {
    return $this->attributes['experience_id'] = $this->hash_decode($value, \App\Models\Experience::class);
  }
}
