<?php

namespace App\Models;

use App\Traits\ApiHash;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use  SoftDeletes, ApiHash;

    protected $table    = 'general.roles'; 
    protected $guarded  = ['id'];
    protected $fillable = ['label'];
    protected $hidden   = ['id'];
    protected $dates    = ['created_at', 'updated_at', 'deleted_at'];
    protected $appends  = ['key'];

    public $timestamps  = true;

    public function accounts()
    {
      return $this->hasMany( \App\Models\Account::class);
    }
}
