<?php

namespace App\Models;

use App\Traits\ApiHash;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Language extends Model
{
  use  SoftDeletes, ApiHash;

  protected $table    = 'general.languages';
  protected $guarded  = [ 'id'];
  protected $fillable = ['label', 'iso', 'picture'];
  protected $hidden   = [ 'id'];
  protected $dates    = ['created_at', 'updated_at', 'deleted_at'];
  protected $appends  = ['key'];

  public $timestamps  = true; 

  public function apps()
  {
    return $this->belongsToMany( \App\Models\App::class, 'general.app_language')->withPivot('default');
  }

  public function users()
  {
    return $this->hasMany( \App\Models\User::class);
  }

  public function setLabelAttribute($value)
  {
    return $this->attributes['label'] = strtolower($value);
  }

  public function getPictureAttribute($value)
  {
    $uri = null;

    if ( !is_null($value) ) {
      $uri = "https://".env('GOOGLE_CLOUD_STORAGE_BUCKET').".storage.googleapis.com/accounts/{$value}";
    }

    return $uri;   
  }

  public function setPictureAttribute($value)
  {
    return $this->attributes['picture'] = ( !is_null($value) ) ? \App\Helpers\ApiStorage::save( $value, 'languages' ) : null;   
  }
}
