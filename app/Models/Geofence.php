<?php

namespace App\Models;

use App\Jobs\Inputs\GeofenceJob;
use App\Jobs\Inputs\InputJob;
use App\Traits\ApiHash;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Geofence extends Model
{
    use  SoftDeletes, ApiHash;

    protected $table    = 'triggers.geofences';
    protected $guarded  = [ 'id'];
    protected $fillable = ['product_id', 'label', 'description', 'geo_position', 'radius', 'input'];
    protected $hidden   = [ 'id', 'product_id', 'input'];
    protected $dates    = ['created_at', 'updated_at', 'deleted_at'];
    protected $appends  = ['key', 'product_key'];

    public $timestamps  = true; 

    protected static function boot()
    {
      parent::boot();

      Geofence::created(function($geofence){
        GeofenceJob::dispatch($geofence, InputJob::TRIGGER_CREATED);
      });

      Geofence::updated(function($geofence){
        GeofenceJob::dispatch($geofence, InputJob::TRIGGER_UPDATED);
      });

      Geofence::deleted(function($geofence){
        GeofenceJob::dispatch($geofence, InputJob::TRIGGER_DELETED);
      });
    }

    public function product()
    {
      return $this->belongsTo( \App\Models\Product::class);  
    }

    public function experiences()
    {
      return $this->hasMany( \App\Models\Experience::class);  
    }

    public function zones()
    {
      return $this->hasMany( \App\Models\Zone::class);  
    }

    public function setGeoPositionAttribute($value)
    {
      return $this->attributes['geo_position'] = json_encode( $value );  
    }

    public function getGeoPositionAttribute($value)
    {
      return json_decode( $value ); 
    }

    public function setInputAttribute($value)
    {
      return $this->attributes['input'] = ( !is_null($value) ) ? json_encode( $value ) : null;
    }

    public function getInputAttribute($value)
    {
      return ( !is_null($value) ) ? json_decode( $value ) : null;
    }

    public function getProductKeyAttribute($value)
    {
      return $this->hash_encode($this->product_id, \App\Models\Product::class);
    }
}
