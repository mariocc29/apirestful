<?php

namespace App\Models;

use App\Traits\ApiHash;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
  use  Notifiable, SoftDeletes, ApiHash, HasApiTokens;

  const NETWORK_FACEBOOK  = 'facebook';
  const NETWORK_TWITTER   = 'twitter';
  const NETWORK_GOOGLE    = 'google';
  const NETWORK_LOCAL     = 'local';

  protected $table    = 'general.users'; 
  protected $guarded  = ['id'];
  protected $fillable = ['app_id', 'language_id', 'network', 'fullname', 'nickname', 'email', 'dni', 'gender', 'birthdate', 'picture', 'additional_data'];
  protected $hidden   = ['id', 'password', 'app_id', 'language_id'];
  protected $dates    = ['created_at', 'updated_at', 'deleted_at'];
  protected $appends  = ['key', 'age', 'language_key'];

  public $timestamps  = true;

  public function app()
  {
    return $this->belongsTo(\App\Models\App::class);
  }

  public function branches()
  {
    return $this->belongsToMany(\App\Models\Branch::class, 'interests.branch_user');
  }

  public function codes()
  {
    return $this->hasMany(\App\Models\Code::class);
  }

  public function customs_properties()
  {
    return $this->hasMany(\App\Models\CustomProperty::class);
  }

  public function interests()
  {
    return $this->belongsToMany(\App\Models\Interest::class);
  }

  public function language()
  {
    return $this->belongsTo(\App\Models\Language::class);
  }

  public function printings()
  {
    return $this->hasMany(\App\Models\Printing::class);
  }

  public function surveys()
  {
    return $this->hasMany(\App\Models\Survey::class);
  }

  public function getPictureAttribute($value)
  {
    return ( !is_null($value) ) ? \App\Helpers\ApiStorage::get( $value, 'products' ) :  null;   
  }

  public function setPictureAttribute($value)
  {
    return $this->attributes['picture'] = ( !is_null($value) ) ? \App\Helpers\ApiStorage::save( $value, 'users' ) : null; 
  }

  public function setFullnameAttribute($value)
  {
    return $this->attributes['fullname'] = strtolower($value);   
  }

  public function setNicknameAttribute($value)
  {
    return $this->attributes['nickname'] = ( !is_null($value) ) ? strtolower($value) : null;   
  }

  public function getAdditionalDataAttribute($value)
  {
    return ( !is_null($value) ) ? json_decode($value) : null;
  }

  public function setAdditionalDataAttribute($value)
  {
    return $this->attributes['additional_data'] = ( !is_null($value) ) ? json_encode($value) : null;
  }

  public function setEmailAttribute($value)
  {
    return $this->attributes['email'] = strtolower($value);   
  }

  public function getAgeAttribute($value)
  {
    return Carbon::parse($this->birthdate)->age;
  }

  public function getLanguageKeyAttribute($value)
  {
    return $this->hash_encode($this->language_id, \App\Models\Language::class);
  }

  public function setLanguageIdAttribute($value)
  {
    return $this->attributes['language_id'] = $this->hash_decode($value, \App\Models\Language::class); 
  }
}
