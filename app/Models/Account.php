<?php

namespace App\Models;

use App\Scopes\AppScope;
use App\Traits\ApiHash;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens; 

class Account extends Authenticatable
{
    use  Notifiable, SoftDeletes, ApiHash, HasApiTokens;

    const ROLE_ADMINISTRATOR  = 1;
    const ROLE_COMMUNITY      = 2;

    protected $table    = 'general.accounts'; 
    protected $guarded  = ['id'];
    protected $fillable = ['app_id', 'role_id', 'email', 'fullname', 'picture'];
    protected $hidden   = ['id', 'password', 'app_id', 'role_id'];
    protected $dates    = ['created_at', 'updated_at', 'deleted_at'];
    protected $appends  = ['key', 'role_key'];

    public $timestamps  = true;

    protected static function boot()
    {
      parent::boot();
      static::addGlobalScope(new AppScope);
    }

    public function role()
    {
      return $this->belongsTo(\App\Models\Role::class);
    }

    public function app()
    {
      return $this->belongsTo(\App\Models\App::class);
    }

    public function getPictureAttribute($value)
    {
      if (is_null(request()->get('application'))) {
        request()->attributes->add(['application' => $this->app]);
      }
      
      return ( !is_null($value) ) ? \App\Helpers\ApiStorage::get( $value, 'accounts' ) :  null;  
    }

    public function setPictureAttribute($value)
    {
      return $this->attributes['picture'] = ( !is_null($value) ) ? \App\Helpers\ApiStorage::save( $value, 'accounts' ) :  null;
    }

    public function setEmailAttribute($value)
    {
      return $this->attributes['email'] = strtolower($value);
    }

    public function setFullnameAttribute($value)
    {
      return $this->attributes['fullname'] = strtolower($value);
    }

    public function getRoleKeyAttribute($value)
    {
      return $this->hash_encode($this->role_id, \App\Models\Role::class);
    }

    public function setRoleIdAttribute($value)
    {
      return $this->attributes['role_id'] = $this->hash_decode($value, \App\Models\Role::class);
    }
}
