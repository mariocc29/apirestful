<?php

namespace App\Models;

use App\Jobs\Inputs\InputJob;
use App\Jobs\Inputs\SegmentJob;
use App\Traits\ApiHash;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Condition extends Model
{
    use  SoftDeletes, ApiHash;

    const TYPE_TAG             = 'tags';
    const TYPE_CUSTOM_PROPERTY = 'customs_properties';
    
    const PROVIDER_CATEGORY    = 'category';
    const PROVIDER_BRANCH      = 'branch';
    const PROVIDER_PRODUCT     = 'product';

    protected $table    = 'marketing.conditions'; 
    protected $guarded  = ['id'];
    protected $fillable = ['segment_id', 'provider', 'type', 'property', 'condition', 'value'];
    protected $hidden   = ['id', 'segment_id'];
    protected $dates    = ['created_at', 'updated_at', 'deleted_at'];
    protected $appends  = ['key'];

    public $timestamps  = true;

    protected static function boot()
    {
      parent::boot();
      
      Condition::created(function($condition){
        if ( $condition->segment->audience->count() === 1 ) {
          SegmentJob::dispatch($condition->segment, InputJob::TRIGGER_CREATED);
        } else {
          SegmentJob::dispatch($condition->segment, InputJob::TRIGGER_UPDATED);
        }
      });

      Condition::updated(function($condition){
        SegmentJob::dispatch($condition->segment, InputJob::TRIGGER_UPDATED);
      });

      Condition::deleted(function($condition){
        if ( $condition->segment->audience->count() > 0 ) {
          SegmentJob::dispatch($condition->segment, InputJob::TRIGGER_UPDATED);
        } else {
          SegmentJob::dispatch($condition->segment, InputJob::TRIGGER_DELETED);
        }
      });
    }

    public function segment()
    {
      return $this->belongsTo(\App\Models\Segment::class);
    }

    public function setPropertyAttribute($value)
    {
      return $this->attributes['property'] = ($this->type === Condition::TYPE_TAG) ? Condition::TYPE_TAG : $value;
    }
}
