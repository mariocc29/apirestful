<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\ApiHash; 

class Area extends Model
{
    use  SoftDeletes, ApiHash;

    protected $table    = 'interests.areas'; 
    protected $guarded  = ['id'];
    protected $fillable = ['branch_id', 'picture', 'order'];
    protected $hidden   = ['id', 'branch_id'];
    protected $dates    = ['created_at', 'updated_at', 'deleted_at'];
    protected $appends  = ['key'];

    public $timestamps  = true;

    public function branch()
    {
      return $this->belongsTo(\App\Models\Branch::class);
    }

    public function beacons()
    {
      return $this->hasMany(\App\Models\Beacon::class);
    }

    public function getPictureAttribute($value)
    {
      return \App\Helpers\ApiStorage::get( $value, 'areas' );  
    }

    public function setPictureAttribute($value)
    {
      if( filter_var($value, FILTER_VALIDATE_URL) === false ){
        return $this->attributes['picture'] = \App\Helpers\ApiStorage::save( $value, 'areas' );
      }

      return $this->attributes['picture'] = basename($value);
    }
}
