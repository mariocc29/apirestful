<?php

namespace App\Models;

use App\Jobs\Inputs\BranchJob;
use App\Jobs\Inputs\InputJob;
use App\Scopes\AppScope;
use App\Traits\ApiHash;
use App\Traits\ClearString;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; 

class Branch extends Model
{
    use  SoftDeletes, ApiHash, ClearString;

    protected $table    = 'interests.branches'; 
    protected $guarded  = ['id'];
    protected $fillable = ['app_id', 'label', 'tagname', 'description', 'geo_position', 'telephones', 'address', 'schedule'];
    protected $hidden   = ['id', 'app_id'];
    protected $dates    = ['created_at', 'updated_at', 'deleted_at'];
    protected $appends  = ['key'];

    public $timestamps  = true;

    protected static function boot()
    {
      parent::boot();
      static::addGlobalScope(new AppScope);

      Branch::created(function($branch){
        BranchJob::dispatch($branch, InputJob::TRIGGER_CREATED);
      });

      Branch::updated(function($branch){

        // Actualiza la geoposición de los beacons vinculados a las áreas de una sucursal
        if ($branch->isDirty('geo_position')) {
          $beacons = $branch->beacons;
          foreach ($beacons as $beacon) {
            $beacon->geo_position = $branch->geo_position;
            $beacon->update();
          }
        }

      });

      Branch::deleted(function($branch){
        BranchJob::dispatch($branch, InputJob::TRIGGER_DELETED);
      });
    }

    public function app()
    {
      return $this->belongsTo(\App\Models\App::class);
    }

    public function areas()
    {
      return $this->hasMany(\App\Models\Area::class);
    }

    public function covers()
    {
      return $this->belongsToMany(\App\Models\Cover::class);
    }

    public function products()
    {
      return $this->belongsToMany(\App\Models\Product::class, 'interests.branch_product');
    }

    public function users()
    {
      return $this->belongsToMany(\App\Models\User::class, 'interests.branch_user');
    }

    public function beacons()
    {
      return $this->hasManyThrough(\App\Models\Beacon::class, \App\Models\Area::class);
    }

    public function getDescriptionAttribute($value)
    {
      return json_decode($value);
    }

    public function setDescriptionAttribute($value)
    {
      return $this->attributes['description'] = json_encode($value);   
    }

    public function getGeoPositionAttribute($value)
    {
      return json_decode($value);
    }

    public function setGeoPositionAttribute($value)
    {
      return $this->attributes['geo_position'] = json_encode($value);   
    }

    public function getTelephonesAttribute($value)
    {
      return json_decode($value);   
    }

    public function setTelephonesAttribute($value)
    {
      return $this->attributes['telephones'] = json_encode($value);   
    }

    public function getScheduleAttribute($value)
    {
      return json_decode($value);   
    }

    public function setScheduleAttribute($value)
    {
      return $this->attributes['schedule'] = json_encode($value);   
    }

    public function getTagnameAttribute($value)
    {
      return "branch:{$tagname}";
    }

    public function setTagnameAttribute($value)
    {
      $tagname = strtolower( preg_replace('/branch:|([^a-zA-Z0-9_])/', '', $this->toUTF8($this->label) ) );
      return $this->attributes['tagname'] = $tagname;
    }
}
