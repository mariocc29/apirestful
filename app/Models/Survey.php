<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\ApiHash;

class Survey extends Model
{
  use  SoftDeletes, ApiHash;

  protected $table    = 'marketing.surveys'; 
  protected $guarded  = ['id'];
  protected $fillable = ['user_id', 'experience_id', 'tag', 'star'];
  protected $hidden   = ['id', 'user_id', 'experience_id'];
  protected $dates    = ['created_at', 'updated_at', 'deleted_at'];
  protected $appends  = ['key', 'user_key', 'experience_key'];

  public $timestamps  = true;

  public function experience()
  {
    return $this->belongsTo(\App\Models\Experience::class);
  }

  public function user()
  {
    return $this->belongsTo(\App\Models\User::class);
  }

  public function getTagAttribute($value)
  {
    return  json_decode($value); 
  }

  public function setTagAttribute($value)
  {
    return $this->attributes['tag'] =  json_encode($value);
  }

  public function getUserKeyAttribute($value)
  {
    return $this->hash_encode($this->user_id, \App\Models\User::class);
  }

  public function getExperienceKeyAttribute($value)
  {
    return $this->hash_encode($this->experience_id, \App\Models\Experience::class);
  }

  public function setUserIdAttribute($value)
  {
    return $this->attributes['user_id'] = $this->hash_decode($value, \App\Models\User::class);
  }

  public function setExperienceIdAttribute($value)
  {
    return $this->attributes['experience_id'] = $this->hash_decode($value, \App\Models\Experience::class);
  }
}
