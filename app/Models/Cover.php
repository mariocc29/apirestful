<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\ApiHash; 

class Cover extends Model
{
    use  SoftDeletes, ApiHash;

    protected $table    = 'content.covers';
    protected $guarded  = ['id'];
    protected $fillable = ['product_id', 'segment_id', 'title', 'description', 'start_at', 'end_at', 'schedule',  'status'];
    protected $hidden   = [ 'id', 'product_id', 'segment_id'];
    protected $dates    = ['created_at', 'updated_at', 'deleted_at'];
    protected $appends  = ['key', 'product_key', 'segment_key'];

    public $timestamps  = true; 

    public function branches()
    {
      return $this->belongsToMany( \App\Models\Branch::class);
    }

    public function product()
    {
      return $this->belongsTo( \App\Models\Product::class );  
    }

    public function segment()
    {
      return $this->belongsTo( \App\Models\Segment::class );  
    }

    public function galleries()
    {
      return $this->morphMany( \App\Models\Gallery::class, 'source');
    }

    public function getProductKeyAttribute($value)
    {
      return $this->hash_encode($this->product_id, \App\Models\Product::class);
    }

    public function getSegmentKeyAttribute($value)
    {
      return $this->hash_encode($this->segment_id, \App\Models\Segment::class);
    }
}

