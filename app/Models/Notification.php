<?php

namespace App\Models;

use App\Jobs\Inputs\InputJob;
use App\Jobs\Inputs\NotificationJob;
use App\Traits\ApiHash;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notification extends Model
{
  use  SoftDeletes, ApiHash;

  protected $table    = 'marketing.notifications';
  protected $guarded  = [ 'id'];
  protected $fillable = ['product_id', 'segment_id', 'title', 'message', 'sent', 'input'];
  protected $hidden   = [ 'id', 'product_id', 'segment_id', 'input'];
  protected $dates    = ['send_at', 'created_at', 'updated_at', 'deleted_at'];
  protected $appends  = ['key', 'segment_key'];

  public $timestamps  = true; 

  protected static function boot()
    {
      parent::boot();
      
      Notification::created(function($notification){
        NotificationJob::dispatch($notification, InputJob::TRIGGER_CREATED);
      });

      Notification::updated(function($notification){
        NotificationJob::dispatch($notification, InputJob::TRIGGER_UPDATED);
      });

      Notification::deleted(function($notification){
        NotificationJob::dispatch($notification, InputJob::TRIGGER_DELETED);
      });
    }

  public function product()
  {
    return $this->belongsTo( \App\Models\Product::class);
  }

  public function segment()
  {
    return $this->belongsTo( \App\Models\Segment::class); 
  }

  public function setTitleAttribute($value)
  {
    return $this->attributes['title'] = json_encode( $value );  
  }

  public function getTitleAttribute($value)
  {
    return json_decode( $value ); 
  }

  public function setMessageAttribute($value)
  {
    return $this->attributes['message'] = json_encode( $value );  
  }

  public function getMessageAttribute($value)
  {
    return json_decode( $value );  
  }

  public function setInputAttribute($value)
  {
    return $this->attributes['input'] = ( !is_null($value) ) ? json_encode( $value ) : null;
  }

  public function getInputAttribute($value)
  {
    return ( !is_null($value) ) ? json_decode( $value ) : null;
  }

  public function getSegmentKeyAttribute($value)
  {
    return $this->hash_encode($this->segment_id, \App\Models\Segment::class);
  }

  public function setSegmentIdAttribute($value)
  {
    return $this->attributes['segment_id'] = $this->hash_decode($value, \App\Models\Segment::class); 
  }

}
