<?php

namespace App\Models;

use App\Traits\ApiHash;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens; 

class App extends Authenticatable
{
    use  SoftDeletes, ApiHash, HasApiTokens;

    const IOS     = 'ios';
    const ANDROID = 'android';

    protected $table    = 'general.apps'; 
    protected $guarded  = ['id'];
    protected $fillable = ['name', 'email', 'appkey', 'deep_link_scheme', 'domain_url', 'status', 'additional_data'];
    protected $hidden   = ['id'];
    protected $dates    = ['created_at', 'updated_at', 'deleted_at'];
    protected $appends  = ['key', 'deep_link'];

    public $timestamps  = true;

    protected static function boot()
    {
      parent::boot();
      
      App::created(function($app){
        \App\Jobs\CreateServerAuthorization::dispatch($app);
      });

    }

    public function accounts()
    {
      return $this->hasMany(\App\Models\Account::class);
    }

    public function branches()
    {
      return $this->hasMany(\App\Models\Branch::class);
    }

    public function categories()
    {
      return $this->hasMany(\App\Models\Category::class);
    }

    public function campaigns_types()
    {
      return $this->belongsToMany(\App\Models\CampaignType::class, 'general.app_campaigns_types');
    }

    public function languages()
    {
      return $this->belongsToMany(\App\Models\Language::class, 'general.app_language')->withPivot('default');
    }

    public function products()
    {
      return $this->hasMany(\App\Models\Product::class);
    }

    public function inputs()
    {
      return $this->belongsToMany(\App\Models\Input::class, 'general.app_input')->using( \App\Models\AppInput::class )->withPivot(['additional_data']);
    }

    public function users()
    {
      return $this->hasMany(\App\Models\User::class);
    }

    public function setNameAttribute($value)
    {
      return $this->attributes['name'] = strtolower($value);
    }

    public function getAdditionalDataAttribute($value)
    {
      return json_decode($value);   
    }

    public function setAdditionalDataAttribute($value)
    {
      return $this->attributes['additional_data'] = json_encode($value);   
    }

    public function getDeepLinkAttribute($value)
    {
      return "{$this->deep_link_scheme}{$this->domain_url}";
    }
}
