<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\ApiHash;

class Code extends Model
{
  use  SoftDeletes, ApiHash;

  const STATUS_AVAILABLE  = 'available';
  const STATUS_RESERVED   = 'reserved';
  const STATUS_USED       = 'used';

  protected $table    = 'marketing.codes'; 
  protected $guarded  = ['id'];
  protected $fillable = ['experience_id', 'code', 'status', 'user_id'];
  protected $hidden   = ['id', 'experience_id', 'user_id'];
  protected $dates    = ['created_at', 'updated_at', 'deleted_at'];
  protected $appends  = ['key', 'experience_key', 'user_key'];

  public $timestamps  = true;

  public function experience()
  {
    return $this->belongsTo(\App\Models\Experience::class);
  }

  public function user()
  {
    return $this->belongsTo(\App\Models\User::class);
  }

  public function getExperienceKeyAttribute($value)
  {
    return $this->hash_encode($this->experience_id, \App\Models\Experience::class);
  }

  public function getUserKeyAttribute($value)
  {
    return $this->hash_encode($this->user_id, \App\Models\User::class);
  }

  public function setUserIdAttribute($value)
  {
    return $this->attributes['user_id'] = $this->hash_decode($value, \App\Models\User::class); 
  }

  public function setExperienceIdAttribute($value)
  {
    return $this->attributes['experience_id'] = $this->hash_decode($value, \App\Models\Experience::class); 
  }

}
