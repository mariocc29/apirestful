<?php

namespace App\Models;

use App\Jobs\Inputs\InputJob;
use App\Jobs\Inputs\ZoneJob;
use App\Traits\ApiHash;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Zone extends Model
{
    use  SoftDeletes, ApiHash;

    protected $table    = 'triggers.zones'; 
    protected $guarded  = ['id'];
    protected $fillable = ['geofence_id', 'label', 'description', 'input'];
    protected $hidden   = ['id', 'geofence_id', 'input'];
    protected $dates    = ['created_at', 'updated_at', 'deleted_at'];
    protected $appends  = ['key'];

    public $timestamps  = true;

    protected static function boot()
    {
      parent::boot();

      Zone::created(function($zone){
        ZoneJob::dispatch($zone, InputJob::TRIGGER_CREATED);
      });

      Zone::updated(function($zone){
        ZoneJob::dispatch($zone, InputJob::TRIGGER_UPDATED);
      });

      Zone::deleted(function($zone){
        ZoneJob::dispatch($zone, InputJob::TRIGGER_DELETED);
      });
    }

    public function beacons()
    {
      return $this->belongsToMany(\App\Models\Beacon::class, 'triggers.beacon_zone');
    }

    public function geofence()
    {
      return $this->belongsTo(\App\Models\Geofence::class);
    }

    public function experiences()
    {
      return $this->hasMany(\App\Models\Experience::class);
    }

    public function getInputAttribute($value)
    {
      return ( !is_null($value) ) ? json_decode($value) : null; 
    }

    public function setInputAttribute($value)
    {
      return $this->attributes['input'] = ( !is_null($value) ) ? json_encode($value) : null;
    }
}
