<?php

namespace App\Models;

use App\Jobs\Inputs\CampaignJob;
use App\Jobs\Inputs\InputJob;
use App\Traits\ApiHash;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; 

class Campaign extends Model
{
    use  SoftDeletes, ApiHash;

    protected $table    = 'marketing.campaigns'; 
    protected $guarded  = ['id'];
    protected $fillable = ['campaign_type_id', 'product_id', 'segment_id', 'name', 'description', 'schedule', 'input', 'status'];
    protected $hidden   = ['id', 'campaign_type_id', 'product_id', 'segment_id', 'input'];
    protected $dates    = ['start_at', 'end_at', 'created_at', 'updated_at', 'deleted_at'];
    protected $appends  = ['key', 'campaign_type_key', 'product_key', 'segment_key'];

    public $timestamps  = true;

    protected static function boot()
    {
      parent::boot();

      Campaign::created(function($campaign){
        if ($campaign->campaign_type->action === \App\Models\CampaignType::ACTION_PROXIMITY) {
          CampaignJob::dispatch($campaign, InputJob::TRIGGER_CREATED);
        }
      });

      Campaign::updated(function($campaign){
        if ($campaign->campaign_type->action === \App\Models\CampaignType::ACTION_PROXIMITY) {
          CampaignJob::dispatch($campaign, InputJob::TRIGGER_UPDATED);
        }
      });

      Campaign::deleted(function($campaign){
        if ($campaign->campaign_type->action === \App\Models\CampaignType::ACTION_PROXIMITY) {
          CampaignJob::dispatch($campaign, InputJob::TRIGGER_DELETED);
        }
      });
    }

    public function campaign_type()
    {
      return $this->belongsTo(\App\Models\CampaignType::class);
    }

    public function experiences()
    {
      return $this->hasMany(\App\Models\Experience::class);
    }

    public function product()
    {
      return $this->belongsTo(\App\Models\Product::class);
    }

    public function segment()
    {
      return $this->belongsTo(\App\Models\Segment::class);
    }

    public function getNameAttribute($value)
    {
      return json_decode($value);   
    }

    public function setNameAttribute($value)
    {
      return $this->attributes['name'] = json_encode($value);   
    }

    public function getDescriptionAttribute($value)
    {
      return json_decode($value);   
    }

    public function setDescriptionAttribute($value)
    {
      return $this->attributes['description'] = json_encode($value);   
    }

    public function getScheduleAttribute($value)
    {
      return json_decode($value);   
    }

    public function setScheduleAttribute($value)
    {
      return $this->attributes['schedule'] = json_encode($value);   
    }

    public function getInputAttribute($value)
    {
      return ( !is_null($value) ) ? json_decode($value) : null; 
    }

    public function setInputAttribute($value)
    {
      return $this->attributes['input'] = ( !is_null($value) ) ? json_encode($value) : null;   
    }

    public function getCampaignTypeKeyAttribute($value)
    {
      return $this->hash_encode($this->campaign_type_id, \App\Models\CampaignType::class);
    }

    public function getProductKeyAttribute($value)
    {
      return $this->hash_encode($this->product_id, \App\Models\Product::class);
    }

    public function getSegmentKeyAttribute($value)
    {
      return $this->hash_encode($this->segment_id, \App\Models\Segment::class);
    }

    public function setCampaignTypeIdAttribute($value)
    {
      return $this->attributes['campaign_type_id'] = $this->hash_decode($value, \App\Models\CampaignType::class);
    }

    public function setSegmentIdAttribute($value)
    {
      return $this->attributes['segment_id'] = $this->hash_decode($value, \App\Models\Segment::class);
    }

    public function setStartAtAttribute($value)
    {
      return $this->attributes['start_at'] = Carbon::parse($value)->startOfDay();   
    }

    public function setEndAtAttribute($value)
    {
      return $this->attributes['end_at'] = Carbon::parse($value)->endOfDay();   
    }

}
