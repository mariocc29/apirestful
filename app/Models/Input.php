<?php

namespace App\Models;

use App\Traits\ApiHash;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Input extends Model
{
  use  SoftDeletes, ApiHash;

  const MOCA = 'moca';
  const LNSR = 'lnsr';

  protected $table    = 'general.inputs'; 
  protected $guarded  = ['id'];
  protected $fillable = ['label', 'code', 'status'];
  protected $hidden   = ['id'];
  protected $dates    = ['created_at', 'updated_at', 'deleted_at'];
  protected $appends  = ['key'];

  public $timestamps  = true;

  public function apps()
  {
    return $this->belongsToMany( \App\Models\App::class, 'general.app_input')->withPivot(['additional_data']);
  }
}
