<?php

namespace App\Providers;

use App\Services\RouterExtended;
use App\Services\ValidatorExtended;
use Illuminate\Routing\ResourceRegistrar;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Agrega el método search en todos los controladores
        $register = new RouterExtended($this->app['router']);
        $this->app->bind(ResourceRegistrar::class, function () use ($register) {
            return $register;
        });

        // Agrega los validadores personalizados
        Validator::resolver(function($translator, $data, $rules, $messages = [], $customAttributes = []){
          return new ValidatorExtended($translator, $data, $rules, $messages, $customAttributes);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
