<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::tokensCan([
          'server_read'   => 'Lectura de registros en la conexión s2s (server to server)',
          'server_write'  => 'Escritura de registros en la conexión s2s (server to server)',
          'console_read'  => 'Lectura de registros en la conexión desde consola',
          'console_write' => 'Escritura de registros en la conexión desde consola',
          'app_read'      => 'Lectura de registros en la conexión desde las aplicaciones in-house',
          'app_write'     => 'Escritura de registros en la conexión desde las aplicaciones in-house',
        ]);
    }
}
