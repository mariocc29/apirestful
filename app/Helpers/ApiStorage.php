<?php

namespace App\Helpers;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Config;

/**
 * Clase para el almacenamiento de archivos en el storage
 */
class ApiStorage
{

  const FILENAME_SIZE = 41;

  const LOCAL_STORAGE = 'enterprises';
  
  public static function get(string $filename, string $source)
  {
    $enterprise = request()->get('application')->key;
    if ( Config::get('filesystems.default') === ApiStorage::LOCAL_STORAGE ) {
      return url("public/storage/{$enterprise}/{$source}/{$filename}");
    } else {
      return ApiStorage::get_cloud("{$enterprise}/{$source}", $filename);
    }
  }

  public static function save(UploadedFile $file, string $source)
  {
    $enterprise = request()->get('application')->key;
    if ( Config::get('filesystems.default') === ApiStorage::LOCAL_STORAGE ) {
      return basename($file->store("{$enterprise}/{$source}"));
    } else {
      return ApiStorage::set_cloud("{$enterprise}/{$source}", $file);
    }
  }

  /**
   * Almacena los ficheros en el storage cloud
   * @param  [type] $directory [description]
   * @param  [type] $file      [description]
   * @return [type]            [description]
   */
  private static function set_cloud($directory, $file)
  {
    $filename = str_random(ApiStorage::FILENAME_SIZE).".{$file->extension()}";

    $storage = new StorageClient([
          'projectId'   => env('GOOGLE_CLOUD_PROJECT_ID'),
          'keyFilePath' => env('GOOGLE_CLOUD_KEY_FILE')
        ]);

    $bucket = $storage->bucket(env('GOOGLE_CLOUD_STORAGE_BUCKET'));
    $bucket->upload(fopen($file, 'r'), ['name' => "{$directory}/{$filename}", 'predefinedAcl' => 'publicRead']);
    return $filename;
  }

  private static function get_cloud($directory, $filename)
  {
    return "https://".env('GOOGLE_CLOUD_STORAGE_BUCKET').".storage.googleapis.com/{$directory}/{$filename}";;
  }


}