<?php

namespace App\Helpers;

use Carbon\Carbon;
use GuzzleHttp\Psr7\Uri;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class ApiLog
{

    const FORMAT = "%datetime% [%level_name%] (%channel%): %message%\n";

    private static $channels = [];

    private static $request;

    private static $response;

    private static $exception;

    private static $payload;

    private static function writeLog($channel, $message, $level)
    {      
        if ( !isset(ApiLog::$channels[$channel]) ) {
          ApiLog::$channels[$channel] = new Logger($channel);
        }

        $today      = Carbon::today()->format('Y-m-d');
        $level_name = strtolower(Logger::getLevelName($level));

        $handler   = new StreamHandler(storage_path("logs/laravel_{$level_name}-{$today}.log"), $level);
        
        $formatter = new LineFormatter( ApiLog::FORMAT );
        $handler->setFormatter($formatter);

        ApiLog::$channels[$channel]->pushHandler($handler);

        ApiLog::$channels[$channel]->{$level_name}($message);
    }

    public static function getUri($exception)
    {
        $uri = $exception->getRequest()->getUri();
        return "{$uri->getScheme()}://{$uri->getHost()}{$uri->getPath()}?{$uri->getQuery()}";
    }

    public static function errorFormatter($exception)
    {
      ApiLog::$request   = ApiLog::getUri($exception);
      
      if ( $exception instanceof \GuzzleHttp\Exception\ClientException ) {
        ApiLog::$exception = \GuzzleHttp\Exception\ClientException::class;
        $response = json_decode($exception->getResponse()->getBody()->getContents());
        ApiLog::$response = ( !is_null($response) ) ? $response->error : $exception->getResponse()->getReasonPhrase();
      } elseif ( $exception instanceof \GuzzleHttp\Exception\ConnectException ) {
        ApiLog::$exception = \GuzzleHttp\Exception\ConnectException::class;
        ApiLog::$response = $exception->getMessage();
      } elseif ( $exception instanceof \Exception ) {
        ApiLog::$exception = \Exception::class;
        ApiLog::$response = $exception->getMessage();
      } else {
        exit( get_class($exception).' Not yet implemented in '.__CLASS__.PHP_EOL );
      }

      return (new static);
    }

    public static function successFormatter($request, $response)
    {
      ApiLog::$request  = $request;      
      ApiLog::$response = $response;      
      return (new static);
    }

    public static function setPayload($value)
    {
      ApiLog::$payload = $value;
    }

    public static function getRequest()
    {
      return ApiLog::$request;
    }

    public static function getResponse()
    {
      return ApiLog::$response;
    }

    public static function getLogMessage()
    {
      $message = 'EXCEPTION['.ApiLog::$exception.']|REQUEST['.ApiLog::$request.']|RESPONSE['.ApiLog::$response.']';
      if ( !is_null(ApiLog::$payload) ) {
        $message .= '|PAYLOAD['.ApiLog::$payload.']'; 
      }
      return $message;
    }

    public static function debug($channel, $message)
    {
      ApiLog::writeLog($channel, $message, Logger::DEBUG);
    }

    public static function info($channel, $message)
    {
      ApiLog::writeLog($channel, $message, Logger::INFO);
    }

    public static function notice($channel, $message)
    {
      ApiLog::writeLog($channel, $message, Logger::NOTICE);
    }

    public static function warning($channel, $message)
    {
      ApiLog::writeLog($channel, $message, Logger::WARNING);
    }

    public static function error($channel, $message)
    {
      ApiLog::writeLog($channel, $message, Logger::ERROR);
    }

    public static function critical($channel, $message)
    {
      ApiLog::writeLog($channel, $message, Logger::CRITICAL);
    }

    public static function alert($channel, $message)
    {
      ApiLog::writeLog($channel, $message, Logger::ALERT);
    }

    public static function emergency($channel, $message)
    {
      ApiLog::writeLog($channel, $message, Logger::EMERGENCY);
    }
}