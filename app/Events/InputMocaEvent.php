<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;

class InputMocaEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $trigger;

    public $path;

    public $payload;

    public $resource_id;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(string $trigger, string $path, Collection $payload = null, string $resource_id = null)
    {
        $this->trigger     = $trigger;
        $this->path        = $path;
        $this->payload     = $payload;
        $this->resource_id = $resource_id;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-moca');
    }
}
