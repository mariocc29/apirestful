<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class StaffGenerateSecret extends Command
{
    const ENV_KEY = 'UBERBLUG_TOKEN';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ucontext:staff {--force=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set el token secreto usado por uberflug para la administración de los endpoints exclusivos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $token = str_random( rand(30, 100) );

        if (file_exists($path = $this->envPath()) === false) {
          return $this->displayKey($token);
        }

        if (Str::contains(file_get_contents($path), self::ENV_KEY) === false) {
          
          file_put_contents($path, PHP_EOL.self::ENV_KEY."={$token}", FILE_APPEND);

        } else {
          
          if ($this->isConfirmed() === false) {
            $this->comment('No hubo cambios en el token de acceso de la consola.');
            exit();
          }

          file_put_contents($path, str_replace(
              self::ENV_KEY."={$this->laravel['config']['uberflug.token']}",
              self::ENV_KEY."={$token}", file_get_contents($path)
            ));
        }

        $this->displayKey($token);
    }

    private function envPath()
    {
        if (method_exists($this->laravel, 'environmentFilePath')) {
          return $this->laravel->environmentFilePath();
        }

        return $this->laravel->basePath('.env');
    }

    private function displayKey($token)
    {
        $this->laravel['config']['uberflug.token'] = $token;
        $this->info("ucontext console token [$token] set successfully.");
    }

    protected function isConfirmed()
    {
        return $this->option('force') ? true : $this->confirm(
            'Esta acción eliminará el acceso de la consola de UBERFLUG COLOMBIA S.A.S. actual. ¿Está seguro de sobre-escribir el token secreto?'
        );
    }
}
