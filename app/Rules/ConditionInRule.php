<?php

namespace App\Rules;

use App\Models\Condition;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Config;

class ConditionInRule extends BaseRule implements Rule
{

    private $type;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($type)
    {
        $this->type      = $type;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ( !is_null($this->type) ) {
          if ( $this->type === Condition::TYPE_TAG ) {
            if ( !$this->validate($value, ['required', 'string', 'in:contains'], $attribute) ) {
              return false;
            }
          } elseif($this->type === Condition::TYPE_CUSTOM_PROPERTY) {
            $conditions = collect( Config::get('constants.segments.operators') )->collapse()->unique()->toArray();
            if ( !$this->validate($value, ['required', 'string', 'in:'.implode(',', $conditions)], $attribute) ) {
              return false;
            }
          }

          return true;
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        $errors = $this->getValidator()->errors();

        if ($errors->any()) {
          return $errors->all();
        }
    
        return __('validation.exists');
    }
}
