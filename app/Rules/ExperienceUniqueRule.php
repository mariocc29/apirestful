<?php

namespace App\Rules;

use App\Models\Experience;
use Illuminate\Contracts\Validation\Rule;

class ExperienceUniqueRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $custom_property = Experience::join('marketing.campaigns', 'marketing.campaigns.id', 'marketing.experiences.campaign_id')
          ->join('interests.products', 'interests.products.id', 'marketing.campaigns.product_id')
          ->where('interests.products.app_id', request()->get('application')->id)
          ->count();

        return ($custom_property === 0);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('validation.unique');
    }
}
