<?php
namespace App\Services;

use App\Traits\ApiHash;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Validator;

/**
 * Clases extendidas para Validation
 */
class ValidatorExtended extends Validator
{

  use ApiHash;

  private $_custom_messages = [
      "auth"       => "The :attribute must be auth format {'field':'nickname|email|dni','has_password':true|false}",
      "geo"        => "The :attribute must be geo format {'latitude':'','longitude':''}",
      "exists_key" => "The :attribute does not exists",
      "language"   => "The :attribute must be language format {'es':'', 'en':...}",
      "position"   => "The :attribute must be geo format {'top':'','left':''}",
      "schedule"   => "The :attribute must be schedule format {'mon':{'start_at':'08:00', 'end_at':'17:00'},'tue'...}",
      "domain"     => "The :attribute must be domain format example.com (without protocol http, https, etc)",
      "colorhex"   => "The :attribute must be hexadecimal color",
    ];

  public function __construct( $translator, $data, $rules, $messages = array(), $customAttributes = array() ) {
      parent::__construct( $translator, $data, $rules, $messages, $customAttributes );
      $this->_set_custom_stuff();
   }

   protected function _set_custom_stuff() {
      $this->setCustomMessages( $this->_custom_messages );
   }
  
  /**
   * Formato para almacenar los lenguages
   * @return boolean           Cumple con la estructura
   */
  protected function validateLanguage( $attribute, $value, $parameters ) {
    if ( !is_array($value) ) {
      return false;
    }
    
    $collection = collect($value);
    $response   = $collection->keys()->filter(function($lang){
      return in_array($lang, array_keys(Config::get('isolangs')));
    });

    return ($response->count() === $collection->count());
  }

  /**
   * Formato para almacenar las geoposiciones
   * @return boolean           Cumple con la estructura
   */
  protected function validateGeo( $attribute, $value, $parameters ) {
    if ( !is_array($value) ) {
      return false;
    }
    
    $collection = collect($value);
    $response   = $collection->keys()->filter(function($key){
      return in_array($key, ['latitude', 'longitude']);
    });

    return ($response->count() === $collection->count());
  }

  /**
   * Formato para almacenar las posiciones XY
   * @return boolean           Cumple con la estructura
   */
  protected function validatePosition( $attribute, $value, $parameters ) {
    if ( !is_array($value) ) {
      return false;
    }
    
    $collection = collect($value);
    $response   = $collection->keys()->filter(function($key){
      return in_array($key, ['top', 'left']);
    });

    return ($response->count() === $collection->count());
  }

  /**
   * Formato para almacenar los horarios
   * @return boolean           Cumple con la estructura
   */
  protected function validateSchedule( $attribute, $value, $parameters ) {
    if ( !is_array($value) ) {
      return false;
    }
    
    $collection = collect($value);
    $week       = ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat'];
    $response   = $collection->keys()->filter(function($day) use ($week, $collection){
      $rule_values = array_diff( array_keys($collection->get($day)), ['start_at','end_at']);
      return in_array($day, $week) && count($rule_values) === 0;
    });

    return ($response->count() === $collection->count());
  }

  /**
   * Valida si la key existe
   * @return boolean           Cumple con la estructura
   */
  protected function validateExistsKey( $attribute, $value, $parameters ) {
    
    // Transforma de plural a singular cada nombre de la entidad
    $composite_names = preg_split("/[_]/", $parameters[1]);
    $composite_model = null;
    foreach ($composite_names as $index) {
      $composite_model .= studly_case(str_singular($index));
    }

    $model_name = "App\Models\\{$composite_model}";
    $id         = $this->hash_decode($value, $model_name);

    $register = DB::connection($parameters[0])->table( $parameters[1] )->select( $parameters[2] );
    $register->where( $parameters[2], $id );

    $options = array_slice($parameters, 3);
    
    for ($index=0; $index < count($options); $index = $index + 2) { 
      switch ( $options[$index + 1] ) {
        case 'NULL':
            $register->whereNull( $options[$index] );
          break;

        case 'NOT_NULL':
            $register->whereNotNull( $options[$index] );
          break;
        
        default:
            $register->where( $options[$index], $options[$index + 1] );
          break;
      }      
    }

    return ( $register->count() > 0 ) ? true : false;
  }

  /**
   * Formato para almacenar el additional_data de apps
   * @return boolean           Cumple con la estructura
   */
  protected function validateAuth( $attribute, $value, $parameters ) {
    if ( !is_array($value) ) {
      return false;
    }

    $collection = collect($value);
    
    if ($collection->has('field') && $collection->has('has_password') &&
        in_array( $collection->get('field'), ['nickname','email','dni']) &&
        is_bool($collection->get('has_password'))) {
      
          return true;
    }

    return false;
  }

  /**
   * Formato para almacenar el additional_data de apps
   * @return boolean           Cumple con la estructura
   */
  protected function validateDomain( $attribute, $value, $parameters ) {
    if ( !is_string($value) ) {
      return false;
    }

    $pattern = '/^[a-zA-Z0-9]{1,}\.([a-zA-Z]{2,})(\.([a-zA-Z]{2,}))?$/';
    preg_match($pattern, $value, $matches);

    return ( count($matches) > 0 );
  }

  /**
   * Formato para validar el color hexadecimal
   * @return boolean           Cumple con la estructura
   */
  protected function validateColorHex( $attribute, $value, $parameters ) {
    if ( !is_string($value) ) {
      return false;
    }

    $pattern = '/^#([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$/';
    preg_match($pattern, $value, $matches);

    return ( count($matches) > 0 );
  }
}