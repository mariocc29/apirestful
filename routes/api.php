<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function(){

  // Rutas exclusivas para el equipo Uberflug Colombia S.A.S.
  Route::group(['prefix' => 'staff', 'middleware' => ['uberflugcallonly']], function(){
    Route::resource('apps', 'v1\AppController');
    Route::resource('apps.campaigntype', 'v1\AppCampaignTypeController',['only' => ['index', 'update', 'destroy']]);
    Route::resource('apps.inputs', 'v1\AppInputController', ['only' => ['index', 'update', 'destroy']]);
    Route::resource('apps.languages', 'v1\AppLanguageController', ['only' => ['index', 'update', 'destroy']]);
    Route::resource('inputs', 'v1\InputController');
    Route::resource('languages', 'v1\LanguageController');
  });
  
  Route::name('login')->post('login',       ['uses' => 'v1\AuthController@signIn']);
  Route::name('remember')->post('remember', ['uses' => 'v1\AuthController@rememberPassword']);
  
  // Rutas taton para conexión de la consola, app y server to server
  Route::group(['middleware' => ['apiauth']], function(){
  
    // Registro de usuarios
    Route::name('users')->post('users', ['uses' => 'v1\UserController@store']);

    Route::group(['middleware' => ['auth:api']], function(){
      #Updates#
      Route::name('products')->post('products/{product}',       ['uses' => 'v1\ProductController@update']);
      Route::name('accounts')->post('accounts/{account}',       ['uses' => 'v1\AccountController@update']);
      Route::name('galleries')->post('galleries/{gallery}',     ['uses' => 'v1\GalleryController@update']);
      Route::name('profile')->post('profile',                   ['uses' => 'v1\ProfileController@update']);
      Route::name('categories')->post('categories/{category}',  ['uses' => 'v1\CategoryController@update']);
      ########
      Route::name('beacons')->get('beacons/free',    ['uses' => 'v1\BeaconController@freeIndex']);

      // Obtención de analíticas
      Route::name('analytics')->get('analytics', ['uses' => 'v1\AnalyticController@index']);

      Route::resource('accounts', 'v1\AccountController', ['except' => ['update']]);
      Route::resource('products.accounts', 'v1\ProductAccountController', ['only' => ['index', 'update', 'destroy']]);
      Route::resource('administrators', 'v1\AdministratorController', ['only' => ['index', 'show']]);
      Route::resource('areas', 'v1\AreaController', ['only' => ['show', 'update', 'destroy']]);
      Route::resource('areas.beacons', 'v1\AreaBeaconController', ['only' => ['index', 'update', 'destroy']]);
      Route::resource('beacons', 'v1\BeaconController');
      Route::resource('beacons.areas', 'v1\BeaconAreaController', ['only' => ['destroy']]);
      Route::resource('beacons.zones', 'v1\BeaconZoneController', ['only' => ['index', 'update', 'destroy']]);
      Route::resource('branches', 'v1\BranchController');
      Route::resource('branches.areas', 'v1\BranchAreaController', ['only' => ['index', 'store']]);
      Route::resource('branches.products', 'v1\BranchProductController', ['only' => ['index', 'update', 'destroy']]);
      Route::resource('campaigns', 'v1\CampaignController', ['only' => ['show', 'update', 'destroy']]);
      Route::resource('campaigns.experiences', 'v1\CampaignExperienceController', ['only' => ['index', 'store']]);
      Route::resource('campaigntypes', 'v1\CampaignTypeController', ['only' => ['index']]);
      Route::resource('categories', 'v1\CategoryController', ['except' => ['update']]);
      Route::resource('categories.products', 'v1\CategoryProductController', ['only' => ['index', 'update', 'destroy']]);
      Route::resource('categories.subcategories', 'v1\CategorySubcategoryController', ['only' => ['index', 'search']]); 
      Route::resource('codes', 'v1\CodeController', ['only' => ['store', 'update', 'destroy']]);
      Route::resource('communities', 'v1\CommunityController', ['only' => ['index', 'show']]);
      Route::resource('communities.products', 'v1\CommunityProductController', ['only' => ['index', 'update', 'destroy']]);
      Route::resource('conditions', 'v1\ConditionController', ['only' => ['update', 'destroy']]);
      Route::resource('covers.galleries', 'v1\CoverGalleryController', ['only' => ['store']]);
      Route::resource('customproperties', 'v1\CustomPropertyController', ['only' => ['update', 'destroy']]);
      Route::resource('experiences', 'v1\ExperienceController', ['only' => ['update', 'destroy', 'show']]);
      Route::resource('experiences.galleries', 'v1\ExperinceGalleryController', ['only' => ['store']]);
      Route::resource('experiences.printings', 'v1\ExperiencePrintingController', ['only' => ['index', 'show']]);
      Route::resource('galleries', 'v1\GalleryController',  ['only' => ['destroy']]);
      Route::resource('geofences', 'v1\GeofenceController', ['only' => ['show', 'update', 'destroy']]);
      Route::resource('geofences.zones', 'v1\GeofenceZoneController', ['only' => ['index', 'store']]);
      Route::resource('interests', 'v1\InterestController', ['only' => ['index']]);
      Route::resource('languages', 'v1\LanguageController', ['only' => ['index']]);
      Route::resource('notifications', 'v1\NotificationController', ['only' => ['show', 'destroy']]);
      Route::resource('notifications.galleries', 'v1\NotificationGalleryController', ['only' => ['store']]);
      Route::resource('operators', 'v1\SegmentOperatorController', ['only' => ['index']]);
      Route::resource('printings', 'v1\PrintingController', ['only' => ['store', 'show', 'update']]);
      Route::resource('products', 'v1\ProductController', ['except' => ['update']]);
      Route::resource('products.beacons', 'v1\ProductBeaconController', ['only' => ['index']]);
      Route::resource('products.branches', 'v1\ProductBranchController', ['only' => ['index', 'update', 'destroy']]);
      Route::resource('products.campaigns', 'v1\ProductCampaignController', ['only' => ['index', 'store', 'search']]);
      Route::resource('products.categories', 'v1\ProductCategoryController', ['only' => ['index', 'update', 'destroy']]);
      Route::resource('products.geofences', 'v1\ProductGeofenceController', ['only' => ['index', 'store', 'search']]); 
      Route::resource('products.notifications', 'v1\ProductNotificationController', ['only' => ['index', 'store', 'search']]);
      Route::resource('products.segments', 'v1\ProductSegmentController', ['only' => ['index', 'store', 'search']]); 
      Route::resource('products.experiences', 'v1\ProductExperienceController', ['only' => ['index']]);
      Route::resource('repeats', 'v1\RepeatController', ['only' => ['index']]);
      Route::resource('roles', 'v1\RoleController', ['only' => ['index']]);
      Route::resource('segments', 'v1\SegmentController', ['only' => ['show', 'update', 'destroy']]);
      Route::resource('segments.conditions', 'v1\SegmentConditionController', ['only' => ['index', 'store']]);
      Route::resource('segments.inputs', 'v1\SegmentInputController', ['only' => ['index']]);
      Route::resource('stickers', 'v1\StickerController', ['only' => ['store', 'update', 'destroy']]);
      Route::resource('surveys', 'v1\SurveyController', ['only' => ['store', 'update']]);
      Route::resource('users', 'v1\UserController', ['except' => ['store']]);
      Route::resource('users.branches', 'v1\UserBranchController', ['only' => ['index', 'update', 'destroy']]);
      Route::resource('users.customsproperties', 'v1\UserCustomPropertyController', ['only' => ['index', 'store']]);
      Route::resource('users.interests', 'v1\UserInterestController', ['only' => ['index', 'update', 'destroy']]);
      Route::resource('users.printings', 'v1\UserPrintingController', ['only' => ['index', 'show']]);
      Route::resource('zones', 'v1\ZoneController', ['only' => ['show', 'update', 'destroy']]);
      Route::resource('zones.beacons', 'v1\ZoneBeaconController', ['only' => ['index', 'update', 'destroy']]);
    });
  });
});
