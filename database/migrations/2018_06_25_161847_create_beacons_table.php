<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBeaconsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('triggers.beacons', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('app_id')->unsigned();
            $table->string('label', 50);
            $table->text('description');
            $table->string('uuid', 255);
            $table->integer('major');
            $table->integer('minor'); 
            $table->json('geo_position')->nullable();
            $table->json('position')->nullable();
            $table->integer('area_id')->nullable()->unsigned();
            $table->json('input')->nullable();

            $table->unique(['uuid', 'major', 'minor']);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('triggers.beacons');
    }
}
