<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBeaconZoneTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('triggers.beacon_zone', function (Blueprint $table) {
            $table->integer('beacon_id')->unsigned();
            $table->integer('zone_id')->unsigned();
            $table->unique(['beacon_id', 'zone_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('triggers.beacon_zone');
    }
}
