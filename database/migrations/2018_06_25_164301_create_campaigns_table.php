<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marketing.campaigns', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('campaign_type_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->integer('segment_id')->nullable()->unsigned();
            $table->json('name');
            $table->json('description');
            $table->timestamp('start_at');
            $table->timestamp('end_at');
            $table->json('schedule');
            $table->json('input')->nullable();
            $table->boolean('status');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marketing.campaigns');
    }
}
