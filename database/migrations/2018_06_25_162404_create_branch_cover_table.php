<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchCoverTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content.branch_cover', function (Blueprint $table) {
            $table->integer('branch_id')->unsigned();
            $table->integer('cover_id')->unsigned();
            $table->unique(['branch_id', 'cover_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content.branch_cover');
    }
}
