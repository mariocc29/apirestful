<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interests.categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('app_id')->unsigned();
            $table->json('label');
            $table->string('tagname');
            $table->json('description')->nullable();
            $table->string('picture', 255)->nullable();
            $table->string('fontcolor', 7)->nullable();
            $table->string('backcolor', 7)->nullable();
            $table->integer('category_id')->nullable()->unsigned();
            $table->json('input')->nullable();

            $table->timestamps();
            $table->softDeletes();
            $table->unique(['app_id', 'tagname']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
