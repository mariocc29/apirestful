<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExperiencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marketing.experiences', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('campaign_id')->unsigned();
            $table->json('name');
            $table->json('description');
            $table->json('message_notification');
            $table->boolean('enabled');
            $table->char('attachement_type', 5);
            $table->integer('geofence_id')->unsigned()->nullable();
            $table->integer('zone_id')->unsigned()->nullable();
            $table->integer('beacon_id')->unsigned()->nullable();
            $table->string('trigger', 10);
            $table->string('url', 255)->nullable();
            $table->string('custom_property_label', 255)->nullable()->unique();
            $table->string('custom_property_key', 255)->nullable()->unique();
            $table->json('custom_property_values')->nullable();
            $table->integer('points')->nullable();
            $table->integer('probability')->nullable();
            $table->integer('steps')->nullable();
            $table->integer('goal')->nullable();
            $table->integer('repeat_id')->nullable();
            $table->integer('reminder_hours')->nullable();
            $table->json('input')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marketing.experiences');
    }
}
