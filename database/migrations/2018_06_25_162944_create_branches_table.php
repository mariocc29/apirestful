<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interests.branches', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('app_id')->unsigned();
            $table->string('label');
            $table->string('tagname');
            $table->json('description');
            $table->json('geo_position');   
            $table->json('telephones');
            $table->text('address');
            $table->json('schedule');

            $table->timestamps();
            $table->softDeletes();
            $table->unique(['app_id', 'tagname']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interests.branches');
    }
}
