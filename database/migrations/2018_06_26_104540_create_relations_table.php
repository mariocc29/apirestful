<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {        
        Schema::table('general.account_product', function(Blueprint $table) {
            $table->foreign('community_id')->references('id')->on('general.accounts')->onUpdate('cascade')->onDelete('restrict');
            $table->foreign('product_id')->references('id')->on('interests.products')->onUpdate('cascade')->onDelete('restrict');            
        });

        Schema::table('general.accounts', function(Blueprint $table) {
            $table->foreign('app_id')->references('id')->on('general.apps')->onUpdate('cascade')->onDelete('restrict');
            $table->foreign('role_id')->references('id')->on('general.roles')->onUpdate('cascade')->onDelete('restrict');            
        });

        Schema::table('general.app_campaigns_types', function(Blueprint $table) {
            $table->foreign('app_id')->references('id')->on('general.apps')->onUpdate('cascade')->onDelete('restrict');
            $table->foreign('campaign_type_id')->references('id')->on('general.campaigns_types')->onUpdate('cascade')->onDelete('restrict');            
        });

        Schema::table('general.app_input', function(Blueprint $table) {
            $table->foreign('app_id')->references('id')->on('general.apps')->onUpdate('cascade')->onDelete('restrict');
            $table->foreign('input_id')->references('id')->on('general.inputs')->onUpdate('cascade')->onDelete('restrict');            
        });

        Schema::table('general.app_language', function(Blueprint $table) {
            $table->foreign('app_id')->references('id')->on('general.apps')->onUpdate('cascade')->onDelete('restrict');
            $table->foreign('language_id')->references('id')->on('general.languages')->onUpdate('cascade')->onDelete('restrict');            
        });

        Schema::table('interests.areas', function(Blueprint $table) {
            $table->foreign('branch_id')->references('id')->on('interests.branches')->onUpdate('cascade')->onDelete('restrict');           
        });

        Schema::table('marketing.beacon_product', function(Blueprint $table) {
            $table->foreign('beacon_id')->references('id')->on('triggers.beacons')->onUpdate('cascade')->onDelete('restrict');  
            $table->foreign('product_id')->references('id')->on('interests.products')->onUpdate('cascade')->onDelete('restrict'); 
        });

        Schema::table('triggers.beacon_zone', function(Blueprint $table) {
            $table->foreign('beacon_id')->references('id')->on('triggers.beacons')->onUpdate('cascade')->onDelete('restrict');  
            $table->foreign('zone_id')->references('id')->on('triggers.zones')->onUpdate('cascade')->onDelete('restrict'); 
        });

        Schema::table('triggers.beacons', function(Blueprint $table) {
            $table->foreign('app_id')->references('id')->on('general.apps')->onUpdate('cascade')->onDelete('restrict');  
            $table->foreign('area_id')->references('id')->on('interests.areas')->onUpdate('cascade')->onDelete('restrict'); 
        });

        Schema::table('content.branch_cover', function(Blueprint $table) {
            $table->foreign('branch_id')->references('id')->on('interests.branches')->onUpdate('cascade')->onDelete('restrict');  
            $table->foreign('cover_id')->references('id')->on('content.covers')->onUpdate('cascade')->onDelete('restrict'); 
        });

        Schema::table('interests.branch_product', function(Blueprint $table) {
            $table->foreign('branch_id')->references('id')->on('interests.branches')->onUpdate('cascade')->onDelete('restrict');  
            $table->foreign('product_id')->references('id')->on('interests.products')->onUpdate('cascade')->onDelete('restrict'); 
        });

        Schema::table('interests.branch_user', function(Blueprint $table) {
            $table->foreign('branch_id')->references('id')->on('interests.branches')->onUpdate('cascade')->onDelete('restrict');  
            $table->foreign('user_id')->references('id')->on('general.users')->onUpdate('cascade')->onDelete('restrict'); 
        });

        Schema::table('interests.branches', function(Blueprint $table) {
            $table->foreign('app_id')->references('id')->on('general.apps')->onUpdate('cascade')->onDelete('restrict');  
        });

        Schema::table('marketing.campaigns', function(Blueprint $table) {
            $table->foreign('campaign_type_id')->references('id')->on('general.campaigns_types')->onUpdate('cascade')->onDelete('restrict');
            $table->foreign('product_id')->references('id')->on('interests.products')->onUpdate('cascade')->onDelete('restrict');  
            $table->foreign('segment_id')->references('id')->on('marketing.segments')->onUpdate('cascade')->onDelete('restrict');  
        });

        Schema::table('interests.categories', function(Blueprint $table) {
            $table->foreign('app_id')->references('id')->on('general.apps')->onUpdate('cascade')->onDelete('restrict');
            $table->foreign('category_id')->references('id')->on('interests.categories')->onUpdate('cascade')->onDelete('restrict');
        });

        Schema::table('marketing.codes', function(Blueprint $table) {
            $table->foreign('experience_id')->references('id')->on('marketing.experiences')->onUpdate('cascade')->onDelete('restrict');
            $table->foreign('user_id')->references('id')->on('general.users')->onUpdate('cascade')->onDelete('restrict');
        });

        Schema::table('marketing.conditions', function(Blueprint $table) {
            $table->foreign('segment_id')->references('id')->on('marketing.segments')->onUpdate('cascade')->onDelete('restrict');
        });

        Schema::table('content.covers', function(Blueprint $table) {
            $table->foreign('product_id')->references('id')->on('interests.products')->onUpdate('cascade')->onDelete('restrict');
            $table->foreign('segment_id')->references('id')->on('marketing.segments')->onUpdate('cascade')->onDelete('restrict');
        });

        Schema::table('general.customs_properties', function(Blueprint $table) {
            $table->foreign('experience_id')->references('id')->on('marketing.experiences')->onUpdate('cascade')->onDelete('restrict');
            $table->foreign('user_id')->references('id')->on('general.users')->onUpdate('cascade')->onDelete('restrict');
        });

        Schema::table('marketing.experiences', function(Blueprint $table) {
            $table->foreign('campaign_id')->references('id')->on('marketing.campaigns')->onUpdate('cascade')->onDelete('restrict');
            $table->foreign('geofence_id')->references('id')->on('triggers.geofences')->onUpdate('cascade')->onDelete('restrict');
            $table->foreign('zone_id')->references('id')->on('triggers.zones')->onUpdate('cascade')->onDelete('restrict');
            $table->foreign('beacon_id')->references('id')->on('triggers.beacons')->onUpdate('cascade')->onDelete('restrict');
            $table->foreign('repeat_id')->references('id')->on('marketing.repeats')->onUpdate('cascade')->onDelete('restrict');
        });

        Schema::table('marketing.galleries', function(Blueprint $table) {
            $table->foreign('language_id')->references('id')->on('general.languages')->onUpdate('cascade')->onDelete('restrict');
        });

        Schema::table('triggers.geofences', function(Blueprint $table) {
            $table->foreign('product_id')->references('id')->on('interests.products')->onUpdate('cascade')->onDelete('restrict');
        });

        Schema::table('interests.interest_user', function(Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('general.users')->onUpdate('cascade')->onDelete('restrict');
            $table->foreign('interest_id')->references('id')->on('interests.interests')->onUpdate('cascade')->onDelete('restrict');
        });

        Schema::table('interests.interests', function(Blueprint $table) {
            $table->foreign('category_id')->references('id')->on('interests.categories')->onUpdate('cascade')->onDelete('restrict');
            $table->foreign('product_id')->references('id')->on('interests.products')->onUpdate('cascade')->onDelete('restrict');
        });

        Schema::table('marketing.notifications', function(Blueprint $table) {
            $table->foreign('product_id')->references('id')->on('interests.products')->onUpdate('cascade')->onDelete('restrict');
            $table->foreign('segment_id')->references('id')->on('marketing.segments')->onUpdate('cascade')->onDelete('restrict');
        });

        Schema::table('marketing.printings', function(Blueprint $table) {
            $table->foreign('experience_id')->references('id')->on('marketing.experiences')->onUpdate('cascade')->onDelete('restrict');
            $table->foreign('user_id')->references('id')->on('general.users')->onUpdate('cascade')->onDelete('restrict');
        });

        Schema::table('interests.products', function(Blueprint $table) {
            $table->foreign('app_id')->references('id')->on('general.apps')->onUpdate('cascade')->onDelete('restrict');
        });

        Schema::table('marketing.segments', function(Blueprint $table) {
            $table->foreign('product_id')->references('id')->on('interests.products')->onUpdate('cascade')->onDelete('restrict');
        });

        Schema::table('marketing.stickers', function(Blueprint $table) {
            $table->foreign('experience_id')->references('id')->on('marketing.experiences')->onUpdate('cascade')->onDelete('restrict');
        });

        Schema::table('marketing.surveys', function(Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('general.users')->onUpdate('cascade')->onDelete('restrict');
            $table->foreign('experience_id')->references('id')->on('marketing.experiences')->onUpdate('cascade')->onDelete('restrict');
        });

        Schema::table('general.users', function(Blueprint $table) {
            $table->foreign('app_id')->references('id')->on('general.apps')->onUpdate('cascade')->onDelete('restrict');
            $table->foreign('language_id')->references('id')->on('general.languages')->onUpdate('cascade')->onDelete('restrict');
        });

        Schema::table('triggers.zones', function(Blueprint $table) {
            $table->foreign('geofence_id')->references('id')->on('triggers.geofences')->onUpdate('cascade')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('general.account_product', function(Blueprint $table) {
            $table->dropForeign('general_account_product_community_id_foreign');
            $table->dropForeign('general_account_product_product_id_foreign');
        });

        Schema::table('general.accounts', function(Blueprint $table) {
            $table->dropForeign('general_accounts_app_id_foreign');
            $table->dropForeign('general_accounts_role_id_foreign');
        });

        Schema::table('general.app_campaigns_types', function(Blueprint $table) {
            $table->dropForeign('general_app_campaigns_types_app_id_foreign');
            $table->dropForeign('general_app_campaigns_types_campaign_type_id_foreign');
        });

        Schema::table('general.app_input', function(Blueprint $table) {
            $table->dropForeign('general_app_input_app_id_foreign');
            $table->dropForeign('general_app_input_input_id_foreign');
        });

        Schema::table('general.app_language', function(Blueprint $table) {
            $table->dropForeign('general_app_language_app_id_foreign');
            $table->dropForeign('general_app_language_language_id_foreign');
        });

        Schema::table('interests.areas', function(Blueprint $table) {
            $table->dropForeign('interests_areas_branch_id_foreign');
        });

        Schema::table('marketing.beacon_product', function(Blueprint $table) {
            $table->dropForeign('marketing_beacon_product_beacon_id_foreign');
            $table->dropForeign('marketing_beacon_product_product_id_foreign');
        });

        Schema::table('triggers.beacon_zone', function(Blueprint $table) {
            $table->dropForeign('triggers_beacon_zone_beacon_id_foreign');
            $table->dropForeign('triggers_beacon_zone_zone_id_foreign');
        });

        Schema::table('triggers.beacons', function(Blueprint $table) {
            $table->dropForeign('triggers_beacons_app_id_foreign');
            $table->dropForeign('triggers_beacons_area_id_foreign');
        });

        Schema::table('content.branch_cover', function(Blueprint $table) {
            $table->dropForeign('content_branch_cover_branch_id_foreign');
            $table->dropForeign('content_branch_cover_cover_id_foreign');
        });

        Schema::table('interests.branch_product', function(Blueprint $table) {
            $table->dropForeign('interests_branch_product_branch_id_foreign');
            $table->dropForeign('interests_branch_product_product_id_foreign');
        });

        Schema::table('interests.branch_user', function(Blueprint $table) {
            $table->dropForeign('interests_branch_user_branch_id_foreign');
            $table->dropForeign('interests_branch_user_user_id_foreign');
        });

        Schema::table('interests.branches', function(Blueprint $table) {
            $table->dropForeign('interests_branches_app_id_foreign');
        });

        Schema::table('marketing.campaigns', function(Blueprint $table) {
            $table->dropForeign('marketing_campaigns_campaign_type_id_foreign');
            $table->dropForeign('marketing_campaigns_product_id_foreign');
            $table->dropForeign('marketing_campaigns_segment_id_foreign');
        });

        Schema::table('interests.categories', function(Blueprint $table) {
            $table->dropForeign('interests_categories_app_id_foreign');
            $table->dropForeign('interests_categories_category_id_foreign');
        });

        Schema::table('marketing.codes', function(Blueprint $table) {
            $table->dropForeign('marketing_codes_experience_id_foreign');
            $table->dropForeign('marketing_codes_user_id_foreign');
        });

        Schema::table('marketing.conditions', function(Blueprint $table) {
            $table->dropForeign('marketing_conditions_segment_id_foreign');
        });

        Schema::table('content.covers', function(Blueprint $table) {
            $table->dropForeign('content_covers_product_id_foreign');
            $table->dropForeign('content_covers_segment_id_foreign');
        });

        Schema::table('general.customs_properties', function(Blueprint $table) {
            $table->dropForeign('general_customs_properties_experience_id_foreign');
            $table->dropForeign('general_customs_properties_user_id_foreign');
        });

        Schema::table('marketing.experiences', function(Blueprint $table) {
            $table->dropForeign('marketing_experiences_campaign_id_foreign');
            $table->dropForeign('marketing_experiences_geofence_id_foreign');
            $table->dropForeign('marketing_experiences_zone_id_foreign');
            $table->dropForeign('marketing_experiences_beacon_id_foreign');
            $table->dropForeign('marketing_experiences_repeat_id_foreign');
        });

        Schema::table('marketing.galleries', function(Blueprint $table) {
            $table->dropForeign('marketing_galleries_language_id_foreign');
        });

        Schema::table('triggers.geofences', function(Blueprint $table) {
            $table->dropForeign('triggers_geofences_product_id_foreign');
        });

        Schema::table('interests.interest_user', function(Blueprint $table) {
            $table->dropForeign('interests_interest_user_user_id_foreign');
            $table->dropForeign('interests_interest_user_interest_id_foreign');   
        });

        Schema::table('interests.interests', function(Blueprint $table) {
            $table->dropForeign('interests_interests_category_id_foreign');
            $table->dropForeign('interests_interests_product_id_foreign');   
        });

        Schema::table('marketing.notifications', function(Blueprint $table) {
            $table->dropForeign('marketing_notifications_product_id_foreign');
            $table->dropForeign('marketing_notifications_segment_id_foreign');   
        });

        Schema::table('marketing.printings', function(Blueprint $table) {
            $table->dropForeign('marketing_printings_experience_id_foreign');
            $table->dropForeign('marketing_printings_user_id_foreign');   
        });

        Schema::table('interests.products', function(Blueprint $table) {
            $table->dropForeign('interests_products_app_id_foreign');   
        });

        Schema::table('marketing.segments', function(Blueprint $table) {
            $table->dropForeign('marketing_segments_product_id_foreign');   
        });

        Schema::table('marketing.stickers', function(Blueprint $table) {
            $table->dropForeign('marketing_stickers_experience_id_foreign');   
        });

        Schema::table('marketing.surveys', function(Blueprint $table) {
            $table->dropForeign('marketing_surveys_user_id_foreign');   
            $table->dropForeign('marketing_surveys_experience_id_foreign');   
        });

        Schema::table('general.users', function(Blueprint $table) {
            $table->dropForeign('general_users_app_id_foreign');   
            $table->dropForeign('general_users_language_id_foreign');   
        });

        Schema::table('triggers.zones', function(Blueprint $table) {
            $table->dropForeign('triggers_zones_geofence_id_foreign');  
        });
    }
}
