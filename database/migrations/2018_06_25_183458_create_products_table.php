<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interests.products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('app_id')->unsigned();
            $table->json('label');
            $table->string('tagname');
            $table->string('picture');
            $table->json('additional_data')->nullable();
            $table->json('input')->nullable();

            $table->timestamps();
            $table->softDeletes();
            $table->unique(['app_id', 'tagname']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interests.products');
    }
}
