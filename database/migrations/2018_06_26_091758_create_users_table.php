<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('general.users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('app_id')->unsigned();
            $table->integer('language_id')->unsigned();
            $table->string('network', 25);
            $table->string('fullname', 255);
            $table->string('nickname', 255)->nullable();
            $table->string('email', 255)->nullable();
            $table->string('dni', 255)->nullable();
            $table->string('password', 255)->nullable();
            $table->char('gender', 1);
            $table->timestamp('birthdate');
            $table->string('picture', 255)->nullable();
            $table->json('additional_data')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('general.users');
    }
}
