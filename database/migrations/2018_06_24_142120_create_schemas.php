<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateSchemas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared('CREATE SCHEMA IF NOT EXISTS content');
        DB::unprepared('CREATE SCHEMA IF NOT EXISTS general');
        DB::unprepared('CREATE SCHEMA IF NOT EXISTS interests');
        DB::unprepared('CREATE SCHEMA IF NOT EXISTS marketing');
        DB::unprepared('CREATE SCHEMA IF NOT EXISTS triggers');
        DB::unprepared('CREATE SCHEMA IF NOT EXISTS public');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP SCHEMA IF EXISTS content CASCADE');
        DB::unprepared('DROP SCHEMA IF EXISTS general CASCADE');
        DB::unprepared('DROP SCHEMA IF EXISTS interests CASCADE');
        DB::unprepared('DROP SCHEMA IF EXISTS marketing CASCADE');
        DB::unprepared('DROP SCHEMA IF EXISTS triggers CASCADE');
    }
}
