<?php

use App\Models\Account;
use App\Models\App;
use App\Models\Area;
use App\Models\Beacon;
use App\Models\Branch;
use App\Models\Campaign;
use App\Models\CampaignType;
use App\Models\Category;
use App\Models\Condition;
use App\Models\Experience;
use App\Models\Geofence;
use App\Models\Product;
use App\Models\Repeat;
use App\Models\Segment;
use App\Models\Zone;
use App\Models\Interest;
use App\Models\Notification;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Carbon\Carbon;

class TestSeeder extends Seeder
{
    private $faker;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->faker = Faker::create();

        $app = $this->apps();
        $this->accounts($app);
        
        $category   = $this->categories($app);
        $product    = $this->products($app);
        $branch     = $this->branches($app);
        $area       = $this->areas($branch);
        $geofence   = $this->geofences($product);
        $zone       = $this->zones($geofence);
        $beacon     = $this->beacons($app, $area, $zone);
        $segment    = $this->segments($product);
        $campaign   = $this->campaigns($product);
        $experience = $this->experiences($campaign);

        $this->notifications($product);


        $this->command->info( "OK\t ".__CLASS__ );
    }

    private function apps()
    {
      App::flushEventListeners();   

      $app = App::create([
        'name'             => $this->faker->name,
        'email'            => $this->faker->safeEmail,
        'deep_link_scheme' => 'uberflug://',
        'domain_url'       => $this->faker->domainName,
        'appkey'           => 'nX24Ux0zx5bYwTxmOXnWy7ovJPcROJZRl7mOLimVabEMPjwy7Z5MZ6hQgT7CvRYw1YQZJcSKQdkOz7AHWCJzC1ybvXdvUfZkUm8qQza0biHRenwdWfeQUXsgrZ0HBAnw4e48LRczCqZILTx9zCh2ARNbWX4GxqTQvTUqpsoW4adyOXdKn8kabtp3jj6uiVQDNfOcB9oMlPQ6G066Kslc84JnV8jGU3yoWvfCt4kFr8VUZinmvmYAWlTeFXy1WWY',
        'additional_data'  => [
          'auth' => [
            'field'        => 'email',
            'has_password' => true,
          ],

          'datamapping'    => [
            'Nombre' => null,
            'Edad'   => null,
            'Género' => ['male', 'female'],
            'Tester' => ['true', 'false'],
            'Apodo'  => null
          ]
        ]
      ]);

      \App\Jobs\CreateServerAuthorization::dispatch($app);

      // Vincula el input MOCA con el app
      $this->attachInputs($app);      

      // Vincula los lenguajes habilitados con el app
      $this->attachLanguages($app);

      // Vincula los tipos de campaña con el app
      $this->attachCampaignTypes($app);      

      $this->command->warn( "OK\t ".__METHOD__ );
      return $app;
    }

    private function attachInputs($app)
    {
      $input = \App\Models\Input::first();
      $input->apps()->syncWithoutDetaching([$app->id => [
        'additional_data'  => '{"account":"lab@uberflug.co","password":"enOHsu58Wq","token":"faa7fec8-bc9d-4e2d-99e2-6b17509e678f", "account_id": "1QjT7wjrQsyfBmVVcOdvLQ", "app_id":"nmLPoSkQQQa8zcaCmQKjVQ"}',
      ]]);
    }

    private function attachLanguages($app)
    {
      $languages = \App\Models\Language::all();
      $language_default = true;
      foreach ($languages as $language) {
        $language->apps()->syncWithoutDetaching([$app->id => [
          'default'  => $language_default,
        ]]);

        $language_default = !$language_default;
      }
    }

    private function attachCampaignTypes($app)
    {
      $campaign_types = \App\Models\CampaignType::all();
      foreach ($campaign_types as $type) {
        $type->apps()->syncWithoutDetaching($app->id);
      }
    }

    private function accounts(App $app)
    {
      Account::flushEventListeners();

      $admin     = \App\Models\Role::find(1); 
      $community = \App\Models\Role::find(2);

      $accounts = [
        [$app->id, $admin->key,     'test@uberflug.com',         $this->faker->name, bcrypt( 'secret' )],
        [$app->id, $admin->key,     'admin@uberflug.com',        $this->faker->name, bcrypt( 'secret' )],
        [$app->id, $community->key, 'management@uberflug.com',   $this->faker->name, bcrypt( 'secret' )],
        [$app->id, $community->key, 'community@uberflug.com',    $this->faker->name, bcrypt( 'secret' )],
        [$app->id, $community->key, 'community_02@uberflug.com', $this->faker->name, bcrypt( 'secret' )],
        [$app->id, $admin->key,     'max@uberflug.com',          $this->faker->name, bcrypt( 'secret' )]
      ]; 

      foreach ($accounts as $account) { 
        Account::create([
          'role_id'   => $account[1],
          'app_id'    => $account[0],
          'email'     => $account[2],
          'fullname'  => $account[3],
          'password'  => $account[4]
        ]);
      }

      $this->command->warn( "OK\t ".__METHOD__ );
    }

    private function categories($app)
    {
      Category::flushEventListeners();

      $categories = [
        [$app->id, ['es' => 'Category Test'], ['moca' => '_ZPL5L7NQoS--F_IAe1ADA'], ['es' => 'Subcategory Test']],
        [$app->id, ['es' => 'Deporte'],       ['moca' => '_ZPL5L7NQoS--F_IAe1ADA'], ['es' => 'Fulbol']],
        [$app->id, ['es' => 'Comida'],        ['moca' => '_ZPL5L7NQoS--F_IAe1ADA'], ['es' => 'China']],
        [$app->id, ['es' => 'Educación'],     ['moca' => '_ZPL5L7NQoS--F_IAe1ADA'], ['es' => 'Cursos']],
        [$app->id, ['es' => 'Hogar'],         ['moca' => '_ZPL5L7NQoS--F_IAe1ADA'], ['es' => 'Electrodomesticos']]
      ]; 

      foreach ($categories as $value) { 
        $category          = new Category;
        $category->app_id  = $value[0];
        $category->label   = $value[1];
        $category->tagname = null;
        $category->input   = $value[2]; 
        $category->save();

        $subcategory              = new Category;                      
        $subcategory->app_id      = $value[0];                
        $subcategory->category_id = $category->key;       
        $subcategory->label       = $value[3];
        $subcategory->tagname     = null;
        $subcategory->save();  
      }                           

      $this->command->warn( "OK\t ".__METHOD__ );
      //return $subcategory;
    }

    private function products($app)
    {
      Product::flushEventListeners();

      $filename = str_random(10).$this->faker->fileExtension;

      $product = new Product;
      $product->app_id          = $app->id;
      $product->label           = ['es' => 'Product Test'];
      $product->tagname         = null;
      $product->picture         = url("public/images/{$filename}");
      $product->additional_data = ['color' => '#fff000'];
      $product->save();

      $product->communities()->syncWithoutDetaching([1, 3, 6]);

      Interest::create([
        'category_id'   => 1,
        'product_id'    => 1
        ]);

      $this->command->warn( "OK\t ".__METHOD__ );
      return $product;
    }

    private function notifications($product)
    {
      Notification::flushEventListeners();
      
      $notification   = new Notification;
      $notification->product_id = $product->id;
      $notification->title      = ['es' => 'Notificación de Prueba', 'en' => 'Notification Test'];
      $notification->message    = ['es' => 'Producto creado', 'en' => 'Product created'];
      $notification->sent       = false;
      $notification->send_at    = Carbon::now();
      $notification->save();

      $this->command->warn( "OK\t ".__METHOD__ );
      return $notification;

    }

    private function branches($app)
    {
      Branch::flushEventListeners();
      
      $branch = new Branch;
      $branch->app_id       = $app->id;
      $branch->label        = 'Branch Test'; 
      $branch->description  = $this->faker->text(200); 
      $branch->geo_position = [
          'latitude'  => $this->faker->latitude,
          'longitude' => $this->faker->longitude,
        ]; 
      $branch->telephones   = [$this->faker->tollFreePhoneNumber]; 
      $branch->address      = $this->faker->address; 
      $branch->schedule     = ['mon' => ['start_at' => '08:00', 'end_at' => '17:00']];
      $branch->tagname      = null;
      $branch->save();
      
      $this->command->warn( "OK\t ".__METHOD__ );
      return $branch;
    }

    private function areas($branch)
    {
      Area::flushEventListeners();

      $filename = str_random(10).$this->faker->fileExtension;
      
      $area = new Area;
      $area->branch_id = $branch->id;
      $area->picture   = url("public/images/{$filename}");
      $area->order     = 1;
      $area->save();
      
      $this->command->warn( "OK\t ".__METHOD__ );
      return $area;
    }

    private function geofences($product)
    {
      Geofence::flushEventListeners();

      $geofence = new Geofence;
      $geofence->product_id   = $product->id;
      $geofence->label        = $this->faker->streetName; 
      $geofence->description  = $this->faker->address; 
      $geofence->geo_position = [
          'latitude'  => $this->faker->latitude,
          'longitude' => $this->faker->longitude,
        ]; 
      $geofence->radius       = $this->faker->numberBetween(100, 200); 
      $geofence->input        = ['moca' => 'jVG1U_MNQiuplWIa8TnZzg'];
      $geofence->save();
      
      $this->command->warn( "OK\t ".__METHOD__ );

      return $geofence;
    }

    private function zones($geofence)
    {
      Zone::flushEventListeners();

      $zone = new Zone;
      $zone->geofence_id  = $geofence->id;
      $zone->label        = $this->faker->name;
      $zone->description  = $this->faker->text(200);
      $zone->input        = ['moca' => 'af2p7euCQoO4qDqthHtJyA'];
      $zone->save();
      
      $this->command->warn( "OK\t ".__METHOD__ );
      return $zone;
    }

    private function beacons($app, $area, $zone)
    {
      Beacon::flushEventListeners();
      $y = "";
      for($x=0; $x<10; $x++){
        $beacon = new Beacon;
        $beacon->app_id       = $app->id;
        $beacon->label        = $this->faker->name;
        $beacon->description  = $this->faker->text(200);
        $beacon->uuid         = "b9407f30-f5f8-466e-aff9-25556b57fe6a$y";
        $beacon->major        = '20223';
        $beacon->minor        = '24101';
        $beacon->input        = ['moca' => 'ygqLm3EuRoO4nwCXVPweOQ'];
        $beacon->area_id      = $area->id;
        $beacon->geo_position = [
            'latitude'  => $area->branch->geo_position->latitude,
            'longitude' => $area->branch->geo_position->longitude,
          ];
        $beacon->save();
        $y .= "$x";
      }
      

      $this->attachZones($beacon, $zone);
      
      $this->command->warn( "OK\t ".__METHOD__ );
      return $beacon;
    }

    private function attachZones($beacon, $zone)
    {
      $beacon->zones()->syncWithoutDetaching($zone->id);
    }

    private function segments($product)
    {

      Segment::flushEventListeners();

      $segment = new Segment;
      $segment->product_id  = $product->id; 
      $segment->label       = $this->faker->word; 
      $segment->description = $this->faker->text(20);  
      $segment->input       = ['moca' => 'UkFzM6n0Qp69czbICWcD-Q']; 
      $segment->save();
      
      Condition::flushEventListeners();

      $condition = new Condition;
      $condition->segment_id = $segment->id;
      $condition->type       = 'tags';
      $condition->provider   = 'category';
      $condition->property   = null;
      $condition->condition  = 'contains';
      $condition->value      = 'category_test';
      $condition->save();

      $this->command->warn( "OK\t ".__METHOD__ );

      return $segment;
    }

    private function campaigns($product)
    {
      $campaign_type = CampaignType::first();

      Campaign::flushEventListeners();

      $schedule = [
        [
          'data'      =>  ['mon', 'tue', 'wed'],
          'end_at'    =>  '17:00',
          'start_at'  =>  '20:00'
        ],
        [
          'data'      =>  ['wed'],
          'end_at'    =>  '12:00',
          'start_at'  =>  '17:00'
        ]
      ];

      $campaign = new Campaign;
      $campaign->campaign_type_id = $campaign_type->key;
      $campaign->product_id       = $product->id; 
      $campaign->segment_id       = null; 
      $campaign->name             = ['es' => $this->faker->word]; 
      $campaign->description      = ['es' => $this->faker->text(20)];
      $campaign->start_at         = '2018-08-02 00:00:00'; 
      $campaign->end_at           = '2018-08-03 00:00:00'; 
      $campaign->schedule         = $schedule; //['mon' => ['start_at' => '08:00', 'end_at' => '17:00']]; 
      $campaign->input            = ['moca' => 'JwoWoGT6T8yzL6AnV1Pw-Q']; 
      $campaign->status           = true;
      $campaign->save();
      
      $this->command->warn( "OK\t ".__METHOD__ );

      return $campaign;
    }

    private function experiences($campaign)
    {

      $geofence = Geofence::first();
      $repeat   = Repeat::first();

      Experience::flushEventListeners();

      $experience = new Experience;
      $experience->campaign_id          = $campaign->id;
      $experience->name                 = ['es' => $this->faker->word];
      $experience->description          = ['es' => $this->faker->text(20)];
      $experience->message_notification = ['es' => $this->faker->text(20)];
      $experience->enabled              = true;
      $experience->attachement_type     = 'image';
      $experience->geofence_id          = $geofence->key;
      $experience->repeat_id            = $repeat->key;
      $experience->trigger              = 'onenter';
      $experience->input                = ['moca' => 'TuhpCMgtTAqGhyRaaPr0Eg-Q']; 
      $experience->save();
      
      $this->command->warn( "OK\t ".__METHOD__ );
    }
}
