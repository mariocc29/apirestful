<?php

use App\Models\Account;
use App\Models\App;
use App\Models\CampaignType;
use App\Models\Input;
use App\Models\Language;
use App\Models\Repeat;
use App\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;

class InitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Artisan::call('passport:install', ['--force' => true]);

        $this->roles();
        $this->repeats(); 
        $this->inputs();
        $this->languages();
        $this->campaignTypes();

        $this->command->info( "OK\t ".__CLASS__ );
    }

    private function roles()
    {
      Role::flushEventListeners();
      Role::create(['label' => 'Administrator']);
      Role::create(['label' => 'Community Manager']);
      $this->command->warn( "OK\t ".__METHOD__ );
    }

    private function repeats () 
    {
      Repeat::flushEventListeners();


      $repeats = ['Immediately', 'Never', 'After1Min', 'After3Min', 'After5Min', 'After15Min', 'After30Min', 'After1Hour', 'After2Hours', 'After4Hours', 'After8Hours', 'After12Hours', 'After24Hours', 'NextDay', 'After2Days', 'After3Days', 'After5Days', 'NextWeek', 'After2Weeks', 'NextMonth', 'After2Months'];

      foreach ($repeats as $repeat) {
        Repeat::create(['label' => $repeat, 'code' => $repeat]);
      }

      $this->command->warn( "OK\t ".__METHOD__ );
    }

    private function inputs()
    {
      Input::flushEventListeners();
      Input::create([
        'label' => 'Moca Platform',
        'code'  => 'moca',
      ]);
      $this->command->warn( "OK\t ".__METHOD__ );
    }

    private function languages()
    {
      Language::flushEventListeners();
      Language::create([
          'label' => 'español',
          'iso'   => 'es',
        ]);
      Language::create([
          'label' => 'inglés',
          'iso'   => 'en',
        ]);
      $this->command->warn( "OK\t ".__METHOD__ );
    }

    private function campaignTypes()
    {
      CampaignType::flushEventListeners();

      $types = [
          'birthday'       => CampaignType::ACTION_PUSH,
          'points'         => CampaignType::ACTION_PROXIMITY,
          'promotions'     => CampaignType::ACTION_PROXIMITY,
          'roulette'       => CampaignType::ACTION_PROXIMITY,
          'share_photo'    => CampaignType::ACTION_PROXIMITY,
          'stickers'       => CampaignType::ACTION_PROXIMITY,
          'surveys_single' => CampaignType::ACTION_PROXIMITY,
          'surveys_stars'  => CampaignType::ACTION_PROXIMITY,
          'treasure'       => CampaignType::ACTION_PROXIMITY,
        ];

      foreach ($types as $label => $action) {
        CampaignType::create(['label' => $label, 'action' => $action]);
      }

      $this->command->warn( "OK\t ".__METHOD__ );
    }  

    
}
