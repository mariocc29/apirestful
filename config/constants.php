<?php

  return [
    'console' => [
      'token' => env('CONSOLE_TOKEN'),
    ],
    'uberflug' => [
      'token' => env('UBERBLUG_TOKEN'),
    ],

    // Operadores para la segmentación de campañas y notificaciones según el tipo de campo
    'segments' => [
      'operators' => [
        'string'    => [ 'is equal to', 'is not equal to', 'in', 'not in', 'is like', 'matches', 'starts with', 'ends with' ],
        'int'       => [ 'is equal to','is not equal to','is less than','is greater than','is not greater than','is not less than' ],
        'float'     => [ 'is equal to','is not equal to','is less than','is greater than','is not greater than','is not less than' ],
        'timestamp' => [ 'is equal to','is not equal to','is less than','is greater than','is not greater than','is not less than' ],
        'boolean'   => [ 'is equal to', 'is not equal to' ],
        'object'    => [ 'contains', 'does not contain' ],
      ],
    ],

    // Configuración de moca
    'moca' => [
      'segments' => [
        // Operadores para la segmentación de campañas y notificaciones
        'operators' => [
          'contains'            => '$contains',
          'does not contain'    => '$doesNotContain',
          'ends with'           => '$endsWith',
          'in'                  => '$in',
          'is equal to'         => '$eq',
          'is greater than'     => '$gt',
          'is less than'        => '$lt',
          'is like'             => '$like',
          'is not equal to'     => '$ne',
          'is not greater than' => '$lte',
          'is not less than'    => '$gte',
          'matches'             => '$matches',
          'not in'              => '$nin',
          'starts with'         => '$startsWith',
        ],
      ],
    ],
  ];